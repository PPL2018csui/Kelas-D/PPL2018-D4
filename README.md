# TemanCatat

**Generic**, **Intuitive** and **insightful** inventory tracking tools native android application.

### Build Status and Code Coverage

Here's the summary of build status and code coverage from important branch :

1. **sit_uat**

    [![build status](https://gitlab.com/PPL2018csui/Kelas-D/PPL2018-D4/badges/sit_uat/build.svg)](https://gitlab.com/PPL2018csui/Kelas-D/PPL2018-D4/commits/sit_uat)
    [![coverage report](https://gitlab.com/PPL2018csui/Kelas-D/PPL2018-D4/badges/sit_uat/coverage.svg)](https://gitlab.com/PPL2018csui/Kelas-D/PPL2018-D4/commits/sit_uat)

## Getting Started

[Clone the projects](https://gitlab.com/PPL2018csui/Kelas-D/PPL2018-D4.git)

### Prerequisites


 * [JDK Ubuntu](https://www.digitalocean.com/community/tutorials/how-to-install-java-with-apt-get-on-ubuntu-16-04) [windows](https://docs.oracle.com/javase/7/docs/webnotes/install/windows/jdk-installation-windows.html)
 * [JDK OS X](https://docs.oracle.com/javase/8/docs/technotes/guides/install/mac_jdk.html)
 * [Android Studio](https://developer.android.com/studio/index.html?hl=id)


### Installing

Clone the projects

```sh
git clone https://gitlab.com/PPL2018csui/Kelas-D/PPL2018-D4.git
```

Run projects in Android Studio

```sh
Open Android Studio
Open projects
Sync Gradle
Run projects
```

## Technologies

1. [Firebase](https://firebase.google.com/?hl=id)
2. [Realm](https://github.com/realm/realm-java)
3. [ButterKnife](https://github.com/JakeWharton/butterknife)
4. [Espresso](https://github.com/codepath/android_guides/wiki/UI-Testing-with-Espresso)
5. [JUnit](https://github.com/junit-team/junit4)
6. [Mockito](https://github.com/mockito/mockito)

## Running the tests

### Unit Tests

Create unit test under /app/src/test/java/com/ppl/d4/temancatat

For Example

```sh
public class MainActivityUnitTest {

    @Test
    public void testAdditionTrue() throws Exception {
        Example example = new Example();
        example.setNumber(5);
        assertTrue(example.getNumber() == 5);
    }
}
```

Run test

```sh
./gradlew jacocoTestDebugReport
```

Report generated under app/build/reports/jacoco/jacocoTestDebugReport/html/index.html

### Instrumented Tests

Create instrumented test under /app/src/androidTest/java/com/ppl/d4/temancatat

For Example

```sh
    public class MainActivityInstrumentedTest {

        @Rule
        public ActivityTestRule<MainActivity> activityTestRule =
                new ActivityTestRule<MainActivity>(MainActivity.class);

        @Test
        public void validateHelloWorldText() {
            onView(withId(R.id.hello_world_textview))
                    .check(matches(withText("Hello World!")));
        }

        @Test
        public void validateHelloWorldTextClickShowToastMessage() {
            onView(withId(R.id.hello_world_textview)).perform(click());

            onView(withText("hello world clicked"))
                    .inRoot(withDecorView(not(is(
                            activityTestRule
                                    .getActivity()
                                    .getWindow()
                                    .getDecorView()))))
                    .check(matches(isDisplayed()));
        }

    }
```

Run test

```sh
    Turn of animations on testing device.
    Open gradle windows and run connectedDebugAndroidTest
```

Report generated under app/build/reports/androidTests/connected/index.html


### Lint

To run lint

```sh
./gradlew lint
```

Report generated under /app/build/reports/lint-results.html

### Crashlytics

Create a fabric.properties file under the app folder and include your own Fabric apiKey and apiSecret there

For Example

```sh
#Contains API Secret used to validate your application. Commit to internal source control; avoid making secret public.
#Wed Apr 04 00:10:23 ICT 2018
apiSecret=xxxxx
apiKey=xxxxx
```

## Deployment

Add additional notes about how to deploy this on a live system

## Built With

* [Dropwizard](http://www.dropwizard.io/1.0.2/docs/) - The web framework used
* [Maven](https://maven.apache.org/) - Dependency Management
* [ROME](https://rometools.github.io/rome/) - Used to generate RSS Feeds

## Contributing

Please read [CONTRIBUTING.md](https://gist.github.com/PurpleBooth/b24679402957c63ec426) for details on our code of conduct, and the process for submitting pull requests to us.

## Versioning

We use [SemVer](http://semver.org/) for versioning. For the versions available, see the [tags on this repository](https://github.com/your/project/tags).

## Authors

[PPL GG] (https://medium.com/temancatat)

## License

This project is licensed under the MIT License - see the [LICENSE.md](LICENSE.md) file for details

## Acknowledgments

* Hat tip to anyone who's code was used
* Inspiration
* etc

## Release Notes Sprint Review 1 (30/03/2018)!

3 user stories are accepted by Product Owner
- As a user I want to add new transaction
- As a user I want to add new product when making new transaction
- As a user I want to see and check the list of transaction
