package com.ppl.d4.temancatat.ui.views;


public interface EditProductView extends BaseView {

    void onSaveProduct();

    void prepareStockUnit();

    void onErrorProductNameField(int messageId);

    void onErrorProductSKUField(int messageId);

    void onErrorProductDescriptionField(int messageId);

    void updateProductPreview(CharSequence text);

    void clearErrorOnAllFields();

    void showToastMessage(int changes_toast_message, int lengthShort);

    void finish();

    void onViewModeChanged();

    void onEditModeChanged();
}
