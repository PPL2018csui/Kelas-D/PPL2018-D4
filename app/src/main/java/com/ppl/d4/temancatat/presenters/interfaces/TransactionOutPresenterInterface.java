package com.ppl.d4.temancatat.presenters.interfaces;

import com.ppl.d4.temancatat.models.TransactionListItem;
import com.ppl.d4.temancatat.ui.views.TransactionOutView;

import java.util.List;

public interface TransactionOutPresenterInterface<V extends TransactionOutView> extends BasePresenterInterface<V> {

    List<TransactionListItem> getTransactionOutItems();

}