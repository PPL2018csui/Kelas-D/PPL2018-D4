package com.ppl.d4.temancatat.ui.activities;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.LinearLayout;

import com.ppl.d4.temancatat.R;
import com.ppl.d4.temancatat.models.Product;
import com.ppl.d4.temancatat.presenters.implementations.SearchProductPresenter;
import com.ppl.d4.temancatat.ui.adapters.ProductListAdapter;
import com.ppl.d4.temancatat.ui.views.SearchProductView;

import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class SearchProductActivity extends BaseActivity implements SearchProductView {

    private static final String TAG = "Choose Product Activity";

    private static final int ADD_PRODUCT_CODE = 1;

    @Inject
    SearchProductPresenter<SearchProductView> mPresenter;

    @BindView(R.id.product_list_recyclerview)
    RecyclerView productListRecyclerView;

    @BindView(R.id.no_product_linearlayout)
    LinearLayout noProductLinearLayout;

    private ProductListAdapter mProductListAdapter;

    @Override
    @OnClick(R.id.add_new_product_fabbuton)
    public void openAddProductActivity() {
        Intent intent = new Intent(this, AddProductActivity.class);
        startActivityForResult(intent, ADD_PRODUCT_CODE);
    }

    @Override
    public void onPrepareProductList() {
        List<Product> productList = mPresenter.getAllProducts();

        if (productList.size() == 0) {
            showNoProductPlaceholder();
        }
        else {
            showProductList(productList);
        }
    }

    @Override
    public void showProductList(List<Product> productList) {
        noProductLinearLayout.setVisibility(View.INVISIBLE);
        productListRecyclerView.setVisibility(View.VISIBLE);

        LinearLayoutManager layoutManager
                = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);

        productListRecyclerView.setLayoutManager(layoutManager);

        mProductListAdapter = new ProductListAdapter(this, productList, ProductListAdapter.SEARCH_PRODUCT_MODE);

        productListRecyclerView.setAdapter(mProductListAdapter);
    }

    @Override
    public void showNoProductPlaceholder() {
        productListRecyclerView.setVisibility(View.INVISIBLE);
        noProductLinearLayout.setVisibility(View.VISIBLE);
    }

    @Override
    public void onPrepareSearchView(Menu menu) {
        MenuItem searchViewMenuItem = menu.findItem(R.id.search_product_item);
        SearchView searchView = (SearchView) searchViewMenuItem.getActionView();

        searchView.setQueryHint(getResources().getString(R.string.search_product_string));

        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                List<Product> productList = mPresenter.filterProductsBySKU(query);
                if (mProductListAdapter != null) {
                    mProductListAdapter.setProductList(productList);
                }
                return true;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                List<Product> productList = mPresenter.filterProductsBySKU(newText);
                if (mProductListAdapter != null) {
                    mProductListAdapter.setProductList(productList);
                }
                return false;
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.search_product_menu, menu);
        return true;
    }

    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        super.onPrepareOptionsMenu(menu);

        onPrepareSearchView(menu);

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
        }

        return super.onOptionsItemSelected(item);

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if(requestCode == ADD_PRODUCT_CODE) {
            if(resultCode == Activity.RESULT_OK) {
                onPrepareProductList();
            }
        }
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_choose_product);

        getActivityComponent().inject(this);

        setUnBinder(ButterKnife.bind(this));
        ButterKnife.bind(this);

        mPresenter.onAttach(this);

        onPrepareProductList();
    }

    @Override
    protected void closeRealm() {
        mPresenter.closeRealm();
    }
}
