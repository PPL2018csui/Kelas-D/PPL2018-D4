package com.ppl.d4.temancatat.seeders;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class DummySeeder extends Seeder {
    private static final String ID = "DummySeeder";
    private static final String DESCRIPTION = "Seed dummy data";

    public static final List<Seeder> DEPENDENCIES =
            Collections.unmodifiableList(Arrays.asList(
                    new StockUnitSeeder(),
                    new DummyProductSeeder(),
                    new DummyProductStockRecordSeeder()
            ));

    public DummySeeder() {
        super(ID, DESCRIPTION, DEPENDENCIES);
    }
}
