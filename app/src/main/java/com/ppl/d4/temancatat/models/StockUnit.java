package com.ppl.d4.temancatat.models;

import java.util.UUID;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

public class StockUnit extends RealmObject {

    @PrimaryKey
    private String id;

    private String name;

    public StockUnit() {
        this.id = UUID.randomUUID().toString();
    }

    public StockUnit(String name) {
        this.name = name;
        this.id = UUID.randomUUID().toString();
    }

    public String getId() {
        return this.id;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getName() {
        return this.name;
    }
}
