package com.ppl.d4.temancatat.presenters.interfaces;

import com.ppl.d4.temancatat.models.Product;
import com.ppl.d4.temancatat.ui.views.SearchProductView;

import java.util.List;

public interface SearchProductPresenterInterface<V extends SearchProductView> extends BasePresenterInterface<V> {
    List<Product> getAllProducts();

    List<Product> filterProductsBySKU(String sKU);
}
