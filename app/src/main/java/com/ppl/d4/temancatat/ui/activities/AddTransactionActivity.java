package com.ppl.d4.temancatat.ui.activities;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.TextInputLayout;
import android.text.Editable;
import android.text.SpannableStringBuilder;
import android.text.TextWatcher;
import android.text.style.ForegroundColorSpan;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.TextView;
import android.widget.Toast;

import com.ppl.d4.temancatat.R;
import com.ppl.d4.temancatat.models.ProductStockRecord;
import com.ppl.d4.temancatat.presenters.interfaces.AddTransactionPresenterInterface;
import com.ppl.d4.temancatat.ui.views.AddTransactionView;
import com.ppl.d4.temancatat.utils.AppException;
import com.ppl.d4.temancatat.utils.DateUtil;
import com.ppl.d4.temancatat.utils.PriceUtil;

import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.Calendar;
import java.util.Date;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.OnTextChanged;

public class AddTransactionActivity extends BaseActivity  implements AddTransactionView {

    private static final String TAG = "Transaction Activity";

    private static final int callAddProductCode = 7;

    private ProductStockRecord.TransactionType transactionType = ProductStockRecord.TransactionType.TRANSACTION_IN;

    private String currentProductSKU;

    private Calendar calendar = Calendar.getInstance();

    private DatePickerDialog.OnDateSetListener date;

    private static final int HIDE_MENU = 0;
    private static final int DISPLAY_MENU = 1;
    private int menuState;

    @Inject
    AddTransactionPresenterInterface<AddTransactionView> mPresenter;

    @BindView(R.id.date_edittext)
    EditText transactionDate;

    @BindView(R.id.amount_edittext)
    EditText stockAmount;

    @BindView(R.id.unit_price_edittext)
    EditText price;

    @BindView(R.id.choose_product_edittext)
    EditText chosenProduct;

    @BindView(R.id.choose_product_textinputlayout)
    TextInputLayout chooseProductTextInputLayout;

    @BindView(R.id.amount_textinputlayout)
    TextInputLayout amountTextInputLayout;

    @BindView(R.id.unit_price_textinputlayout)
    TextInputLayout unitPriceTextInputLayout;

    @BindView(R.id.unit_stock_textview)
    TextView unitStock;

    @BindView(R.id.total_amount_textview)
    TextView transactionPreview;

    @BindView(R.id.transaction_in_radiobutton)
    RadioButton transactionInButton;

    @BindView(R.id.transaction_out_radiobutton)
    RadioButton transactionOutButton;

    @BindView(R.id.transaction_in_imageview)
    ImageView transactionInImage;

    @BindView(R.id.transaction_out_imageview)
    ImageView transactionOutImage;

    @BindView(R.id.save_transaction_button)
    Button saveButton;

    @OnClick(R.id.date_edittext)
    public void dateEditTextClicked() {
        new DatePickerDialog(AddTransactionActivity.this, R.style.DatePicker, date,
                calendar.get(Calendar.YEAR),
                calendar.get(Calendar.MONTH),
                calendar.get(Calendar.DAY_OF_MONTH)).show();
    }

    @OnClick(R.id.save_transaction_button)
    public void saveButtonClicked() {
        saveTransactionButtonClick();
    }

    @OnClick(R.id.transaction_in_radiobutton)
    public void transactionToIn() {
        transactionType = ProductStockRecord.TransactionType.TRANSACTION_IN;
        unitPriceTextInputLayout.setError(null);
    }

    @OnClick(R.id.transaction_out_radiobutton)
    public void transactionToOut() {
        transactionType = ProductStockRecord.TransactionType.TRANSACTION_OUT;
        showPriceRecommendation();
    }

    @OnClick(R.id.choose_product_edittext)
    public void openSearchProductActivity() {
        Intent intent = new Intent(this, SearchProductActivity.class);
        this.startActivityForResult(intent, callAddProductCode);
    }

    @OnTextChanged(value=R.id.amount_edittext, callback = OnTextChanged.Callback.AFTER_TEXT_CHANGED)
    public void amountEditTextChange() {
        updateTotalPrice();
        if(getStockAmount() != null) {
            cleanAmountError();
        }
    }

    @OnTextChanged(value=R.id.unit_price_edittext, callback = OnTextChanged.Callback.AFTER_TEXT_CHANGED)
    public void unitPriceEditTextChange() {
        updateTotalPrice();
        if(getPrice() != null) {
            cleanPriceError();
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_transaction);

        getActivityComponent().inject(this);

        initializeDateSetListener();

        setUnBinder(ButterKnife.bind(this));
        ButterKnife.bind(this);

        mPresenter.onAttach(this);
        mPresenter.setTransactionById(getIntent().getStringExtra("EXTRA_TRANSACTION_ID"));
        mPresenter.changeMode(getIntent().getStringExtra("EXTRA_MODE"));

        updateCalendarLabel();

        price.addTextChangedListener(onPriceFormattedChangedListener());
    }

    protected void clearFormError() {
        cleanAmountError();
        cleanPriceError();
        cleanProductError();
    }

    protected void disableForm() {
        price.setEnabled(false);
        stockAmount.setEnabled(false);
        chosenProduct.setEnabled(false);
        transactionDate.setEnabled(false);

        transactionType = mPresenter.getTransaction().getTransactionType();
        switch (transactionType) {
            case TRANSACTION_IN:
                transactionOutImage.setVisibility(View.GONE);
                transactionOutButton.setVisibility(View.GONE);
                break;
            case TRANSACTION_OUT:
                transactionInImage.setVisibility(View.GONE);
                transactionInButton.setVisibility(View.GONE);
                break;
        }
    }

    protected void enableForm() {
        price.setEnabled(true);
        stockAmount.setEnabled(true);
        chosenProduct.setEnabled(true);
        transactionDate.setEnabled(true);
        transactionOutImage.setVisibility(View.VISIBLE);
        transactionOutButton.setVisibility(View.VISIBLE);
        transactionInImage.setVisibility(View.VISIBLE);
        transactionInButton.setVisibility(View.VISIBLE);
    }

    protected void prefillFormData(ProductStockRecord transaction) {
        clearFormError();

        chosenProduct.setText(transaction.getProduct().getSKU());
        unitStock.setText(transaction.getProduct().getStockUnit().getName());
        currentProductSKU = transaction.getProduct().getSKU();

        transactionType = transaction.getTransactionType();
        switch (transactionType) {
            case TRANSACTION_IN:
                transactionInButton.setChecked(true);
                break;
            case TRANSACTION_OUT:
                transactionOutButton.setChecked(true);
                break;
        }

        stockAmount.setText(Integer.toString(transaction.getStocks()));
        price.setText(getResources().getString(R.string.rp_string) + doubleToStringNoExponent(transaction.getPrice()));
        calendar.setTime(transaction.getDate());
        updateCalendarLabel();
    }

    @Override
    public void onViewModeChanged() {
        displayMenuItems();
        getSupportActionBar().setTitle(getResources().getString(R.string.view_transaction_menu));
        saveButton.setVisibility(View.GONE);
        prefillFormData(mPresenter.getTransaction());
        disableForm();
    }

    @Override
    public void onEditModeChanged() {
        displayMenuItems();
        getSupportActionBar().setTitle(getResources().getString(R.string.edit_transaction_menu));
        saveButton.setVisibility(View.VISIBLE);
        prefillFormData(mPresenter.getTransaction());
        enableForm();
    }

    @Override
    public void onAddModeChanged() {
        hideMenuItems();
        getSupportActionBar().setTitle(getResources().getString(R.string.new_transaction_menu));
        saveButton.setVisibility(View.VISIBLE);
        enableForm();
    }

    protected void hideMenuItems() {
        menuState = HIDE_MENU;
        invalidateOptionsMenu();
    }

    protected void displayMenuItems() {
        menuState = DISPLAY_MENU;
        invalidateOptionsMenu();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if(requestCode == callAddProductCode) {
            if(resultCode == Activity.RESULT_OK) {
                chosenProduct.setText(data.getStringExtra("productSKU"));
                unitStock.setText(data.getStringExtra("productStockUnit"));
                currentProductSKU = data.getStringExtra("productSKU");
                chooseProductTextInputLayout.setError(null);
            }
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.add_transaction_menu, menu);

        if (menuState == HIDE_MENU)
        {
            for (int i = 0; i < menu.size(); i++)
                menu.getItem(i).setVisible(false);
        }
        else if (menuState == DISPLAY_MENU)
        {
            for (int i = 0; i < menu.size(); i++)
                menu.getItem(i).setVisible(true);
        }

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                Intent intent = new Intent(this, TransactionListActivity.class);
                setResult(RESULT_OK, intent);
                finish();
                return true;
            case R.id.edit_transaction_item:
                mPresenter.toggleEdit();
                return true;

            case R.id.delete_transaction_item:
                showDeleteDialog();
                return true;

            default:
                return super.onOptionsItemSelected(item);

        }
    }

    protected void showDeleteDialog() {
        Activity currentActivity = this;
        new AlertDialog.Builder(this, R.style.DeleteDialogStyle)
                .setMessage(R.string.delete_confirmation_dialog)
                .setPositiveButton(R.string.yes, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int whichButton) {
                        try {
                            mPresenter.deleteTransaction();
                        } catch (AppException e) {
                            showErrorMessage(e.getErrorMessage());
                            showToastMessage(R.string.negative_stock_message, Toast.LENGTH_LONG);
                            return;
                        }
                        Toast.makeText(getApplicationContext(), R.string.delete_toast_message, Toast.LENGTH_SHORT).show();
                        currentActivity.finish();
                    }})
                .setNegativeButton(R.string.no, null).show();
    }

    @Override
    protected void closeRealm() {
        mPresenter.closeRealm();
    }

    @Override
    public void saveTransactionButtonClick() {
        Date date = getTransactionDate();
        Integer stock = getStockAmount();
        Double priceAmount = getPrice();
        try {
            mPresenter.onSaveTransaction(currentProductSKU, date, transactionType, stock, priceAmount);
        } catch(AppException e) {
            showErrorMessage(e.getErrorMessage());
        }
    }

    @Override
    public void setInvalidProductError() {
        chooseProductTextInputLayout.setError(getResources().getString(R.string.error_message_product_field_empty));
    }

    @Override
    public void cleanProductError() {
        chooseProductTextInputLayout.setError(null);
    }

    @Override
    public void setInvalidAmountError() {
        amountTextInputLayout.setError(getResources().getString(R.string.error_message_stock_invalid_format));
    }

    @Override
    public void setNotEnoughStockError(int currentStock) {
        amountTextInputLayout.setError(String.format(getResources().getString(R.string.error_message_not_enough_stock), currentStock));
    }

    @Override
    public void setEditTransactionStockError() {
        amountTextInputLayout.setError(getResources().getString(R.string.error_message_failed_edit_transaction));
    }

    @Override
    public void cleanAmountError() {
        amountTextInputLayout.setError(null);
    }

    @Override
    public void setInvalidPriceError() {
        unitPriceTextInputLayout.setError(getResources().getString(R.string.error_message_price_invalid_format));
    }

    @Override
    public void cleanPriceError() {
        unitPriceTextInputLayout.setError(null);
    }

    @Override
    public Date getTransactionDate() {
        try {
            return calendar.getTime();
        } catch (Exception e) {
            return null;
        }
    }

    @Override
    public Integer getStockAmount() {
        try {
            return Integer.parseInt(stockAmount.getText().toString());
        } catch (Exception e) {
            return null;
        }
    }

    @Override
    public Double getPrice() {
        try {
            String rp = getResources().getString(R.string.rp_string);
            return Double.parseDouble(price.getText().toString().replaceAll("\\.","").replaceFirst(",",".").replace(rp,""));
        } catch (Exception e) {
            return null;
        }
    }

    @Override
    public void openTransactionListActivity() {
        Intent intent = new Intent(this, TransactionListActivity.class);
        this.startActivity(intent);
    }

    @Override
    protected void onDestroy() {
        mPresenter.onDetach();
        super.onDestroy();
    }

    @Override
    public void showToastMessage(int stringResourceId, int lengthModifier) {
        Toast.makeText(getApplicationContext(), stringResourceId, lengthModifier).show();
    }

    private void updateCalendarLabel() {
        transactionDate.setText(DateUtil.formatToDMY(calendar.getTime()));
    }

    private void updateTotalPrice() {
        String displayText = PriceUtil.convertPriceFormatID(0.0);
        if(getPrice() != null && getStockAmount() != null) {
            displayText = PriceUtil.convertPriceFormatID(getPrice() * getStockAmount());
        }
        transactionPreview.setText(displayText);
    }

    private void initializeDateSetListener() {
        date = new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                calendar.set(Calendar.YEAR, year);
                calendar.set(Calendar.MONTH, monthOfYear);
                calendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                updateCalendarLabel();
            }
        };
    }

    public String doubleToStringNoExponent(Double value) {
        DecimalFormat formatter = new DecimalFormat("0.00");
        String tmp = formatter.format(value);
        return PriceUtil.convertPriceFormatID(Double.parseDouble(tmp));
    }

    @OnTextChanged(value=R.id.unit_price_edittext, callback = OnTextChanged.Callback.AFTER_TEXT_CHANGED)
    public void showPriceRecommendation() {

        if(transactionType == ProductStockRecord.TransactionType.TRANSACTION_OUT && currentProductSKU != null) {
            double recommendationPrice = mPresenter.getRecommendationPrice(currentProductSKU);
            String recommendationString = getResources().getString(R.string.price_recommendation_message) + PriceUtil.convertPriceFormatID(recommendationPrice);
            ForegroundColorSpan foregroundColorSpan = new ForegroundColorSpan(getResources().getColor(R.color.colorPriceRecommendation));
            SpannableStringBuilder spannableStringBuilder = new SpannableStringBuilder(recommendationString);
            spannableStringBuilder.setSpan(foregroundColorSpan, 0, recommendationString.length(), 0);
            unitPriceTextInputLayout.setError(spannableStringBuilder);
        }
    }

    private TextWatcher onPriceFormattedChangedListener() {
        return new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                price.removeTextChangedListener(this);

                try {
                    String rp = getResources().getString(R.string.rp_string);
                    String originalString = s.toString();

                    originalString = originalString.replaceAll("\\.", "").replaceFirst(",",".");
                    originalString = originalString.replaceAll("[A-Z]", "").replaceAll("[a-z]","");

                    Double doubleval = Double.parseDouble(originalString);

                    DecimalFormatSymbols symbols = new DecimalFormatSymbols();
                    symbols.setDecimalSeparator(',');
                    symbols.setGroupingSeparator('.');

                    String pattern = "#,###.##";
                    DecimalFormat formatter = new DecimalFormat(pattern, symbols);
                    String formattedString = rp + formatter.format(doubleval);

                    price.setText(formattedString);
                    price.setSelection(price.getText().length());
                } catch (NumberFormatException nfe) {
                    nfe.printStackTrace();
                }

                price.addTextChangedListener(this);
            }
        };
    }
}
