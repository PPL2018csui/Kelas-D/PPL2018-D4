package com.ppl.d4.temancatat.presenters.implementations;

import com.github.mikephil.charting.charts.PieChart;
import com.github.mikephil.charting.data.PieData;
import com.github.mikephil.charting.data.PieDataSet;
import com.github.mikephil.charting.data.PieEntry;
import com.github.mikephil.charting.formatter.PercentFormatter;
import com.github.mikephil.charting.utils.ColorTemplate;
import com.ppl.d4.temancatat.models.LegendItem;
import com.ppl.d4.temancatat.models.Product;
import com.ppl.d4.temancatat.models.ProductStockRecord;
import com.ppl.d4.temancatat.models.manager.RealmDataManager;
import com.ppl.d4.temancatat.presenters.interfaces.OverallStatisticsPresenterInterface;
import com.ppl.d4.temancatat.ui.views.OverallStatisticsView;
import com.ppl.d4.temancatat.utils.Error;
import com.ppl.d4.temancatat.utils.RevenueUtil;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

import javax.inject.Inject;

public class OverallStatisticsPresenter<V extends OverallStatisticsView> extends BasePresenter<V> implements OverallStatisticsPresenterInterface<V>, RealmDataManager.OnTransactionCallback {

    private static final String TAG = "OverallStatisticsPresenter";

    private final int SALES_MODE = 0;
    private final int PURCHASES_MODE = 1;

    List<PieEntry> salesEntries;
    List<PieEntry> purchasesEntries;
    List<Product> products;
    List<LegendItem> salesLegendItems;
    List<LegendItem> purchasesLegendItems;
    List<Integer> colors = new ArrayList<>(Arrays.asList(
            ColorTemplate.JOYFUL_COLORS[0],
            ColorTemplate.JOYFUL_COLORS[1],
            ColorTemplate.JOYFUL_COLORS[3],
            ColorTemplate.COLORFUL_COLORS[0],
            ColorTemplate.COLORFUL_COLORS[1],
            ColorTemplate.COLORFUL_COLORS[3],
            ColorTemplate.COLORFUL_COLORS[4]
            ));

    @Inject
    public OverallStatisticsPresenter(final RealmDataManager realmDataManager) {
        super(realmDataManager);
    }

    @Override
    public void closeRealm() {
        super.closeRealm();
    }

    @Override
    public void onRealmSuccess() {}

    @Override
    public void onRealmError(Throwable e) {
        getView().showErrorMessage(e.toString());
    }

    @Override
    public boolean checkIsExistTransaction() {
        List<ProductStockRecord> productStockRecordList = getRealmDataManager().getAllProductStockRecords();
        return !productStockRecordList.isEmpty();
    }

    @Override
    public boolean checkIsExistTransactionOut() {
        List<ProductStockRecord> productStockRecordList = getRealmDataManager().getTransactionOutProductStockRecords();
        return !productStockRecordList.isEmpty();
    }

    @Override
    public boolean checkIsExistTransactionIn() {
        List<ProductStockRecord> productStockRecordList = getRealmDataManager().getTransactionInProductStockRecords();
        return !productStockRecordList.isEmpty();
    }

    @Override
    public double getTotalPurchasesAmount() {
        getView().onLoading();
        List<ProductStockRecord> productStockRecordList = getRealmDataManager().getAllProductStockRecords();
        getView().onFinishLoading();
        return RevenueUtil.countPurchases(productStockRecordList);
    }

    @Override
    public double getTotalSalesAmount() {
        getView().onLoading();
        List<ProductStockRecord> productStockRecordList = getRealmDataManager().getAllProductStockRecords();
        getView().onFinishLoading();
        return RevenueUtil.countSales(productStockRecordList);
    }

    @Override
    public double getNetRevenueAmount() {
        getView().onLoading();
        List<ProductStockRecord> productStockRecordList = getRealmDataManager().getAllProductStockRecords();
        getView().onFinishLoading();
        return RevenueUtil.countTotalRevenue(productStockRecordList);
    }

    @Override
    public void onPreparePieChartData() {
        getView().onLoading();
        salesEntries = new ArrayList<>();
        purchasesEntries = new ArrayList<>();
        salesLegendItems = new ArrayList<>();
        purchasesLegendItems = new ArrayList<>();
        products = getRealmDataManager().getAllProducts();

        double sales = RevenueUtil.countSales(getRealmDataManager().getAllProductStockRecords());
        double purchases = RevenueUtil.countPurchases(getRealmDataManager().getAllProductStockRecords());
        int salesColorCounter = 0;
        int purchaseColorCounter = 0;
        for (Product product: products) {
            String productSKU = product.getSKU();
            List<ProductStockRecord> transactions = getRealmDataManager().getProductStockRecordsByProductSKU(productSKU);
            double productSales = RevenueUtil.countSales(transactions);
            int productsSold = RevenueUtil.countTotalStockSales(transactions);
            if ((float)(productSales/sales) > 0.05 && productsSold > 0) {
                salesEntries.add(new PieEntry((float) (productSales / sales), productSKU));
                LegendItem saleLegend = new LegendItem(colors.get(salesColorCounter % 7), productSKU, productSales, productsSold);
                salesLegendItems.add(saleLegend);
            }
            salesColorCounter++;

            double productPurchases = RevenueUtil.countPurchases(transactions);
            int productsBought = RevenueUtil.countTotalStockPurchase(transactions);
            if ((float)(productPurchases/purchases) > 0.05 && productsBought > 0) {
                purchasesEntries.add(new PieEntry((float) (productPurchases / purchases), product.getSKU()));
                LegendItem purchaseLegend = new LegendItem(colors.get(purchaseColorCounter % 7), productSKU, productPurchases, productsBought);
                purchasesLegendItems.add(purchaseLegend);
            }
            purchaseColorCounter++;
        }
        getView().onFinishLoading();
    }

    @Override
    public void showPieChart(PieChart pieChart, int mode) {
        getView().onLoading();
        pieChart.getLegend().setEnabled(false);
        pieChart.getDescription().setEnabled(false);
        pieChart.setUsePercentValues(true);
        pieChart.setNoDataText("Belum ada data");
        pieChart.setHoleRadius(40f);
        pieChart.setTransparentCircleRadius(45f);
        PieDataSet set = null;
        if (mode == SALES_MODE) {
            pieChart.setCenterText("Penjualan Berdasarkan Produk");
            set = new PieDataSet(salesEntries, "Penjualan");
            set.setValueTextSize(16f);
            set.setValueTextColor(0xffffff00);
            set.setValueFormatter(new PercentFormatter());
            set.setColors(colors);
        } else if (mode == PURCHASES_MODE) {
            pieChart.setCenterText("Pembelian Berdasarkan Produk");
            set = new PieDataSet(purchasesEntries, "Pembelian");
            set.setValueTextSize(16f);
            set.setValueTextColor(0xffffff00);
            set.setValueFormatter(new PercentFormatter());
            set.setColors(colors);
        }
        PieData data = new PieData(set);
        pieChart.setData(data);
        pieChart.invalidate();
        getView().onFinishLoading();
    }

    @Override
    public List<LegendItem> getLegendItems(int mode) {
        if (mode == SALES_MODE) {
            Collections.sort(salesLegendItems);
            return salesLegendItems;
        } else {
            Collections.sort(purchasesLegendItems);
            return purchasesLegendItems;
        }
    }

}