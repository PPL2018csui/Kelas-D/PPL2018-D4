package com.ppl.d4.temancatat.di.components;

import com.ppl.d4.temancatat.di.PerActivity;
import com.ppl.d4.temancatat.di.modules.ActivityModule;
import com.ppl.d4.temancatat.models.manager.RealmDataManager;
import com.ppl.d4.temancatat.ui.activities.AddTransactionActivity;
import com.ppl.d4.temancatat.ui.activities.AddProductActivity;
import com.ppl.d4.temancatat.ui.activities.EditProductActivity;
import com.ppl.d4.temancatat.ui.activities.SearchProductActivity;
import com.ppl.d4.temancatat.ui.activities.TransactionListActivity;
import com.ppl.d4.temancatat.ui.fragments.AllTransactionFragment;
import com.ppl.d4.temancatat.ui.fragments.OverallStatisticsFragment;
import com.ppl.d4.temancatat.ui.fragments.ProductListFragment;
import com.ppl.d4.temancatat.ui.fragments.TransactionInFragment;
import com.ppl.d4.temancatat.ui.fragments.TransactionOutFragment;

import dagger.Component;

/**
 * Class used to specify which component to be injected by the modules.
 * Don't forget to add newly added activities in this class.
 */
@PerActivity
@Component(dependencies = ApplicationComponent.class, modules = ActivityModule.class)
public interface ActivityComponent {

    void inject(TransactionListActivity activity);

    void inject(AllTransactionFragment fragment);

    void inject(TransactionInFragment fragment);

    void inject(TransactionOutFragment fragment);

    void inject(OverallStatisticsFragment fragment);

    void inject(ProductListFragment fragment);

    void inject(AddTransactionActivity activity);

    void inject(AddProductActivity activity);

    void inject(SearchProductActivity activity);

    void inject(RealmDataManager realmDataManager);

    void inject(EditProductActivity editProductActivity);
}