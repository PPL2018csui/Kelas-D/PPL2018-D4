package com.ppl.d4.temancatat.models;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

import java.util.UUID;
import java.util.Date;

public class ProductStockRecord extends RealmObject implements TransactionListItem {

    public enum TransactionType {
        TRANSACTION_IN,
        TRANSACTION_OUT
    }

    @PrimaryKey
    private String id;
    private Product product;
    private Date date;
    private String transactionType;
    private int stocks;
    private double price;

    public ProductStockRecord(Product product, Date date, TransactionType type, int stocks, double price) {
        this.id = UUID.randomUUID().toString();
        if(product == null) {
            throw new IllegalArgumentException("Product cannot be null.");
        }
        this.product = product;
        if(date == null) {
            throw new IllegalArgumentException("Date cannot be null.");
        }
        this.date = date;
        if(type == null) {
            throw new IllegalArgumentException("Type cannot be null.");
        }
        this.transactionType = type.toString();
        if(stocks < 1) {
            throw new IllegalArgumentException("Stocks value must be at least 1.");
        }
        this.stocks = stocks;
        if(price < 0.0) {
            throw new IllegalArgumentException("Price value must be at least 0.");
        }
        this.price = price;
    }

    public ProductStockRecord() {
        this(new Product(), new Date(), TransactionType.TRANSACTION_IN, 1, 0);
    }

    public String getId() {
        return this.id;
    }

    public Product getProduct() {
        return this.product;
    }

    public int getType() {
        return TYPE_TRANSACTION;
    }

    public TransactionType getTransactionType() { return TransactionType.valueOf(this.transactionType); }

    public Date getDate() {
        return this.date;
    }

    public int getStocks() {
        return this.stocks;
    }

    public double getPrice() {
        return this.price;
    }

    public void setProduct(Product product) {
        if(product == null) {
            throw new IllegalArgumentException("Product cannot be null.");
        }
        this.product = product;
    }

    public void setTransactionType(TransactionType type) {
        if(type == null) {
            throw new IllegalArgumentException("Type cannot be null.");
        }
        this.transactionType = type.toString();
    }

    public void setDate(Date date) {
        if(date == null) {
            throw new IllegalArgumentException("Date cannot be null.");
        }
        this.date = date;
    }

    public void setStocks(int stocks) {
        if(stocks < 1) {
            throw new IllegalArgumentException("Stocks value must be at least 1.");
        }
        this.stocks = stocks;
    }

    public void setPrice(double price) {
        if(price < 0.0) {
            throw new IllegalArgumentException("Price value must be at least 0.");
        }
        this.price = price;
    }
}
