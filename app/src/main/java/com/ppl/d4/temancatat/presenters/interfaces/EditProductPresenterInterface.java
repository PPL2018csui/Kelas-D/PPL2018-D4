package com.ppl.d4.temancatat.presenters.interfaces;

import com.ppl.d4.temancatat.models.Product;
import com.ppl.d4.temancatat.models.StockUnit;
import com.ppl.d4.temancatat.ui.views.AddProductView;
import com.ppl.d4.temancatat.ui.views.EditProductView;
import com.ppl.d4.temancatat.utils.AppException;

import java.util.List;
import java.util.Map;

public interface EditProductPresenterInterface<V extends EditProductView> extends BasePresenterInterface<V> {

    List<String> getStockUnitsName();

    void onSaveProduct(String name, String description, String stockUnitName, String photoUrl) throws AppException;

    StockUnit getStockUnitByName(String name);

    String generateDefaultSKU(String productName, String productStockUnit);

    boolean isText(String editTextString, boolean required);
    boolean isStockUnitPresent(StockUnit stockUnit);
    boolean isNumber(String editTextString, boolean required);

    String getMode();
    void toggleEdit();

    Product getProduct();

    void changeMode(String mode);

    void setProductBySKU(String productSKU) throws AppException;

    int getStockValue(String productSKU);
    int getStockTransactionIn(String productSKU);
    int getStockTransactionOut(String productSKU);

    Map<String, Integer> getMonthlyStock(String productSKU);
}
