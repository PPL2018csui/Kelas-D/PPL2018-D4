package com.ppl.d4.temancatat.models;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

public class Product extends RealmObject {

    @PrimaryKey
    private String sKU;
    private String name;
    private String description;
    private StockUnit stockUnit;

    public Product() {

    }

    public Product(String name, String description, StockUnit stockUnit, String sKU) {
        this.name = name;
        this.description = description;
        this.stockUnit = stockUnit;
        this.sKU = sKU;
    }

    public void setSKU(String sKU) {
        this.sKU = sKU;
    }

    public String getSKU() {
        return this.sKU;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getName() {
        return this.name;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getDescription() {
        return this.description;
    }

    public void setStockUnit(StockUnit stockUnit) {
        this.stockUnit = stockUnit;
    }

    public StockUnit getStockUnit() {
        return this.stockUnit;
    }

    public String toString() {
        return this.name;
    }
}
