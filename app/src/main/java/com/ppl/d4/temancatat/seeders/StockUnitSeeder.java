package com.ppl.d4.temancatat.seeders;

import com.ppl.d4.temancatat.models.StockUnit;
import com.ppl.d4.temancatat.models.manager.RealmDataManager;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class StockUnitSeeder extends Seeder {

    private static final String ID = "StockUnitSeeder";
    private static final String DESCRIPTION = "Seed mandatory stock unit data";

    public static final List<String> STOCK_NAMES =
            Collections.unmodifiableList(Arrays.asList(
                    "box",
                    "cm",
                    "gram",
                    "kg",
                    "ltr",
                    "lusin",
                    "m",
                    "order",
                    "pcs",
                    "unit"
            ));

    public StockUnitSeeder() {
        super(ID, DESCRIPTION);
    }

    @Override
    public void handleSeed(RealmDataManager realmDataManager) {
        for (String stockName: STOCK_NAMES) {
            StockUnit stockUnit = realmDataManager.getStockUnitByName(stockName);
            if (stockUnit == null) {
                stockUnit = new StockUnit(stockName);
                realmDataManager.createStockUnit(stockUnit);
            }
        }
    }
}