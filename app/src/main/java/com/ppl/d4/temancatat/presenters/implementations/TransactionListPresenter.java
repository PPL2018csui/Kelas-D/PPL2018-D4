package com.ppl.d4.temancatat.presenters.implementations;

import com.ppl.d4.temancatat.utils.DateUtil;
import com.ppl.d4.temancatat.models.ProductStockRecord;
import com.ppl.d4.temancatat.models.TransactionDate;
import com.ppl.d4.temancatat.models.TransactionListItem;
import com.ppl.d4.temancatat.models.manager.RealmDataManager;
import com.ppl.d4.temancatat.presenters.interfaces.TransactionListPresenterInterface;
import com.ppl.d4.temancatat.ui.views.TransactionListView;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import javax.inject.Inject;

public class TransactionListPresenter<V extends TransactionListView> extends BasePresenter<V> implements TransactionListPresenterInterface<V>, RealmDataManager.OnTransactionCallback {

    private static final String TAG = "TransaxListPresenter";

    @Inject
    public TransactionListPresenter(final RealmDataManager realmDataManager) { super(realmDataManager); }

    @Override
    public void closeRealm() {
        super.closeRealm();
    }

    @Override
    public void onRealmSuccess() {}

    @Override
    public void onRealmError(Throwable e){}

}
