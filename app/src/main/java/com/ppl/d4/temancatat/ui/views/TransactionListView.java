package com.ppl.d4.temancatat.ui.views;

public interface TransactionListView extends BaseView {
    void onPreparePagerAdapter();

    void onClickAddNewFloatingActionButton();

    void onPrepareBottomNavigationView();

    void hideTab();

    void showTab();

    void hideFloatingActionButton();

    void showFloatingActionButton();
}
