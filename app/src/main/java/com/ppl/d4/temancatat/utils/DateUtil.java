package com.ppl.d4.temancatat.utils;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;


public class DateUtil  {
    private static final String DMY_FORMAT = "EEEE, dd MMMM yyyy";
    private static final String[] BULAN = {"Januari", "Februari", "Maret", "April", "Mei", "Juni", "Juli", "Agustus", "September", "Oktober", "November","Desember"};
    private static final String[] HARI = {"Minggu", "Senin", "Selasa", "Rabu", "Kamis", "Jumat", "Sabtu"};

    public static String formatToDMY(Date date) {
        SimpleDateFormat sdf = new SimpleDateFormat(DMY_FORMAT, Locale.US);
        String translateMonth = sdf.format(date).replaceFirst("(?s)"+"[^\\d ]+"+"(?!.*?"+"[^\\d ]+"+")" , BULAN[date.getMonth()]);
        return translateMonth.replaceFirst("[^\\d ]+",HARI[date.getDay()]+ ",");
    }
}
