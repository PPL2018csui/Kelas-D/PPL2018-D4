package com.ppl.d4.temancatat.presenters.interfaces;

import com.ppl.d4.temancatat.models.Product;
import com.ppl.d4.temancatat.ui.views.ProductListView;

import java.util.List;

public interface ProductListPresenterInterface<V extends ProductListView> extends BasePresenterInterface<V> {
    List<Product> getAllProducts();

    List<Product> filterProductsBySKU(String sKU);
}
