package com.ppl.d4.temancatat.models;

public interface TransactionListItem {
    public static final int TYPE_DATE = 0;
    public static final int TYPE_TRANSACTION = 1;

    abstract public int getType();
}
