package com.ppl.d4.temancatat.seeders;

import com.ppl.d4.temancatat.models.Product;
import com.ppl.d4.temancatat.models.StockUnit;
import com.ppl.d4.temancatat.models.manager.RealmDataManager;
import com.ppl.d4.temancatat.presenters.implementations.AddProductPresenter;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Locale;
import java.util.Random;

public class DummyProductSeeder extends Seeder {

    private static final String ALPHABET_STRING = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
    private static final String NUMERIC_STRING = "0123456789";
    private static final String ALPHANUMERIC_STRING = ALPHABET_STRING + NUMERIC_STRING;

    private static final String ID = "DummyProductSeeder";
    private static final String DESCRIPTION = "Seed dummy product data";
    private static final List<Seeder> DEPENDENCIES =
            Collections.unmodifiableList(Arrays.asList(
                    new StockUnitSeeder()
            ));

    public static final int SEED_COUNT = 3;
    public static final int DEFAULT_NAME_LENGTH = 5;
    public static final int DEFAULT_DESC_LENGTH = 10;
    public static final int DEFAULT_PHOTOURL_LENGTH = 10;
    public static final int DEFAULT_SKU_LENGTH = 5;

    public DummyProductSeeder() {
        super(ID, DESCRIPTION, DEPENDENCIES);
    }

    @Override
    public void handleSeed(RealmDataManager realmDataManager) {
        List<StockUnit> stockUnitList = realmDataManager.getAllStockUnits();

        for (int i = 0; i < SEED_COUNT; ++i) {
            Product product = this.generateDummyProduct(
                    DEFAULT_NAME_LENGTH,
                    DEFAULT_DESC_LENGTH,
                    stockUnitList,
                    DEFAULT_PHOTOURL_LENGTH,
                    DEFAULT_SKU_LENGTH
            );
            realmDataManager.syncCreateOrUpdateProduct(product);
        }
    }

    private String generateRandomString(int length, String characters) {
        Random random = new Random();
        StringBuilder builder = new StringBuilder();

        while (length-- != 0) {
            int character = (int)(random.nextDouble() * characters.length());
            builder.append(characters.charAt(character));
        }

        return builder.toString();
    }

    public String generateRandomName(int length) {
        return generateRandomString(length, ALPHABET_STRING + " ");
    }

    public String generateRandomDescription(int length) {
        return generateRandomString(length, ALPHANUMERIC_STRING + " ");
    }

    public String generateRandomPhotoURL(int length) {
        return generateRandomString(length, ALPHABET_STRING);
    }

    public String generateRandomSKU(int length) {
        return generateRandomString(length, ALPHANUMERIC_STRING);
    }

    public Product generateDummyProduct(
            int nameLength,
            int descLength,
            List<StockUnit> stockUnitList,
            int photoURLLength,
            int sKULength
    ) {
        Random random = new Random();
        StockUnit usedStockUnit = stockUnitList.get(random.nextInt(stockUnitList.size()));
        String name = this.generateRandomName(nameLength);
        String sKU = (name + "-" + usedStockUnit.getName()).toUpperCase(Locale.ROOT);

        Product product = new Product(
                name,
                this.generateRandomDescription(descLength),
                usedStockUnit,
                this.generateRandomPhotoURL(photoURLLength)
        );

        product.setSKU(sKU);

        return product;
    }
}
