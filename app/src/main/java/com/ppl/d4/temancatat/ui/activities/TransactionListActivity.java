package com.ppl.d4.temancatat.ui.activities;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.TabLayout;
import android.view.MenuItem;
import android.view.View;

import com.ppl.d4.temancatat.R;
import com.ppl.d4.temancatat.presenters.interfaces.TransactionListPresenterInterface;
import com.ppl.d4.temancatat.ui.adapters.TransactionPagerAdapter;
import com.ppl.d4.temancatat.ui.fragments.ProductListFragment;
import com.ppl.d4.temancatat.ui.pagers.CustomViewPager;
import com.ppl.d4.temancatat.ui.views.TransactionListView;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class TransactionListActivity extends BaseActivity implements TransactionListView {

    private static final String TAG = "TransactionListActivity";

    private static final int TRANSACTION_PAGER_NUM = 0;
    private static final int PRODUCT_PAGER_NUM = 3;
    private static final int STATISTIC_PAGER_NUM = 4;

    private static final int PAGER_NUM = 5;

    private static final int ADD_PRODUCT_CODE = 1;
    private static final int EDIT_PRODUCT_CODE = 2;

    @Inject
    TransactionListPresenterInterface<TransactionListView> mPresenter;

    @Inject
    TransactionPagerAdapter mPagerAdapter;

    @BindView(R.id.transaction_view_pager)
    CustomViewPager mViewPager;

    @BindView(R.id.transaction_tab_layout)
    TabLayout mTabLayout;

    @BindView(R.id.add_new_fabutton)
    FloatingActionButton addNewFloatingActionButton;

    @BindView(R.id.bottom_navigation_view)
    BottomNavigationView bottomNavigationView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_transaction_list);

        getActivityComponent().inject(this);

        setUnBinder(ButterKnife.bind(this));

        mPresenter.onAttach(this);

        getSupportActionBar ().setElevation (0);

        onPreparePagerAdapter();

        onPrepareBottomNavigationView();
    }

    @Override
    public void onPreparePagerAdapter() {

        mPagerAdapter.setCount(PAGER_NUM);

        mViewPager.setAdapter(mPagerAdapter);

        mTabLayout.addTab(mTabLayout.newTab().setText(getString(R.string.all_transaction_tab_text)).setIcon(R.drawable.transaction_default_white));
        mTabLayout.addTab(mTabLayout.newTab().setText(getString(R.string.transaction_in_tab_text)).setIcon(R.drawable.transaction_in_white));
        mTabLayout.addTab(mTabLayout.newTab().setText(getString(R.string.transaction_out_tab_text)).setIcon(R.drawable.transaction_out_white));

        mViewPager.setOffscreenPageLimit(PAGER_NUM);
        mViewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(mTabLayout));
        mTabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                mViewPager.setCurrentItem(tab.getPosition());
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }
            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });
    }

    @Override
    @OnClick(R.id.add_new_fabutton)
    public void onClickAddNewFloatingActionButton() {
        Intent intent;
        if (mViewPager.getCurrentItem() <= 2) {
            intent = new Intent(this, AddTransactionActivity.class);
            this.startActivity(intent);
        }
        else if (mViewPager.getCurrentItem() == PRODUCT_PAGER_NUM) {
            intent = new Intent(this, AddProductActivity.class);
            this.startActivityForResult(intent, ADD_PRODUCT_CODE);
        }

        mPresenter.onAttach(this);
    }

    @Override
    public void onPrepareBottomNavigationView() {
        bottomNavigationView.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                switch (item.getItemId()) {
                    case R.id.nav_transactions_item:
                        if (mViewPager.getCurrentItem() > 2) {
                            TransactionListActivity.this.setTitle(R.string.transaction_list_toolbar_text);
                            showTab();
                            showFloatingActionButton();
                            mViewPager.setCurrentItem(TRANSACTION_PAGER_NUM, false);
                        }

                        return true;
                    case R.id.nav_product_item:
                        TransactionListActivity.this.setTitle(R.string.product_list_toolbar_text);
                        hideTab();
                        showFloatingActionButton();
                        mViewPager.setCurrentItem(PRODUCT_PAGER_NUM, false);
                        ((ProductListFragment)mPagerAdapter.getFragment(PRODUCT_PAGER_NUM)).restoreCompleteProductList();
                        return true;
                    case R.id.nav_statistic_item:
                        TransactionListActivity.this.setTitle(R.string.statistic_toolbar_text);
                        hideTab();
                        hideFloatingActionButton();
                        mViewPager.setCurrentItem(STATISTIC_PAGER_NUM, false);
                        return true;
                }
                return false;
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if(requestCode == ADD_PRODUCT_CODE  || requestCode == EDIT_PRODUCT_CODE) {
            if(resultCode == Activity.RESULT_OK) {
                ((ProductListFragment) mPagerAdapter.getFragment(PRODUCT_PAGER_NUM)).onPrepareProductList();
                ((ProductListFragment) mPagerAdapter.getFragment(PRODUCT_PAGER_NUM)).clearSearchBar();
            }
        }
    }

    @Override
    public void hideTab() {
        mTabLayout.setVisibility(View.GONE);
    }

    @Override
    public void showTab() {
        mTabLayout.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideFloatingActionButton() {
        addNewFloatingActionButton.setVisibility(View.INVISIBLE);
    }

    @Override
    public void showFloatingActionButton() {
        addNewFloatingActionButton.setVisibility(View.VISIBLE);
    }

    @Override
    protected void onDestroy() {
        mPresenter.onDetach();
        super.onDestroy();
    }

    @Override
    protected void closeRealm() {
        mPresenter.closeRealm();
    }
}
