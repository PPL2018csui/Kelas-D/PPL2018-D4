package com.ppl.d4.temancatat.presenters.implementations;

import com.ppl.d4.temancatat.models.ProductStockRecord;
import com.ppl.d4.temancatat.models.TransactionDate;
import com.ppl.d4.temancatat.models.TransactionListItem;
import com.ppl.d4.temancatat.models.manager.RealmDataManager;
import com.ppl.d4.temancatat.presenters.interfaces.TransactionOutPresenterInterface;
import com.ppl.d4.temancatat.ui.views.TransactionOutView;
import com.ppl.d4.temancatat.utils.DateUtil;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import javax.inject.Inject;

public class TransactionOutPresenter<V extends TransactionOutView> extends BasePresenter<V> implements TransactionOutPresenterInterface<V>, RealmDataManager.OnTransactionCallback{

    @Inject
    public TransactionOutPresenter(RealmDataManager realmDataManager) {
        super(realmDataManager);
    }

    @Override
    public void closeRealm() {
        super.closeRealm();
    }

    @Override
    public void onRealmSuccess() {

    }

    @Override
    public void onRealmError(Throwable e) {

    }

    @Override
    public List<TransactionListItem> getTransactionOutItems() {
        List<ProductStockRecord> productStockRecordList = getRealmDataManager().getTransactionOutProductStockRecords();

        List<TransactionListItem> transactionListItems = new ArrayList<>();
        String date = "";
        for (ProductStockRecord productStockRecord: productStockRecordList) {
            String dateCursor = DateUtil.formatToDMY(productStockRecord.getDate()).toUpperCase(Locale.ROOT);
            if (date.equalsIgnoreCase(dateCursor)) {
                transactionListItems.add(productStockRecord);
            } else {
                date = dateCursor;
                TransactionDate transactionDate = new TransactionDate(date);
                transactionListItems.add(transactionDate);
                transactionListItems.add(productStockRecord);
            }
        }

        return transactionListItems;
    }
}