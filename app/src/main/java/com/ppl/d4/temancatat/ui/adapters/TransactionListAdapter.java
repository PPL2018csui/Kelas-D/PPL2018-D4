package com.ppl.d4.temancatat.ui.adapters;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.ppl.d4.temancatat.R;
import com.ppl.d4.temancatat.models.ProductStockRecord;
import com.ppl.d4.temancatat.models.TransactionDate;
import com.ppl.d4.temancatat.models.TransactionListItem;
import com.ppl.d4.temancatat.ui.activities.AddTransactionActivity;
import com.ppl.d4.temancatat.utils.PriceUtil;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class TransactionListAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private static final String TAG = "TransactionListAdapter";

    private Context context;
    private List<TransactionListItem> transactionListItems;

    public TransactionListAdapter(Context context, List<TransactionListItem> transactionListItems) {
        this.context = context;
        this.transactionListItems = transactionListItems;
    }

    public void onOpenEditTransaction(String transactionId) {
        Intent intent = new Intent(context, AddTransactionActivity.class);
        intent.putExtra("EXTRA_TRANSACTION_ID", transactionId);
        intent.putExtra("EXTRA_MODE", "VIEW");

        context.startActivity(intent);
    }

    class TransactionItemViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.transaction_thumbnail_imageview)
        ImageView transactionThumbnailImageView;

        @BindView(R.id.transaction_name_textview)
        TextView transactionNameTextView;

        @BindView(R.id.transaction_amount_textview)
        TextView transactionAmountTextView;

        @BindView(R.id.transaction_price_textview)
        TextView transactionPriceTextView;

        @BindView(R.id.transaction_status)
        TextView transactionStatusTextView;

        @BindView(R.id.transaction_item_layout)
        LinearLayout transactionItemLayout;

        public TransactionItemViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

    class TransactionDateViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.transaction_date_textview)
        TextView transactionDateTextView;

        @BindView(R.id.transaction_date_layout)
        LinearLayout transactionDateLayout;

        public TransactionDateViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        RecyclerView.ViewHolder viewHolder = null;
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());

        switch(viewType) {

            case TransactionListItem.TYPE_DATE:
                View viewDate = inflater.inflate(R.layout.transaction_date, parent, false);
                viewHolder = new TransactionDateViewHolder(viewDate);
                break;

            case TransactionListItem.TYPE_TRANSACTION:
                View viewTransaction = inflater.inflate(R.layout.transaction_item, parent, false);
                viewHolder = new TransactionItemViewHolder(viewTransaction);
                break;
        }

        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder viewHolder, int position) {

        switch(viewHolder.getItemViewType()) {

            case TransactionListItem.TYPE_TRANSACTION:

                ProductStockRecord productStockRecord = (ProductStockRecord) transactionListItems.get(position);
                TransactionItemViewHolder transactionItemViewHolder = (TransactionItemViewHolder) viewHolder;
                transactionItemViewHolder.transactionNameTextView.setText(productStockRecord.getProduct().getSKU());
                transactionItemViewHolder.transactionAmountTextView.setText(
                        "Banyaknya: "
                                + Integer.toString(productStockRecord.getStocks())
                                + " "
                                + productStockRecord.getProduct().getStockUnit().getName()
                );
                transactionItemViewHolder.transactionPriceTextView.setText(
                        "Harga Satuan: "
                                + PriceUtil.convertPriceFormatWithCurrency(
                                        productStockRecord.getPrice(),"Rp","ID"
                                  )
                );
                String transactionType = getStatusTextByTransactionType(productStockRecord.getTransactionType());
                transactionItemViewHolder.transactionStatusTextView.setText(transactionType);

                if (transactionType.equals("Keluar")) {
                    transactionItemViewHolder.transactionStatusTextView.setTextColor(
                            context.getResources().getColor(R.color.colorTextWarning)
                    );
                }
                transactionItemViewHolder.transactionItemLayout.setOnClickListener(
                        v -> this.onOpenEditTransaction(
                                productStockRecord.getId()
                        )
                );

                if (transactionItemViewHolder.transactionStatusTextView.getText().equals("Masuk")) {
                    transactionItemViewHolder.transactionThumbnailImageView.setImageResource(R.drawable.transaction_in_thumbnail);
                } else {
                    transactionItemViewHolder.transactionThumbnailImageView.setImageResource(R.drawable.transaction_out_thumbnail);
                }
                break;

            case TransactionListItem.TYPE_DATE:

                TransactionDate transactionDate = (TransactionDate) transactionListItems.get(position);
                TransactionDateViewHolder transactionDateViewHolder = (TransactionDateViewHolder) viewHolder;
                transactionDateViewHolder.transactionDateTextView.setText(transactionDate.getDate());
                break;

        }
    }

    @Override
    public int getItemCount() {
        return transactionListItems.size();
    }

    @Override
    public int getItemViewType(int position) { return transactionListItems.get(position).getType(); }

    private String getStatusTextByTransactionType(ProductStockRecord.TransactionType type) {
        if (type == ProductStockRecord.TransactionType.TRANSACTION_IN) {
            return "Masuk";
        }
        else if (type == ProductStockRecord.TransactionType.TRANSACTION_OUT) {
            return "Keluar";
        }

        return "Undefined";
    }
}
