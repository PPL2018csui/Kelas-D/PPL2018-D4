package com.ppl.d4.temancatat.ui.views;

import java.util.Date;

public interface AddTransactionView extends BaseView {

    void saveTransactionButtonClick();

    Date getTransactionDate();

    Integer getStockAmount();
    Double getPrice();

    void openTransactionListActivity();

    void setInvalidProductError();
    void setInvalidAmountError();
    void setNotEnoughStockError(int currentStock);
    void setInvalidPriceError();
    void setEditTransactionStockError();

    void onAddModeChanged();
    void onViewModeChanged();
    void onEditModeChanged();

    void cleanProductError();
    void cleanAmountError();
    void cleanPriceError();
    void openSearchProductActivity();
    void showToastMessage(int stringResourceId, int lengthModifier);
    void finish();
}
