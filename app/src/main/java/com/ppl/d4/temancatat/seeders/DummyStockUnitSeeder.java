package com.ppl.d4.temancatat.seeders;

import java.util.Random;

import com.ppl.d4.temancatat.models.StockUnit;
import com.ppl.d4.temancatat.models.manager.RealmDataManager;

public class DummyStockUnitSeeder extends Seeder {

    private static final String ALPHABET_STRING = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
    private static final double spaceCharProbability = 0.2;

    private static final String ID = "DummyStockUnitSeeder";
    private static final String DESCRIPTION = "Seed dummy stock unit data";

    public static final int SEED_COUNT = 10;
    public static final int DEFAULT_NAME_LENGTH = 10;

    public DummyStockUnitSeeder() {
        super(ID, DESCRIPTION);
    }

    @Override
    public void handleSeed(RealmDataManager realmDataManager) {
        for (int i = 0; i < SEED_COUNT; ++i) {
            StockUnit stockUnit = this.generateDummyStockUnit(DEFAULT_NAME_LENGTH);
            realmDataManager.createStockUnit(stockUnit);
        }
    }

    public String generateRandomName(int length) {
        Random random = new Random();
        StringBuilder builder = new StringBuilder();

        while (length-- != 0) {
            boolean isSpaceChar = (random.nextDouble() <= spaceCharProbability);
            if (isSpaceChar) {
                builder.append(' ');
            }
            else {
                int character = (int)(random.nextDouble() * ALPHABET_STRING.length());
                builder.append(ALPHABET_STRING.charAt(character));
            }
        }

        return builder.toString();
    }

    public StockUnit generateDummyStockUnit(int nameLength) {
        return new StockUnit(this.generateRandomName(nameLength));
    }
}