package com.ppl.d4.temancatat.ui.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.TextInputLayout;
import android.support.percent.PercentRelativeLayout;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.github.mikephil.charting.charts.BarChart;
import com.github.mikephil.charting.components.AxisBase;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.data.BarData;
import com.github.mikephil.charting.data.BarDataSet;
import com.github.mikephil.charting.data.BarEntry;
import com.github.mikephil.charting.formatter.IAxisValueFormatter;
import com.ppl.d4.temancatat.R;
import com.ppl.d4.temancatat.models.Product;
import com.ppl.d4.temancatat.presenters.implementations.EditProductPresenter;
import com.ppl.d4.temancatat.ui.views.EditProductView;
import com.ppl.d4.temancatat.utils.AppException;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.OnTextChanged;

public class EditProductActivity extends BaseActivity implements EditProductView {

    private static final String TAG = "Edit Product Activity";
    private String stockUnitName;

    @Inject
    EditProductPresenter<EditProductView> mPresenter;

    @BindView(R.id.product_unit_spinner)
    Spinner productUnitSpinner;

    @BindView(R.id.add_product_name_editText)
    TextView addProductNameEditText;

    @BindView(R.id.add_product_desc_editText)
    TextView addProductDescEditText;

    @BindView(R.id.add_product_SKU_editText)
    TextView addProductSKUEditText;

    @BindView(R.id.product_SKU_textinputlayout)
    TextInputLayout productSKUTextInputLayout;

    @BindView(R.id.product_name_textinputlayout)
    TextInputLayout productNameTextInputLayout;

    @BindView(R.id.product_desc_textinputlayout)
    TextInputLayout productDescTextInputLayout;

    @BindView(R.id.product_preview_textview)
    TextView productPreviewTextView;

    @BindView(R.id.add_product_save_button)
    Button saveButton;

    @BindView(R.id.current_stock_value_textview)
    TextView currentStockValueTextView;

    @BindView(R.id.current_transaction_in_count_textview)
    TextView currentTransactionInCountTextView;

    @BindView(R.id.current_transaction_out_count_textview)
    TextView currentTransactionOutCountTextView;

    @BindView(R.id.footer)
    PercentRelativeLayout footer;

    @BindView(R.id.statistic_layout)
    LinearLayout statisticLayout;

    @BindView(R.id.bar_chart)
    BarChart barChart;

    @Override
    @OnClick(R.id.add_product_save_button)
    public void onSaveProduct() {
        String productName = addProductNameEditText.getText().toString();
        String productDesc = addProductDescEditText.getText().toString();
        String productSKU = addProductSKUEditText.getText().toString();
        String stockUnitName = productUnitSpinner.getSelectedItem().toString();

        try {
            Intent intent = new Intent(this, SearchProductActivity.class);
            setResult(RESULT_OK, intent);
            mPresenter.onSaveProduct(productName, productDesc, stockUnitName, productSKU);
        }
        catch (AppException e) {
            showErrorMessage(e.getErrorMessage());
        }
    }

    @Override
    @OnTextChanged(R.id.add_product_SKU_editText)
    public void updateProductPreview(CharSequence text) {
        productPreviewTextView.setText(text.toString());
    }

    @Override
    public void clearErrorOnAllFields() {
        productNameTextInputLayout.setError(null);
        productSKUTextInputLayout.setError(null);
        productDescTextInputLayout.setError(null);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_product);

        getActivityComponent().inject(this);

        setUnBinder(ButterKnife.bind(this));
        ButterKnife.bind(this);

        mPresenter.onAttach(this);
        mPresenter.setProductBySKU(getIntent().getStringExtra("productSKU"));
        mPresenter.changeMode(getIntent().getStringExtra("EXTRA_MODE"));

        stockUnitName = (getIntent().getStringExtra("productStockUnitName"));

        prepareStockUnit();

    }

    @Override
    public void prepareStockUnit() {
        List<String> stockUnitsName = mPresenter.getStockUnitsName();

        ArrayAdapter<String> adapter = new ArrayAdapter<>(this,
                R.layout.spinner_item,
                stockUnitsName);


        adapter.setDropDownViewResource(R.layout.spinner_dropdown_item);
        productUnitSpinner.setAdapter(adapter);
        productUnitSpinner.setSelection(adapter.getPosition(stockUnitName));
    }

    @Override
    public void onErrorProductNameField(int messageId) {
        productNameTextInputLayout.setError(getResources().getString(messageId));
        addProductNameEditText.requestFocus();
    }

    @Override
    public void onErrorProductSKUField(int messageId) {
        productSKUTextInputLayout.setError(getResources().getString(messageId));
        addProductSKUEditText.requestFocus();
    }

    @Override
    public void onErrorProductDescriptionField(int messageId) {
        productDescTextInputLayout.setError(getResources().getString(messageId));
        addProductDescEditText.requestFocus();
    }

    protected void disableForm() {
        addProductSKUEditText.setEnabled(false);
        addProductDescEditText.setEnabled(false);
        addProductNameEditText.setEnabled(false);
        productUnitSpinner.setEnabled(false);
    }

    protected void enableForm() {
        addProductDescEditText.setEnabled(true);
        addProductNameEditText.setEnabled(true);
        productUnitSpinner.setEnabled(true);
    }

    protected void prefillFormData(Product product) {
        clearErrorOnAllFields();
        addProductSKUEditText.setText(product.getSKU());
        addProductDescEditText.setText(product.getDescription());
        addProductNameEditText.setText(product.getName());
    }

    protected void prefillStatisticData(String productSKU) {
        currentStockValueTextView.setText(Integer.toString(mPresenter.getStockValue(productSKU)));
        currentTransactionInCountTextView.setText(Integer.toString((mPresenter.getStockTransactionIn((productSKU)))));
        currentTransactionOutCountTextView.setText(Integer.toString((mPresenter.getStockTransactionOut(productSKU))));
    }

    @Override
    public void onViewModeChanged() {
        getSupportActionBar().setTitle(getResources().getString(R.string.view_product_menu));
        footer.setVisibility(View.GONE);
        statisticLayout.setVisibility(View.VISIBLE);
        makeChart(mPresenter.getProduct().getSKU());
        prefillFormData(mPresenter.getProduct());
        prefillStatisticData(mPresenter.getProduct().getSKU());
        disableForm();
    }

    @Override
    public void onEditModeChanged() {
        getSupportActionBar().setTitle(getResources().getString(R.string.edit_product_menu));
        footer.setVisibility(View.VISIBLE);
        statisticLayout.setVisibility(View.GONE);
        prefillFormData(mPresenter.getProduct());
        prefillStatisticData(mPresenter.getProduct().getSKU());
        enableForm();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.edit_product_menu, menu);

        for (int i = 0; i < menu.size(); i++) {
            menu.getItem(i).setVisible(true);
        }

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                Intent intent = new Intent(this, TransactionListActivity.class);
                setResult(RESULT_OK, intent);
                finish();
                return true;
            case R.id.edit_product_item:
                mPresenter.toggleEdit();
                return true;
            default:
                return super.onOptionsItemSelected(item);

        }
    }

    @Override
    public void showToastMessage(int stringResourceId, int lengthModifier) {
        Toast.makeText(getApplicationContext(), stringResourceId, lengthModifier).show();
    }

    @Override
    protected void onDestroy() {
        mPresenter.onDetach();
        super.onDestroy();
    }

    @Override
    protected void closeRealm() {
        mPresenter.closeRealm();
    }

    public void makeChart(String productSKU){
        Map<String,Integer> monthAndStock = mPresenter.getMonthlyStock(productSKU);
        List<String> monthKeys = new ArrayList<String>(monthAndStock.keySet());

        ArrayList<BarEntry> barEntry = new ArrayList<>();
        ArrayList<String> labels = new ArrayList<>();

        final int LIMIT_MONTH = 6;
        int indexLatestMonth = monthKeys.size() - LIMIT_MONTH;
        if( indexLatestMonth < 0) {
            indexLatestMonth = 0;
        }

        int xIndex = 0;
        while(indexLatestMonth < LIMIT_MONTH || indexLatestMonth < monthKeys.size()) {
            String month = getResources().getString(R.string.xAxis_label_month_none);
            int stock = 0;

            if(indexLatestMonth < monthKeys.size()) {
                month = monthKeys.get(indexLatestMonth);
                stock = monthAndStock.get(monthKeys.get(indexLatestMonth));
            }

            barEntry.add(new BarEntry((float) xIndex,stock));
            labels.add(month);

            indexLatestMonth++;
            xIndex++;
        }

        XAxis xAxis = barChart.getXAxis();
        xAxis.setPosition(XAxis.XAxisPosition.TOP);
        xAxis.setValueFormatter(new IAxisValueFormatter() {
            @Override
            public String getFormattedValue(float value, AxisBase axis) {
                return labels.get((int)value);
            }
        });

        BarDataSet dataSet = new BarDataSet(barEntry, getResources().getString(R.string.bar_dataset_label));
        BarData data = new BarData(dataSet);
        data.setBarWidth(0.45f);

        dataSet.setColors(new int [] { R.color.colorTertiaryLightPurple});

        barChart.getAxisLeft().setAxisMinimum(0);
        barChart.getAxisRight().setEnabled(false);
        barChart.setDrawBorders(true);
        barChart.setData(data);
        barChart.setFitBars(true);
        barChart.setDescription(null);
        barChart.invalidate();

    }
}
