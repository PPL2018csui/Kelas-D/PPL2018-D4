package com.ppl.d4.temancatat.ui.views;

/**
 * Interface that is implemented by activity.
 * Contains method given to its presenter.
 * It is one-to-one relationship between activity.
 * Other views have to extend BaseView.
 */
public interface BaseView {

    void showErrorMessage(String message);

    void showErrorMessage(int messageId);

    void onLoading();

    void onFinishLoading();
}
