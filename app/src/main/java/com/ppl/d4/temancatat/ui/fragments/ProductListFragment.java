package com.ppl.d4.temancatat.ui.fragments;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.ppl.d4.temancatat.R;
import com.ppl.d4.temancatat.di.components.ActivityComponent;
import com.ppl.d4.temancatat.models.Product;
import com.ppl.d4.temancatat.presenters.interfaces.ProductListPresenterInterface;
import com.ppl.d4.temancatat.ui.adapters.ProductListAdapter;
import com.ppl.d4.temancatat.ui.views.ProductListView;

import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ProductListFragment extends BaseFragment implements ProductListView {

    private static final String TAG = "Product List Fragment";

    private static final int ADD_PRODUCT_CODE = 1;

    private SearchView searchView;

    private List<Product> completeProductList;
    private List<Product> currentProductList;

    @Inject
    ProductListPresenterInterface<ProductListView> mPresenter;

    @BindView(R.id.product_list_recyclerview)
    RecyclerView productListRecyclerView;

    @BindView(R.id.no_product_linearlayout)
    LinearLayout noProductLinearLayout;

    private ProductListAdapter mProductListAdapter;

    public static ProductListFragment newInstance() {
        Bundle args = new Bundle();
        ProductListFragment fragment = new ProductListFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        getActivity().getMenuInflater().inflate(R.menu.search_product_menu, menu);
    }

    @Override
    public void onPrepareOptionsMenu(Menu menu) {
        super.onPrepareOptionsMenu(menu);

        onPrepareSearchView(menu);
    }

    @Override
    public void onPrepareProductList() {
        List<Product> productList = mPresenter.getAllProducts();
        completeProductList = productList;
        currentProductList = productList;

        if (productList.size() == 0) {
            showNoProductPlaceholder();
        }
        else {
            showProductList(productList);
        }
    }

    @Override
    public void restoreCompleteProductList() {
        completeProductList = mPresenter.getAllProducts();
        if (completeProductList == null || currentProductList == null) {
            onPrepareProductList();
        }
        else if (completeProductList.size() == 0) {
            showNoProductPlaceholder();
        }
        else if (currentProductList.size() != completeProductList.size()) {
            showProductList(completeProductList);
        }
    }

    @Override
    public void showProductList(List<Product> productList) {
        noProductLinearLayout.setVisibility(View.INVISIBLE);
        productListRecyclerView.setVisibility(View.VISIBLE);

        LinearLayoutManager layoutManager
                = new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false);

        productListRecyclerView.setLayoutManager(layoutManager);

        mProductListAdapter = new ProductListAdapter(getContext(), productList, ProductListAdapter.LIST_PRODUCT_MODE);

        productListRecyclerView.setAdapter(mProductListAdapter);
    }

    @Override
    public void showNoProductPlaceholder() {
        productListRecyclerView.setVisibility(View.INVISIBLE);
        noProductLinearLayout.setVisibility(View.VISIBLE);
    }

    public void onPrepareSearchView(Menu menu) {
        MenuItem searchViewMenuItem = menu.findItem(R.id.search_product_item);
        searchView = (SearchView) searchViewMenuItem.getActionView();

        searchView.setQueryHint(getResources().getString(R.string.search_product_string));

        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                List<Product> productList = mPresenter.filterProductsBySKU(query);
                currentProductList = productList;
                if (mProductListAdapter != null) {
                    mProductListAdapter.setProductList(productList);
                }
                return true;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                List<Product> productList = mPresenter.filterProductsBySKU(newText);
                currentProductList = productList;
                if (mProductListAdapter != null) {
                    mProductListAdapter.setProductList(productList);
                }
                return false;
            }
        });
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.product_list_fragment, container, false);
        ActivityComponent component = getActivityComponent();
        if (component != null) {
            component.inject(this);
            setUnBinder(ButterKnife.bind(this,view));
            mPresenter.onAttach(this);
        }

        onPrepareProductList();

        return view;
    }

    @Override
    protected void setUp(View view) {

    }

    @Override
    public void clearSearchBar() {
        if (searchView == null)
            return;

        if (!searchView.isIconified()) {
            searchView.setQuery("", false);
            searchView.setIconified(true);
            onPrepareProductList();
        }
    }

    @Override
    public void onStart() {
        super.onStart();
    }
}
