package com.ppl.d4.temancatat.presenters.interfaces;

import com.ppl.d4.temancatat.ui.views.TransactionListView;

public interface TransactionListPresenterInterface<V extends TransactionListView> extends BasePresenterInterface<V> {

}
