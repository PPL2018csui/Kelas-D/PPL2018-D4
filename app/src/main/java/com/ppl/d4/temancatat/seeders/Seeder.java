package com.ppl.d4.temancatat.seeders;

import com.ppl.d4.temancatat.models.SeederTransaction;
import com.ppl.d4.temancatat.models.manager.RealmDataManager;

import java.util.ArrayList;
import java.util.List;

public class Seeder {
    private String id;
    private String description;
    private List<Seeder> dependencies;

    public Seeder(String id, String description) {
        this.id = id;
        this.description = description;
        this.dependencies = new ArrayList<Seeder>();
    }

    public Seeder(String id, String description, List<Seeder> dependencies) {
        this(id, description);
        this.dependencies = dependencies;
    }

    public String getId() {
        return this.id;
    }
    public String getDescription() {
        return this.description;
    }
    public List<Seeder> getDependencies() {
        return this.dependencies;
    }

    public void seed(RealmDataManager realmDataManager) {
        if (this.isAlreadyExecuted(realmDataManager))
            return;

        this.seedDependencies(realmDataManager);
        this.handleSeed(realmDataManager);
        this.commitTransaction(realmDataManager);
    }

    public void handleSeed(RealmDataManager realmDataManager) {
        return;
    }

    public boolean isAlreadyExecuted(RealmDataManager realmDataManager) {
        SeederTransaction transaction = realmDataManager.getSeederTransactionById(this.id);

        if (transaction != null)
            return true;
        else
            return false;
    }

    public void commitTransaction(RealmDataManager realmDataManager) {
        SeederTransaction transaction = new SeederTransaction(this.id, this.description);
        realmDataManager.createSeederTransaction(transaction);
    }

    public void seedDependencies(RealmDataManager realmDataManager) {
        for (Seeder dependency: this.getDependencies()) {
            dependency.seed(realmDataManager);
        }
    }
}
