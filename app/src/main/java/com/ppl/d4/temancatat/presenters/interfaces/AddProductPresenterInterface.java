package com.ppl.d4.temancatat.presenters.interfaces;

import com.ppl.d4.temancatat.models.StockUnit;
import com.ppl.d4.temancatat.ui.views.AddProductView;
import com.ppl.d4.temancatat.utils.AppException;

import java.util.List;

public interface AddProductPresenterInterface<V extends AddProductView> extends BasePresenterInterface<V> {

    List<String> getStockUnitsName();

    void onSaveProduct(String name, String description, String stockUnitName, String photoUrl) throws AppException;

    StockUnit getStockUnitByName(String name);

    String generateDefaultSKU(String productName, String productStockUnit);

    boolean isText(String editTextString, boolean required);
    boolean isStockUnitPresent(StockUnit stockUnit);
    boolean isNumber(String editTextString, boolean required);
}
