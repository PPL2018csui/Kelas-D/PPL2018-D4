package com.ppl.d4.temancatat.di.modules;

import android.content.Context;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;

import com.ppl.d4.temancatat.di.ActivityContext;
import com.ppl.d4.temancatat.di.PerActivity;
import com.ppl.d4.temancatat.models.manager.RealmDataManager;

import com.ppl.d4.temancatat.presenters.implementations.AddProductPresenter;
import com.ppl.d4.temancatat.presenters.implementations.AddTransactionPresenter;
import com.ppl.d4.temancatat.presenters.implementations.AllTransactionPresenter;
import com.ppl.d4.temancatat.presenters.implementations.OverallStatisticsPresenter;
import com.ppl.d4.temancatat.presenters.implementations.ProductListPresenter;
import com.ppl.d4.temancatat.presenters.implementations.TransactionInPresenter;
import com.ppl.d4.temancatat.presenters.implementations.TransactionOutPresenter;
import com.ppl.d4.temancatat.presenters.implementations.SearchProductPresenter;
import com.ppl.d4.temancatat.presenters.implementations.TransactionListPresenter;

import com.ppl.d4.temancatat.presenters.interfaces.AddProductPresenterInterface;
import com.ppl.d4.temancatat.presenters.interfaces.AddTransactionPresenterInterface;
import com.ppl.d4.temancatat.presenters.interfaces.AllTransactionPresenterInterface;
import com.ppl.d4.temancatat.presenters.interfaces.OverallStatisticsPresenterInterface;
import com.ppl.d4.temancatat.presenters.interfaces.ProductListPresenterInterface;
import com.ppl.d4.temancatat.presenters.interfaces.TransactionInPresenterInterface;
import com.ppl.d4.temancatat.presenters.interfaces.TransactionOutPresenterInterface;
import com.ppl.d4.temancatat.presenters.interfaces.SearchProductPresenterInterface;
import com.ppl.d4.temancatat.presenters.interfaces.TransactionListPresenterInterface;

import com.ppl.d4.temancatat.ui.adapters.TransactionPagerAdapter;

import com.ppl.d4.temancatat.ui.views.AddProductView;
import com.ppl.d4.temancatat.ui.views.AddTransactionView;
import com.ppl.d4.temancatat.ui.views.AllTransactionView;
import com.ppl.d4.temancatat.ui.views.OverallStatisticsView;
import com.ppl.d4.temancatat.ui.views.ProductListView;
import com.ppl.d4.temancatat.ui.views.TransactionInView;
import com.ppl.d4.temancatat.ui.views.TransactionOutView;
import com.ppl.d4.temancatat.ui.views.TransactionListView;
import com.ppl.d4.temancatat.ui.views.SearchProductView;

import dagger.Module;
import dagger.Provides;
import io.realm.Realm;

/**
 * Class to provide modules to application.
 * Also used to provide presenter to activities.
 * Don't forget to provide newly added presenter in this class.
 */
@Module
public class ActivityModule {

    private AppCompatActivity mActivity;

    public ActivityModule(AppCompatActivity activity) {
        this.mActivity = activity;
    }

    @Provides
    @ActivityContext
    Context provideContext() {
        return mActivity;
    }

    @Provides
    AppCompatActivity provideActivity() {
        return mActivity;
    }

    @Provides
    TransactionListPresenterInterface<TransactionListView> provideTransactionListPresenter(
            TransactionListPresenter<TransactionListView> presenter) {
        return presenter;
    }

    @Provides
    AllTransactionPresenterInterface<AllTransactionView> provideAllTransactionPresenterInterface(
            AllTransactionPresenter<AllTransactionView> presenter) {
        return presenter;
    }

    @Provides
    TransactionInPresenterInterface<TransactionInView> provideTransactionInPresenterInterface(
            TransactionInPresenter<TransactionInView> presenter) {
        return presenter;
    }

    @Provides
    TransactionOutPresenterInterface<TransactionOutView> provideTransactionOutPresenterInterface(
            TransactionOutPresenter<TransactionOutView> presenter) {
        return presenter;
    }

    @Provides
    OverallStatisticsPresenterInterface<OverallStatisticsView> provideOverallStatisticsPresenterInterface(
            OverallStatisticsPresenter<OverallStatisticsView> presenter) {
        return presenter;
    }

    @Provides
    ProductListPresenterInterface<ProductListView> provideProductListPresenterInterface(
            ProductListPresenter<ProductListView> presenter) {
        return presenter;
    }


    @Provides
    @PerActivity
    AddTransactionPresenterInterface<AddTransactionView> provideAddTransactionPresenter(
            AddTransactionPresenter<AddTransactionView> presenter) {
        return presenter;
    }

    @Provides
    @PerActivity
    AddProductPresenterInterface<AddProductView> provideAddProductPresenter(
            AddProductPresenter<AddProductView> presenter) {
        return presenter;
    }

    @Provides
    TransactionPagerAdapter provideTransactionPagerAdapter(AppCompatActivity activity) {
        return new TransactionPagerAdapter(activity.getSupportFragmentManager());
    }

    @Provides
    LinearLayoutManager provideLinearLayoutManager(AppCompatActivity activity) {
        return new LinearLayoutManager(activity);
    }

    @Provides
    @PerActivity
    SearchProductPresenterInterface<SearchProductView> provideSearchProductPresenter(
            SearchProductPresenter<SearchProductView> presenter){
        return presenter;
    }

    @Provides
    Realm provideRealm() {
        return Realm.getDefaultInstance();
    }

    @Provides
    RealmDataManager provideRealmService(final Realm realm) {
        return new RealmDataManager(realm);
    }
}