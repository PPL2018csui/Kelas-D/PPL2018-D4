package com.ppl.d4.temancatat.presenters.interfaces;

import android.support.v7.widget.RecyclerView;
import android.widget.ListView;

import com.github.mikephil.charting.charts.PieChart;
import com.github.mikephil.charting.data.PieEntry;
import com.ppl.d4.temancatat.models.LegendItem;
import com.ppl.d4.temancatat.ui.views.OverallStatisticsView;

import java.util.List;

public interface OverallStatisticsPresenterInterface<V extends OverallStatisticsView> extends BasePresenterInterface<V> {

    double getTotalPurchasesAmount();

    double getTotalSalesAmount();

    double getNetRevenueAmount();

    void onPreparePieChartData();

    void showPieChart(PieChart pieChart, int mode);

    List<LegendItem> getLegendItems(int mode);

    boolean checkIsExistTransaction();

    boolean checkIsExistTransactionOut();

    boolean checkIsExistTransactionIn();
}