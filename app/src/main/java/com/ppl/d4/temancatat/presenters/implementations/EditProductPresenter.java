package com.ppl.d4.temancatat.presenters.implementations;

import android.widget.Toast;

import com.ppl.d4.temancatat.R;
import com.ppl.d4.temancatat.models.Product;
import com.ppl.d4.temancatat.models.ProductStockRecord;
import com.ppl.d4.temancatat.models.StockUnit;
import com.ppl.d4.temancatat.models.manager.RealmDataManager;
import com.ppl.d4.temancatat.presenters.interfaces.EditProductPresenterInterface;
import com.ppl.d4.temancatat.ui.views.EditProductView;
import com.ppl.d4.temancatat.utils.AppException;
import com.ppl.d4.temancatat.utils.DateUtil;
import com.ppl.d4.temancatat.utils.Error;

import org.apache.commons.lang3.math.NumberUtils;

import java.time.Month;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import javax.inject.Inject;

public class EditProductPresenter<V extends EditProductView> extends BasePresenter<V> implements EditProductPresenterInterface<V>, RealmDataManager.OnTransactionCallback {

    private static final String TAG = "Edit Product Presenter";
    public static final String VIEW_MODE = "VIEW";
    public static final String EDIT_MODE = "EDIT";

    private String mode;
    private Product product;

    @Inject
    public EditProductPresenter(final RealmDataManager realmDataManager) {
        super(realmDataManager);
    }

    @Override
    public void onRealmSuccess() {
        getView().onFinishLoading();
        getView().showToastMessage(R.string.change_product_toast_message, Toast.LENGTH_SHORT);
        getView().finish();
    }

    @Override
    public void onRealmError(Throwable e) {
        getView().showErrorMessage(e.toString());
    }

    @Override
    public void setProductBySKU(String productSKU) throws AppException {
        if (productSKU == null || (product != null && productSKU.equals(product.getSKU())))
            return;

        product = getRealmDataManager().getProductBySKU(productSKU);
        if (product == null)
            throw new AppException(Error.FAILED_DATA_RETRIEVAL);
    }

    @Override
    public List<String> getStockUnitsName() {
        List<StockUnit> stockUnits = getRealmDataManager().getAllStockUnits();
        List<String> stockUnitsName = new ArrayList<>();

        for (StockUnit stockUnit: stockUnits) {
            stockUnitsName.add(stockUnit.getName());
        }

        return stockUnitsName;
    }

    @Override
    public void changeMode(String mode) {
        if (mode == null)
            mode = VIEW_MODE;

        if (mode.equals(this.mode))
            return;

        this.mode = mode;
        if (mode.equals(VIEW_MODE))
            getView().onViewModeChanged();
        else if (mode.equals(EDIT_MODE))
            getView().onEditModeChanged();
    }

    @Override
    public Product getProduct() {
        return product;
    }

    @Override
    public void toggleEdit() {
        if (mode.equals(EDIT_MODE))
            changeMode(VIEW_MODE);
        else if (mode.equals(VIEW_MODE))
            changeMode(EDIT_MODE);
    }

    @Override
    public String getMode() {
        return mode;
    }

    @Override
    public void onSaveProduct(String name, String description, String stockUnitName, String productSKU) throws AppException {

        getView().clearErrorOnAllFields();

        boolean hasError = false;

        if (!isText(name, true)) {
            getView().onErrorProductNameField(R.string.error_message_name_field_empty);
            hasError = true;
        }

        if (!isText(productSKU, true)) {
            getView().onErrorProductSKUField(R.string.error_message_SKU_field_empty);
            hasError = true;
        }

        if (!isText(description, true)) {
            getView().onErrorProductDescriptionField(R.string.error_message_desc_field_empty);
            hasError = true;
        }

        StockUnit stockUnit = getStockUnitByName(stockUnitName);

        if (!isStockUnitPresent(stockUnit)) {
            hasError = true;
        }

        if (hasError) {
            throw new AppException(Error.USER_INVALID_INPUT);
        }

        product.setName(name);
        product.setDescription(description);
        product.setStockUnit(stockUnit);

        getView().onLoading();
        getRealmDataManager().createOrUpdateProduct(product, this);
    }

    @Override
    public StockUnit getStockUnitByName(String name) {
        return getRealmDataManager().getStockUnitByName(name);
    }

    @Override
    public String generateDefaultSKU(String productName, String productStockUnit) {
        return (productName + "-" + productStockUnit).toUpperCase(Locale.ROOT);
    }

    @Override
    public void closeRealm() {
        super.closeRealm();
    }

    @Override
    public boolean isStockUnitPresent(StockUnit stockUnit) {
        if (stockUnit == null) {
            return false;
        }
        return true;
    }

    @Override
    public boolean isText(String editTextString, boolean required) {

        if ( required && editTextString.isEmpty()) {
            return false;
        }

        return true;
    }

    @Override
    public boolean isNumber(String editTextString, boolean required) {
        String number = editTextString;

        if(!required) {
            return true;
        } else if (number.isEmpty()) {
            return false;
        }

        if (NumberUtils.isNumber(number)) {
            return true;
        }

        return false;
    }

    @Override
    public int getStockValue(String productSKU) {
        return getRealmDataManager().getCurrentProductStockByProductSKU(productSKU);
    }

    @Override
    public int getStockTransactionIn(String productSKU) {
        List<ProductStockRecord> productStockRecordInList = getRealmDataManager().getProductStockRecordsInByProductSKU(productSKU);

        int cumulativeStocks = 0;
        for (ProductStockRecord productStockRecord: productStockRecordInList) {
            cumulativeStocks += productStockRecord.getStocks();
        }
        return cumulativeStocks;
    }

    @Override
    public int getStockTransactionOut(String productSKU) {
        List<ProductStockRecord> productStockRecordOutList = getRealmDataManager().getProductStockRecordsOutByProductSKU(productSKU);

        int cumulativeStocks = 0;
        for (ProductStockRecord productStockRecord: productStockRecordOutList) {
            cumulativeStocks += productStockRecord.getStocks();
        }
        return cumulativeStocks;
    }

    @Override
    public Map<String, Integer> getMonthlyStock(String productSKU) {
        List<ProductStockRecord> productStockRecordList = getRealmDataManager().getProductStockRecordsByProductSKU(productSKU);

        Map<String, Integer> monthlyStock = new LinkedHashMap<>();

        int cumulativeStocks = 0;
        for (ProductStockRecord productStockRecord: productStockRecordList) {
            Date dateCursor = productStockRecord.getDate();
            Calendar calendar = Calendar.getInstance();
            calendar.setTime(dateCursor);

            String month = calendar.getDisplayName(Calendar.MONTH, Calendar.SHORT, Locale.getDefault());
            int year = calendar.get(Calendar.YEAR) % 2000;
            String monthYearCursor = month + " " + year;

            int currentStock = productStockRecord.getStocks();
            if(productStockRecord.getTransactionType().equals(ProductStockRecord.TransactionType.TRANSACTION_OUT)) {
                currentStock *= -1;
            }

            cumulativeStocks += currentStock;
            monthlyStock.put(monthYearCursor, cumulativeStocks);
        }

        return monthlyStock;
    }
}
