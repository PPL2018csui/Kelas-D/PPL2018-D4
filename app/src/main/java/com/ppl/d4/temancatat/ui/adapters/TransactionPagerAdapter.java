package com.ppl.d4.temancatat.ui.adapters;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.util.SparseArray;
import android.view.ViewGroup;

import com.ppl.d4.temancatat.ui.fragments.AllTransactionFragment;
import com.ppl.d4.temancatat.ui.fragments.OverallStatisticsFragment;
import com.ppl.d4.temancatat.ui.fragments.ProductListFragment;
import com.ppl.d4.temancatat.ui.fragments.TransactionInFragment;
import com.ppl.d4.temancatat.ui.fragments.TransactionOutFragment;

public class TransactionPagerAdapter extends FragmentStatePagerAdapter {

    private int mTabCount;
    private SparseArray<Fragment> mPageReferenceMap;

    public TransactionPagerAdapter(FragmentManager fragmentManager) {
        super(fragmentManager);
        this.mTabCount = 0;
        this.mPageReferenceMap = new SparseArray<>();
    }

    @Override
    public Fragment getItem(int position) {
        Fragment fragment;
        switch (position) {
            case 0:
                fragment = AllTransactionFragment.newInstance();
                break;
            case 1:
                fragment = TransactionInFragment.newInstance();
                break;
            case 2:
                fragment = TransactionOutFragment.newInstance();
                break;
            case 3:
                fragment = ProductListFragment.newInstance();
                break;
            case 4:
                fragment = OverallStatisticsFragment.newInstance();
                break;
            default:
                return null;
        }

        mPageReferenceMap.put(position, fragment);
        return fragment;
    }

    @Override
    public void destroyItem (ViewGroup container, int position, Object object) {
        super.destroyItem(container, position, object);
        mPageReferenceMap.remove(position);
    }

    public Fragment getFragment(int key) {
        return mPageReferenceMap.get(key);
    }

    @Override
    public int getCount() {
        return mTabCount;
    }

    public void setCount(int count) {
        mTabCount = count;
    }
}
