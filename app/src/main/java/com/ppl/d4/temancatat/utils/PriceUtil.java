package com.ppl.d4.temancatat.utils;

import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;

public class PriceUtil {

    private static String INVALID_PRICE_ERROR = "Error";
    private static String US_FORMAT = "US";
    private static String ID_FORMAT = "ID";

    private static DecimalFormat decFormat;

    public static String convertPriceFormatUS(Double price) {
        if (price < 0) {
            return INVALID_PRICE_ERROR;
        }

        decFormat = new DecimalFormat("#,##0.00");
        return decFormat.format(price);
    }

    public static String convertPriceFormatID(Double price) {
        if (price < 0) {
            return INVALID_PRICE_ERROR;
        }

        DecimalFormatSymbols decFormatSym = new DecimalFormatSymbols();

        decFormatSym.setDecimalSeparator(',');
        decFormatSym.setGroupingSeparator('.');

        decFormat = new DecimalFormat("#,##0.00", decFormatSym);
        return decFormat.format(price);
    }

    public static String convertPriceFormatWithCurrency(
            Double price,
            String currencySymbol,
            String formatPrice
    ) {
        String formattedPrice;

        if (formatPrice.equals(ID_FORMAT))
            formattedPrice = convertPriceFormatID(Math.abs(price));
        else if (formatPrice.equals(US_FORMAT))
            formattedPrice = convertPriceFormatUS(Math.abs(price));
        else
            return INVALID_PRICE_ERROR;

        if (price < 0)
            return "-" + currencySymbol + formattedPrice;
        else
            return currencySymbol + formattedPrice;
    }

}