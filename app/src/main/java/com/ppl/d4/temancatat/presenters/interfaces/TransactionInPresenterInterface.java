package com.ppl.d4.temancatat.presenters.interfaces;

import com.ppl.d4.temancatat.models.TransactionListItem;
import com.ppl.d4.temancatat.ui.views.TransactionInView;

import java.util.List;

public interface TransactionInPresenterInterface<V extends TransactionInView> extends BasePresenterInterface<V> {

    List<TransactionListItem> getTransactionInItems();

}
