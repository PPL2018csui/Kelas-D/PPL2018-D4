package com.ppl.d4.temancatat.models;

import android.support.annotation.NonNull;

public class LegendItem implements Comparable{
    private int color;
    private String productSKU;
    private double productPrice;
    private int productAmount;

    public LegendItem(int color, String productSKU, double productPrice, int productAmount) {
        this.color = color;
        this.productSKU = productSKU;
        this.productPrice = productPrice;
        this.productAmount = productAmount;
    }

    public int getColor() {
        return this.color;
    }

    public String getProductSKU() {
        return this.productSKU;
    }

    public double getProductPrice() {
        return this.productPrice;
    }

    public int getProductAmount() {
        return this.productAmount;
    }

    @Override
    public int compareTo(@NonNull Object o) {
        return (int)(((LegendItem)o).getProductPrice()-this.productPrice);
    }
}
