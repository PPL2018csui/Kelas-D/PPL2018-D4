package com.ppl.d4.temancatat.ui.activities;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.view.WindowManager;

import com.ppl.d4.temancatat.Application;
import com.ppl.d4.temancatat.di.components.ActivityComponent;
import com.ppl.d4.temancatat.di.components.DaggerActivityComponent;
import com.ppl.d4.temancatat.di.modules.ActivityModule;
import com.ppl.d4.temancatat.ui.fragments.BaseFragment;
import com.ppl.d4.temancatat.ui.views.BaseView;
import com.ppl.d4.temancatat.utils.ProgressBarHandler;

import butterknife.Unbinder;

/**
 * Class that will render the UI.
 * Activity will implement corresponding view interface.
 * Other activities have to extends BaseActivity.
 */
public abstract class BaseActivity extends AppCompatActivity implements BaseView, BaseFragment.Callback {

    private ActivityComponent mActivityComponent;

    private Unbinder mUnBinder;

    private ProgressBarHandler mProgressBarHandler;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mActivityComponent = DaggerActivityComponent.builder()
                .activityModule(new ActivityModule(this))
                .applicationComponent(((Application) getApplication()).getApplicationComponent())
                .build();

        mProgressBarHandler = new ProgressBarHandler(this);
    }

    public ActivityComponent getActivityComponent() {
        return this.mActivityComponent;
    }

    public void setUnBinder(Unbinder unBinder) {
        this.mUnBinder = unBinder;
    }

    @Override
    protected void onDestroy() {

        if (mUnBinder != null) {
            mUnBinder.unbind();
        }

        closeRealm();

        super.onDestroy();
    }

    private void showSnackbar(String message) {
        Snackbar snackbar = Snackbar.make(
                findViewById(android.R.id.content),
                message,
                Snackbar.LENGTH_LONG);

        snackbar.show();
    }

    @Override
    public void showErrorMessage(String message) {
        showSnackbar(message);
    }

    @Override
    public void showErrorMessage(int messageId) {
        showSnackbar(getResources().getString(messageId));
    }

    @Override
    public void onLoading() {
        mProgressBarHandler.show();
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
    }

    @Override
    public void onFinishLoading() {
        mProgressBarHandler.hide();
        getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
    }

    @Override
    public void onFragmentAttached() {

    }

    @Override
    public void onFragmentDetached(String tag) {

    }

    protected abstract void closeRealm();
}
