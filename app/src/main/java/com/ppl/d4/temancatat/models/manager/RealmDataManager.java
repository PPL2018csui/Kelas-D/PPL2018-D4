package com.ppl.d4.temancatat.models.manager;

import com.ppl.d4.temancatat.models.ProductStockRecord;
import com.ppl.d4.temancatat.models.Product;
import com.ppl.d4.temancatat.models.SeederTransaction;
import com.ppl.d4.temancatat.models.StockUnit;
import com.ppl.d4.temancatat.utils.AppException;
import com.ppl.d4.temancatat.utils.Error;

import java.util.List;

import io.realm.Case;
import io.realm.Realm;
import io.realm.RealmResults;
import io.realm.Sort;

/**
 * Class to communicate with realm database manager
 */
public class RealmDataManager {

    private final Realm mRealm;

    public RealmDataManager(final Realm realm) {
        mRealm = realm;
    }

    public void closeRealm() {
        mRealm.close();
    }

    public ProductStockRecord getProductStockRecordById(String transactionId) {
        ProductStockRecord transaction = mRealm.where(ProductStockRecord.class).equalTo("id", transactionId).findFirst();
        return transaction == null ? null : mRealm.copyFromRealm(transaction);
    }

    public void syncCreateOrUpdateProductStockRecord(final ProductStockRecord productStockRecord) {
        mRealm.executeTransaction(realm1 -> realm1.copyToRealmOrUpdate(productStockRecord));
    }

    public void createOrUpdateProductStockRecord(final ProductStockRecord productStockRecord,
                                                 final OnTransactionCallback onTransactionCallback) {
        mRealm.executeTransactionAsync(realm -> {
            realm.copyToRealmOrUpdate(productStockRecord);
        }, () -> {
            if (onTransactionCallback != null) {
                onTransactionCallback.onRealmSuccess();
            }
        }, error -> {
            if (onTransactionCallback != null) {
                onTransactionCallback.onRealmError(error);
            }
        });
    }

    public List<ProductStockRecord> getAllProductStockRecords() {
        RealmResults<ProductStockRecord> productStockRecordRealmResults
                = mRealm.where(ProductStockRecord.class)
                .sort("date", Sort.DESCENDING)
                .findAll();
        List<ProductStockRecord> productStockRecordList = mRealm.copyFromRealm(productStockRecordRealmResults);
        return productStockRecordList;

    }

    public List<ProductStockRecord> getProductStockRecordsByProductName(String productName) {
        RealmResults<ProductStockRecord> productStockRecordRealmResults
                = mRealm.where(ProductStockRecord.class)
                .equalTo("product.name", productName)
                .sort("date", Sort.ASCENDING)
                .findAll();
        List<ProductStockRecord> productStockRecordList = mRealm.copyFromRealm(productStockRecordRealmResults);
        return productStockRecordList;
    }

    public List<ProductStockRecord> getTransactionInProductStockRecords() {
        RealmResults<ProductStockRecord> productStockRecordRealmResults
                = mRealm.where(ProductStockRecord.class)
                .equalTo("transactionType", ProductStockRecord.TransactionType.TRANSACTION_IN.toString())
                .sort("date", Sort.DESCENDING)
                .findAll();
        List<ProductStockRecord> productStockRecordList = mRealm.copyFromRealm(productStockRecordRealmResults);
        return productStockRecordList;

    }

    public List<ProductStockRecord> getTransactionOutProductStockRecords() {
        RealmResults<ProductStockRecord> productStockRecordRealmResults
                = mRealm.where(ProductStockRecord.class)
                .equalTo("transactionType", ProductStockRecord.TransactionType.TRANSACTION_OUT.toString())
                .sort("date", Sort.DESCENDING)
                .findAll();
        List<ProductStockRecord> productStockRecordList = mRealm.copyFromRealm(productStockRecordRealmResults);
        return productStockRecordList;
    }

    public void deleteProductStockRecordById(final String id, final OnTransactionCallback onTransactionCallback) {
        mRealm.executeTransactionAsync(realm -> {
            ProductStockRecord productStockRecord = realm.where(ProductStockRecord.class).equalTo("id", id).findFirst();
            if (productStockRecord != null && productStockRecord.isValid()) {
                productStockRecord.deleteFromRealm();
            }
        }, () -> {
            if (onTransactionCallback != null) {
                onTransactionCallback.onRealmSuccess();
            }
        }, error -> {
            if (onTransactionCallback != null) {
                onTransactionCallback.onRealmError(error);
            }
        });
    }

    public void syncDeleteProductStockRecordById(final String id) throws AppException {
        ProductStockRecord transaction = mRealm.where(ProductStockRecord.class).equalTo("id", id).findFirst();
        if (transaction == null)
            throw new AppException(Error.FAILED_DATA_DELETION);

        mRealm.executeTransaction(realm1 ->
                transaction.deleteFromRealm()
        );
    }

    public List<ProductStockRecord> getProductStockRecordsByProductSKU(String productSKU) {
        RealmResults<ProductStockRecord> productStockRecordRealmResults
                = mRealm.where(ProductStockRecord.class)
                .equalTo("product.sKU", productSKU)
                .sort("date", Sort.ASCENDING)
                .findAll();
        List<ProductStockRecord> productStockRecordList = mRealm.copyFromRealm(productStockRecordRealmResults);
        return productStockRecordList;
    }

    public int getCurrentProductStockByProductSKU(String productSKU) {
        int stockIn = mRealm.where(ProductStockRecord.class)
                .equalTo("product.sKU", productSKU)
                .and()
                .equalTo("transactionType", ProductStockRecord.TransactionType.TRANSACTION_IN.toString())
                .findAll()
                .sum("stocks")
                .intValue();
        int stockOut = mRealm.where(ProductStockRecord.class)
                .equalTo("product.sKU", productSKU)
                .and()
                .equalTo("transactionType", ProductStockRecord.TransactionType.TRANSACTION_OUT.toString())
                .findAll()
                .sum("stocks")
                .intValue();
        return stockIn - stockOut;
    }

    public List<ProductStockRecord> getProductStockRecordsInByProductSKU(String productSKU) {
        RealmResults<ProductStockRecord> productStockRecordRealmResults = mRealm.where(ProductStockRecord.class)
                .equalTo("product.sKU", productSKU)
                .and()
                .equalTo("transactionType", ProductStockRecord.TransactionType.TRANSACTION_IN.toString())
                .findAll();
        List<ProductStockRecord> productStockRecords = mRealm.copyFromRealm(productStockRecordRealmResults);
        return productStockRecords;
    }

    public void createStockUnit(final StockUnit stockUnit) {
        mRealm.executeTransaction(realm1 -> realm1.copyToRealm(stockUnit));
    }

    public List<StockUnit> getAllStockUnits() {
        RealmResults<StockUnit> stockUnitRealmResults = mRealm.where(StockUnit.class).findAll();
        List<StockUnit> stockUnitList = mRealm.copyFromRealm(stockUnitRealmResults);
        return stockUnitList;
    }

    public StockUnit getStockUnitByName(String name) {
        StockUnit stockUnit = mRealm.where(StockUnit.class).equalTo("name", name).findFirst();
        return stockUnit == null ? null : mRealm.copyFromRealm(stockUnit);
    }

    public void syncCreateOrUpdateProduct(final Product product) {
        mRealm.executeTransaction(realm1 -> realm1.copyToRealmOrUpdate(product));
    }

    public void createOrUpdateProduct(final Product product,
                                      final OnTransactionCallback onTransactionCallback) {
        mRealm.executeTransactionAsync(realm -> {
            realm.copyToRealmOrUpdate(product);
        }, () -> {
            if (onTransactionCallback != null) {
                onTransactionCallback.onRealmSuccess();
            }
        }, error -> {
            if (onTransactionCallback != null) {
                onTransactionCallback.onRealmError(error);
            }
        });
    }

    public Product getProductBySKU(String productSKU) {
        Product product = mRealm.where(Product.class).equalTo("sKU", productSKU).findFirst();
        return product == null ? null : mRealm.copyFromRealm(product);
    }

    public List<Product> getAllProducts() {
        RealmResults<Product> productRealmResults = mRealm.where(Product.class).findAll();
        List<Product> productList = mRealm.copyFromRealm(productRealmResults);
        return productList;
    }

    public List<Product> getProductsFilterBySKU(String sKUFilter) {
        RealmResults<Product> productRealmResults = mRealm
                .where(Product.class)
                .contains("sKU", sKUFilter, Case.INSENSITIVE)
                .findAll();

        List<Product> productList = mRealm.copyFromRealm(productRealmResults);
        return productList;
    }

    public void createSeederTransaction(final SeederTransaction seederTransaction) {
        mRealm.executeTransaction(realm1 -> realm1.copyToRealm(seederTransaction));
    }

    public SeederTransaction getSeederTransactionById(String id) {
        SeederTransaction transaction = mRealm.where(SeederTransaction.class).equalTo("id", id).findFirst();
        return transaction == null ? null : mRealm.copyFromRealm(transaction);
    }

    public interface OnTransactionCallback {
        void onRealmSuccess();

        void onRealmError(Throwable e);
    }

    public List<ProductStockRecord> getProductStockRecordsOutByProductSKU(String productSKU) {
        RealmResults<ProductStockRecord> productStockRecordRealmResults = mRealm.where(ProductStockRecord.class)
                .equalTo("product.sKU", productSKU)
                .and()
                .equalTo("transactionType", ProductStockRecord.TransactionType.TRANSACTION_OUT.toString())
                .findAll();
        List<ProductStockRecord> productStockRecords = mRealm.copyFromRealm(productStockRecordRealmResults);
        return productStockRecords;
    }
}
