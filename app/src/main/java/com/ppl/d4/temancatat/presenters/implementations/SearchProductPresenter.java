package com.ppl.d4.temancatat.presenters.implementations;

import com.ppl.d4.temancatat.models.Product;
import com.ppl.d4.temancatat.models.manager.RealmDataManager;
import com.ppl.d4.temancatat.presenters.interfaces.SearchProductPresenterInterface;
import com.ppl.d4.temancatat.ui.views.SearchProductView;

import java.util.List;

import javax.inject.Inject;

public class SearchProductPresenter<V extends SearchProductView> extends BasePresenter<V> implements SearchProductPresenterInterface<V>, RealmDataManager.OnTransactionCallback {

    private static final String TAG = "Search Product Presenter";

    @Inject
    public SearchProductPresenter(final RealmDataManager realmDataManager) {
        super(realmDataManager);
    }

    @Override
    public List<Product> getAllProducts() {
        getView().onLoading();
        List<Product> productList = getRealmDataManager().getAllProducts();
        getView().onFinishLoading();
        return productList;
    }

    @Override
    public List<Product> filterProductsBySKU(String sKU) {
        getView().onLoading();
        List<Product> productList = getRealmDataManager().getProductsFilterBySKU(sKU);
        getView().onFinishLoading();
        return productList;
    }

    @Override
    public void onRealmSuccess() {

    }

    @Override
    public void onRealmError(Throwable e) {
        getView().showErrorMessage(e.toString());
    }
}
