package com.ppl.d4.temancatat.models;

import java.util.UUID;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

public class SeederTransaction extends RealmObject {

    @PrimaryKey
    private String id;

    private String description;

    public SeederTransaction() {
        this.id = UUID.randomUUID().toString();
    }

    public SeederTransaction(String id) {
        this.id = id;
    }

    public SeederTransaction(String id, String description) {
        this(id);
        this.description = description;
    }

    public String getId() {
        return this.id;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getDescription() {
        return this.description;
    }
}
