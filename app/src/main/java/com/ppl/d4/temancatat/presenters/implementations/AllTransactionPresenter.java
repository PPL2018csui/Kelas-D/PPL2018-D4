package com.ppl.d4.temancatat.presenters.implementations;

import com.ppl.d4.temancatat.models.ProductStockRecord;
import com.ppl.d4.temancatat.models.TransactionDate;
import com.ppl.d4.temancatat.models.TransactionListItem;
import com.ppl.d4.temancatat.models.manager.RealmDataManager;
import com.ppl.d4.temancatat.presenters.interfaces.AllTransactionPresenterInterface;
import com.ppl.d4.temancatat.ui.views.AllTransactionView;
import com.ppl.d4.temancatat.utils.DateUtil;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import javax.inject.Inject;

public class AllTransactionPresenter<V extends AllTransactionView> extends BasePresenter<V> implements AllTransactionPresenterInterface<V>, RealmDataManager.OnTransactionCallback{

    @Inject
    public AllTransactionPresenter(RealmDataManager realmDataManager) {
        super(realmDataManager);
    }

    @Override
    public void closeRealm() {
        super.closeRealm();
    }

    @Override
    public void onRealmSuccess() {

    }

    @Override
    public void onRealmError(Throwable e) {

    }

    @Override
    public List<TransactionListItem> getAllTransactionItems() {
        List<ProductStockRecord> productStockRecordList = getRealmDataManager().getAllProductStockRecords();

        List<TransactionListItem> transactionListItems = new ArrayList<>();
        String date = "";
        for (ProductStockRecord productStockRecord: productStockRecordList) {
            String dateCursor = DateUtil.formatToDMY(productStockRecord.getDate()).toUpperCase(Locale.ROOT);
            if (date.equalsIgnoreCase(dateCursor)) {
                transactionListItems.add(productStockRecord);
            } else {
                date = dateCursor;
                TransactionDate transactionDate = new TransactionDate(date);
                transactionListItems.add(transactionDate);
                transactionListItems.add(productStockRecord);
            }
        }

        return transactionListItems;
    }
}
