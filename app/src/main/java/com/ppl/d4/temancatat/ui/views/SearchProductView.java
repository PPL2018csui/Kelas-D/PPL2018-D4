package com.ppl.d4.temancatat.ui.views;

import android.view.Menu;

import com.ppl.d4.temancatat.models.Product;

import java.util.List;

public interface SearchProductView extends BaseView {
    void openAddProductActivity();

    void onPrepareProductList();

    void showProductList(List<Product> productList);

    void showNoProductPlaceholder();

    void onPrepareSearchView(Menu menu);
}
