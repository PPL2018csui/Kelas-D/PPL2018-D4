package com.ppl.d4.temancatat.ui.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.CardView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.github.mikephil.charting.charts.PieChart;
import com.github.mikephil.charting.components.Legend;
import com.ppl.d4.temancatat.R;
import com.ppl.d4.temancatat.di.components.ActivityComponent;
import com.ppl.d4.temancatat.models.LegendItem;
import com.ppl.d4.temancatat.presenters.interfaces.OverallStatisticsPresenterInterface;
import com.ppl.d4.temancatat.ui.adapters.LegendAdapter;
import com.ppl.d4.temancatat.ui.views.OverallStatisticsView;
import com.ppl.d4.temancatat.utils.PriceUtil;

import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;

public class OverallStatisticsFragment extends BaseFragment implements OverallStatisticsView{

    private static final String TAG = "OverallStatsFragment";

    @Inject
    OverallStatisticsPresenterInterface<OverallStatisticsView> mPresenter;

    @BindView(R.id.sale_amount_textview)
    TextView saleAmountTextView;

    @BindView(R.id.purchase_amount_textview)
    TextView purchaseAmountTextView;

    @BindView(R.id.net_revenue_amount_textview)
    TextView netRevenueAmountTextView;

    @BindView(R.id.sales_piechart)
    PieChart salesPieChart;

    @BindView(R.id.purchases_piechart)
    PieChart purchasesPieChart;

    @BindView(R.id.sales_section_cardview)
    CardView salesCardView;

    @BindView(R.id.purchases_section_cardview)
    CardView purchasesCardView;

    @BindView(R.id.sales_recyclerView)
    RecyclerView salesRecyclerView;

    @BindView(R.id.purchases_recyclerView)
    RecyclerView purchasesRecyclerView;

    @BindView(R.id.no_data_layout)
    LinearLayout noDataLayout;

    @BindView(R.id.statistic_layout)
    LinearLayout statisticLayout;

    public static OverallStatisticsFragment newInstance() {
        Bundle args = new Bundle();
        OverallStatisticsFragment fragment = new OverallStatisticsFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.overall_statistics_fragment, container, false);

        ActivityComponent component = getActivityComponent();
        if (component != null) {
            component.inject(this);
            setUnBinder(ButterKnife.bind(this,view));
            mPresenter.onAttach(this);
        }
        return view;
    }

    @Override
    protected void setUp(View view) {
        if (mPresenter.checkIsExistTransaction()) {
            showOverview();
            setUpPieChartData();
            if (mPresenter.checkIsExistTransactionOut() && mPresenter.checkIsExistTransactionIn()) {
                showSalesSection();
                showPurchaseSection();
            } else if (mPresenter.checkIsExistTransactionOut()) {
                showSalesSection();
                hidePurchaseSection();
            } else {
                hideSalesSection();
                showPurchaseSection();
            }
        } else {
            hideOverview();
            hideAllSection();
        }
    }

    @Override
    public void showPurchaseOverview() {
        purchaseAmountTextView.setText("Rp"+PriceUtil.convertPriceFormatID(mPresenter.getTotalPurchasesAmount()));
        purchaseAmountTextView.setTextColor(getResources().getColor(R.color.colorNominalRed));
    }

    @Override
    public void showSaleOverview() {
        saleAmountTextView.setText("Rp"+PriceUtil.convertPriceFormatID(mPresenter.getTotalSalesAmount()));
        saleAmountTextView.setTextColor(getResources().getColor(R.color.colorNominalGreen));
    }

    @Override
    public void showNetRevenue() {
        if(mPresenter.getNetRevenueAmount() < 0) {
            netRevenueAmountTextView.setText("- Rp"+PriceUtil.convertPriceFormatID(Math.abs(mPresenter.getNetRevenueAmount())));
            netRevenueAmountTextView.setTextColor(getResources().getColor(R.color.colorPriceRecommendation));
        } else {
            netRevenueAmountTextView.setText("Rp"+PriceUtil.convertPriceFormatID(mPresenter.getNetRevenueAmount()));
            netRevenueAmountTextView.setTextColor(getResources().getColor(R.color.colorNominalGreen));
        }
    }

    @Override
    public void setUpPieChartData() {
        mPresenter.onPreparePieChartData();
    }

    @Override
    public void showSalesSection() {
        salesCardView.setVisibility(View.VISIBLE);
        mPresenter.showPieChart(salesPieChart, 0);
        setCustomLegend(salesRecyclerView, 0);
    }

    @Override
    public void hideSalesSection() {
        salesCardView.setVisibility(View.GONE);
    }

    @Override
    public void showPurchaseSection() {
        purchasesCardView.setVisibility(View.VISIBLE);
        mPresenter.showPieChart(purchasesPieChart, 1);
        setCustomLegend(purchasesRecyclerView, 1);
    }

    @Override
    public void hidePurchaseSection() {
        purchasesCardView.setVisibility(View.GONE);
    }

    @Override
    public void hideAllSection() {
        noDataLayout.setVisibility(View.VISIBLE);
        statisticLayout.setVisibility(View.GONE);
    }

    @Override
    public void showOverview() {
        noDataLayout.setVisibility(View.GONE);
        statisticLayout.setVisibility(View.VISIBLE);
        showPurchaseOverview();
        showSaleOverview();
        showNetRevenue();
    }

    @Override
    public void hideOverview() {
        purchaseAmountTextView.setText("-");
        saleAmountTextView.setText("-");
        netRevenueAmountTextView.setText("-");
    }

    public void setCustomLegend(RecyclerView recyclerView, int mode) {
        List<LegendItem> legendItems = mPresenter.getLegendItems(mode);
        LinearLayoutManager layoutManager
                = new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false);
        recyclerView.setLayoutManager(layoutManager);
        LegendAdapter legendAdapter = new LegendAdapter(getContext(), legendItems);
        recyclerView.setAdapter(legendAdapter);
        recyclerView.setNestedScrollingEnabled(false);
    }

    @Override
    public void onStart() {
        super.onStart();
        setUp(this.getView());
    }
}
