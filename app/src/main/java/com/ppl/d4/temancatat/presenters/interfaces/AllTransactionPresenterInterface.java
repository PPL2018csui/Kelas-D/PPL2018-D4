package com.ppl.d4.temancatat.presenters.interfaces;

import com.ppl.d4.temancatat.models.TransactionListItem;
import com.ppl.d4.temancatat.ui.views.AllTransactionView;

import java.util.List;

public interface AllTransactionPresenterInterface<V extends AllTransactionView> extends BasePresenterInterface<V> {

    List<TransactionListItem> getAllTransactionItems();
}
