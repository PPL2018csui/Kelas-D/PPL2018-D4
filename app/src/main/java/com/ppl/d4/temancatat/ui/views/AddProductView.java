package com.ppl.d4.temancatat.ui.views;


public interface AddProductView extends BaseView {

    void onSaveProduct();

    void prepareStockUnit();

    void openSearchProductActivity();

    void onErrorProductNameField(int messageId);

    void onErrorProductSKUField(int messageId);

    void onErrorProductDescriptionField(int messageId);

    void useDefaultSKU();

    void updateProductPreview(CharSequence text);

    void showToastMessage(int stringResourceId, int lengthModifier);

    void clearErrorOnAllFields();
}
