package com.ppl.d4.temancatat.presenters.interfaces;

import com.ppl.d4.temancatat.models.Product;
import com.ppl.d4.temancatat.models.ProductStockRecord;
import com.ppl.d4.temancatat.ui.views.AddTransactionView;
import com.ppl.d4.temancatat.utils.AppException;

import java.util.Date;
import java.util.List;

public interface AddTransactionPresenterInterface<V extends AddTransactionView> extends BasePresenterInterface<V> {

    void onSaveTransaction(String productSKU, Date date, ProductStockRecord.TransactionType transactionType, Integer stocks, Double price) throws AppException;
    void setTransactionById(String s) throws AppException;
    void changeMode(String s);
    void toggleEdit();
    void deleteTransaction();
    String getMode();
    ProductStockRecord getTransaction();
    boolean isValidEditOrDeleteStock(String productSKU, Integer inputStocks, ProductStockRecord.TransactionType inputTransactionType);
    double getRecommendationPrice(String productSKU);
}
