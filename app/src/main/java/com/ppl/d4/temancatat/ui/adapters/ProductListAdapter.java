package com.ppl.d4.temancatat.ui.adapters;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.ppl.d4.temancatat.R;
import com.ppl.d4.temancatat.models.Product;
import com.ppl.d4.temancatat.ui.activities.AddTransactionActivity;
import com.ppl.d4.temancatat.ui.activities.EditProductActivity;
import com.ppl.d4.temancatat.ui.activities.SearchProductActivity;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

import static android.app.Activity.RESULT_OK;

public class ProductListAdapter extends RecyclerView.Adapter<ProductListAdapter.ProductListViewHolder> {

    private Context mContext;
    private List<Product> mProductList;
    private int mode;

    public static final int SEARCH_PRODUCT_MODE = 0;
    public static final int LIST_PRODUCT_MODE = 1;
    public static final int EDIT_PRODUCT_CODE = 2;

    public ProductListAdapter(Context mContext, List<Product> mProductList, int mode){
        this.mContext = mContext;
        this.mProductList = mProductList;
        this.mode = mode;
    }

    @NonNull
    @Override
    public ProductListViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(mContext);
        View view = inflater.inflate(R.layout.product_item, parent, false);
        return new ProductListViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ProductListViewHolder holder, int position) {
        Product product = mProductList.get(position);

        holder.setProductNameItem(product.getName());
        holder.setProductSKUItemTextView(product.getSKU());
        holder.setProductNameMetricItemTextView(product.getStockUnit().getName());

        switch (this.mode) {
            case ProductListAdapter.SEARCH_PRODUCT_MODE:
                holder.productItemLinearLayout.setOnClickListener(
                        v -> holder.onOpenAddTransaction(
                                product.getSKU(),
                                product.getStockUnit().getName()
                        )
                );
                break;
            case ProductListAdapter.LIST_PRODUCT_MODE:
                holder.productItemLinearLayout.setOnClickListener(
                        v -> holder.onOpenShowDetails(
                                product.getSKU(),
                                product.getStockUnit().getName()
                        )
                );
                break;
        }
    }

    @Override
    public int getItemCount() {
        return mProductList.size();
    }

    public void setProductList(List<Product> productList) {
        this.mProductList = productList;
        notifyDataSetChanged();
    }

    class ProductListViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.product_SKU_item_textview)
        TextView productSKUItemTextView;

        @BindView(R.id.product_name_item_textview)
        TextView productNameItemTextView;

        @BindView(R.id.product_unit_metric_item_textview)
        TextView productNameMetricItemTextView;

        @BindView(R.id.product_item_linearlayout)
        LinearLayout productItemLinearLayout;

        public void onOpenAddTransaction(String productSKU, String productStockUnit) {
            Intent intent = new Intent(mContext, AddTransactionActivity.class);
            intent.putExtra("productSKU", productSKU);
            intent.putExtra("productStockUnit", productStockUnit);

            (((SearchProductActivity)mContext)).setResult(RESULT_OK, intent);
            (((SearchProductActivity)mContext)).finish();
        }

        public void onOpenShowDetails(String productSKU, String stockUnitName) {
             Intent intent = new Intent(mContext, EditProductActivity.class);
             intent.putExtra("productSKU", productSKU);
             intent.putExtra("productStockUnitName", stockUnitName);

            ((Activity) mContext).startActivityForResult(intent, EDIT_PRODUCT_CODE);
        }

        public ProductListViewHolder(View itemView) {
            super(itemView);

            ButterKnife.bind(this, itemView);
        }

        public void setProductNameItem(String name) {
            String formattedName = String.format("Nama: %s", name);
            productNameItemTextView.setText(formattedName);
        }

        public void setProductSKUItemTextView(String name) {
            productSKUItemTextView.setText(name);
        }

        public void setProductNameMetricItemTextView(String name) {
            String formattedName = String.format("Satuan: %s", name);
            productNameMetricItemTextView.setText(formattedName);
        }
    }
}
