package com.ppl.d4.temancatat.seeders;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class MandatorySeeder extends Seeder {
    private static final String ID = "MandatorySeeder";
    private static final String DESCRIPTION = "Seed mandatory data";

    public static final List<Seeder> DEPENDENCIES =
            Collections.unmodifiableList(Arrays.asList(
                    new StockUnitSeeder()
            ));

    public MandatorySeeder() {
        super(ID, DESCRIPTION, DEPENDENCIES);
    }
}
