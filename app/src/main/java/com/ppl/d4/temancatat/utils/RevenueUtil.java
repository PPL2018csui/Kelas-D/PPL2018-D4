package com.ppl.d4.temancatat.utils;

import com.ppl.d4.temancatat.models.ProductStockRecord;

import java.util.List;

public class RevenueUtil {

    public static Double countPurchases(List<ProductStockRecord> productStockRecordList) {
        double purchase = 0;

        for(ProductStockRecord productStockRecord: productStockRecordList) {
            if(productStockRecord.getTransactionType() == ProductStockRecord.TransactionType.TRANSACTION_IN) {
                purchase += productStockRecord.getPrice() * productStockRecord.getStocks();
            }
        }

        return purchase;
    }

    public static Double countSales(List<ProductStockRecord> productStockRecordList) {
        double sales = 0;

        for(ProductStockRecord productStockRecord: productStockRecordList) {
            if(productStockRecord.getTransactionType() == ProductStockRecord.TransactionType.TRANSACTION_OUT) {
                sales += productStockRecord.getPrice() * productStockRecord.getStocks();
            }
        }

        return sales;
    }

    public static Double countTotalRevenue(List<ProductStockRecord> productStockRecordList) {
        double revenue = 0;

        for(ProductStockRecord productStockRecord: productStockRecordList) {
            if(productStockRecord.getTransactionType() == ProductStockRecord.TransactionType.TRANSACTION_OUT) {
                revenue += productStockRecord.getPrice() * productStockRecord.getStocks();
            } else {
                revenue -= productStockRecord.getPrice() * productStockRecord.getStocks();
            }
        }

        return revenue;
    }

    public static Double countTotalRevenue(double purchases, double sales) {
        return sales - purchases;
    }

    public static Integer countTotalStockPurchase(List<ProductStockRecord> productStockRecordList) {
        int purchase = 0;

        for(ProductStockRecord productStockRecord: productStockRecordList) {
            if(productStockRecord.getTransactionType() == ProductStockRecord.TransactionType.TRANSACTION_IN) {
                purchase += productStockRecord.getStocks();
            }
        }

        return purchase;
    }

    public static Integer countTotalStockSales(List<ProductStockRecord> productStockRecordList) {
        int sales = 0;

        for(ProductStockRecord productStockRecord: productStockRecordList) {
            if(productStockRecord.getTransactionType() == ProductStockRecord.TransactionType.TRANSACTION_OUT) {
                sales += productStockRecord.getStocks();
            }
        }

        return sales;
    }

}
