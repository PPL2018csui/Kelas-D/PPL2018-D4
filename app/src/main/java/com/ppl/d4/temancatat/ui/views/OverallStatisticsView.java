package com.ppl.d4.temancatat.ui.views;

public interface OverallStatisticsView extends BaseView {

    void showPurchaseOverview();

    void showSaleOverview();

    void showNetRevenue();

    void setUpPieChartData();

    void showSalesSection();

    void hideSalesSection();

    void showPurchaseSection();

    void hidePurchaseSection();

    void hideAllSection();

    void showOverview();

    void hideOverview();
}
