package com.ppl.d4.temancatat.seeders;

import com.ppl.d4.temancatat.models.Product;
import com.ppl.d4.temancatat.models.ProductStockRecord;
import com.ppl.d4.temancatat.models.manager.RealmDataManager;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Random;

import io.realm.Realm;

public class DummyProductStockRecordSeeder extends Seeder {

    private static final double PRICE_LOWER_BOUND = 1000;
    private static final double PRICE_UPPER_BOUND = 10000000;

    private static final int STOCKS_LOWER_BOUND = 1;
    private static final int STOCKS_UPPER_BOUND = 50;

    private Random rand = new Random();

    private static final String ID = "DummyProductStockRecordSeeder";
    private static final String DESCRIPTION = "Seed dummy product stock record for each product";
    private static final List<Seeder> DEPENDENCIES =
            Collections.unmodifiableList(Arrays.asList(
                    new DummyProductSeeder()
            ));

    private static final String DATE_LOWER_BOUND = "2018-04-01 00:00:00";
    private static final String DATE_UPPER_BOUND = "2018-04-20 23:59:59";

    public static final int SEED_COUNT = 5;

    public DummyProductStockRecordSeeder() {
        super(ID, DESCRIPTION, DEPENDENCIES);
    }

    @Override
    public void handleSeed(RealmDataManager realmDataManager) {
        List<Product> productList = realmDataManager.getAllProducts();
        List<ProductStockRecord.TransactionType> transactionTypes = new ArrayList<>();
        transactionTypes.add(ProductStockRecord.TransactionType.TRANSACTION_IN);
        transactionTypes.add(ProductStockRecord.TransactionType.TRANSACTION_OUT);

        for (Product product: productList) {
            for (int i = 0; i < SEED_COUNT; ++i) {
                ProductStockRecord productStockRecord = this.generateDummyProductStockRecord (
                        product,
                        transactionTypes,
                        Timestamp.valueOf(DATE_LOWER_BOUND).getTime(),
                        Timestamp.valueOf(DATE_UPPER_BOUND).getTime()
                );

                realmDataManager.syncCreateOrUpdateProductStockRecord(productStockRecord);
            }
        }
    }

    public double generatePrice() {
        return PRICE_LOWER_BOUND +
                Math.abs(PRICE_UPPER_BOUND - PRICE_LOWER_BOUND) *
                        rand.nextDouble();
    }

    public int generateStocks() {
        return STOCKS_LOWER_BOUND +
                rand.nextInt(Math.abs(STOCKS_UPPER_BOUND - STOCKS_LOWER_BOUND));
    }

    public ProductStockRecord.TransactionType generateType(
            List<ProductStockRecord.TransactionType> type) {
        return type.get(rand.nextInt(type.size()));
    }

    public Date generateRandomDate(long dateLowerBound, long dateUpperBound) {
        long diff = dateUpperBound - dateLowerBound + 1;
        long randDate = dateLowerBound + (long) (Math.random() * diff);
        return new Date(randDate);
    }

    public ProductStockRecord generateDummyProductStockRecord (
            Product product,
            List<ProductStockRecord.TransactionType> transactionType,
            long dateLowerBound,
            long dateUpperBound
    ) {
        Date tempDate = generateRandomDate(dateLowerBound, dateUpperBound);
        ProductStockRecord.TransactionType tempType =
                generateType(transactionType);
        int tempStock = generateStocks();
        double tempPrice = generatePrice();
        tempPrice = (int) (tempPrice - (tempPrice % 1000));
        if (tempType == ProductStockRecord.TransactionType.TRANSACTION_IN)
            tempStock += STOCKS_UPPER_BOUND;

        return new ProductStockRecord(
                product,
                tempDate,
                tempType,
                tempStock,
                tempPrice
        );
    }

}
