package com.ppl.d4.temancatat.ui.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.NavUtils;
import android.view.MenuItem;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.ppl.d4.temancatat.R;
import com.ppl.d4.temancatat.presenters.implementations.AddProductPresenter;
import com.ppl.d4.temancatat.ui.views.AddProductView;
import com.ppl.d4.temancatat.utils.AppException;

import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.OnTextChanged;

public class AddProductActivity extends BaseActivity implements AddProductView {

    private static final String TAG = "Add Product Activity";

    @Inject
    AddProductPresenter<AddProductView> mPresenter;

    @BindView(R.id.product_unit_spinner)
    Spinner productUnitSpinner;

    @BindView(R.id.add_product_name_editText)
    TextView addProductNameEditText;

    @BindView(R.id.add_product_desc_editText)
    TextView addProductDescEditText;

    @BindView(R.id.add_product_SKU_editText)
    TextView addProductSKUEditText;

    @BindView(R.id.product_SKU_textinputlayout)
    TextInputLayout productSKUTextInputLayout;

    @BindView(R.id.product_name_textinputlayout)
    TextInputLayout productNameTextInputLayout;

    @BindView(R.id.product_desc_textinputlayout)
    TextInputLayout productDescTextInputLayout;

    @BindView(R.id.product_preview_textview)
    TextView productPreviewTextView;

    @Override
    @OnClick(R.id.get_default_sku_textview)
    public void useDefaultSKU() {
        String productName = addProductNameEditText.getText().toString();
        String stockUnitName = productUnitSpinner.getSelectedItem().toString();

        String defaultSKU = mPresenter.generateDefaultSKU(productName, stockUnitName);

        addProductSKUEditText.setText(defaultSKU);
    }


    @Override
    @OnClick(R.id.add_product_save_button)
    public void onSaveProduct() {
        String productName = addProductNameEditText.getText().toString();
        String productDesc = addProductDescEditText.getText().toString();
        String productSKU = addProductSKUEditText.getText().toString();
        String stockUnitName = productUnitSpinner.getSelectedItem().toString();

        try {
            mPresenter.onSaveProduct(productName, productDesc, stockUnitName, productSKU);
        }
        catch (AppException e) {
            showErrorMessage(e.getErrorMessage());
        }
    }

    @Override
    @OnTextChanged(R.id.add_product_SKU_editText)
    public void updateProductPreview(CharSequence text) {
        productPreviewTextView.setText(text.toString());
    }

    @Override
    public void clearErrorOnAllFields() {
        productNameTextInputLayout.setError(null);
        productSKUTextInputLayout.setError(null);
        productDescTextInputLayout.setError(null);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_product);

        getActivityComponent().inject(this);

        setUnBinder(ButterKnife.bind(this));
        ButterKnife.bind(this);

        mPresenter.onAttach(this);

        prepareStockUnit();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case android.R.id.home:
                NavUtils.navigateUpFromSameTask(this);
                return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void prepareStockUnit() {
        List<String> stockUnitsName = mPresenter.getStockUnitsName();

        ArrayAdapter<String> adapter = new ArrayAdapter<>(this,
                R.layout.spinner_item,
                stockUnitsName);

        adapter.setDropDownViewResource(R.layout.spinner_dropdown_item);
        productUnitSpinner.setAdapter(adapter);
    }

    @Override
    public void openSearchProductActivity() {
        Intent intent = new Intent(this, SearchProductActivity.class);
        setResult(RESULT_OK, intent);
        finish();
    }

    @Override
    public void onErrorProductNameField(int messageId) {
        productNameTextInputLayout.setError(getResources().getString(messageId));
        addProductNameEditText.requestFocus();
    }

    @Override
    public void onErrorProductSKUField(int messageId) {
        productSKUTextInputLayout.setError(getResources().getString(messageId));
        addProductSKUEditText.requestFocus();
    }

    @Override
    public void onErrorProductDescriptionField(int messageId) {
        productDescTextInputLayout.setError(getResources().getString(messageId));
        addProductDescEditText.requestFocus();
    }

    @Override
    public void showToastMessage(int stringResourceId, int lengthModifier) {
        Toast.makeText(getApplicationContext(), stringResourceId, lengthModifier).show();
    }

    @Override
    protected void onDestroy() {
        mPresenter.onDetach();
        super.onDestroy();
    }

    @Override
    protected void closeRealm() {
        mPresenter.closeRealm();
    }
}
