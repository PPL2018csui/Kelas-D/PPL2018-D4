package com.ppl.d4.temancatat.presenters.implementations;

import android.widget.Toast;

import com.ppl.d4.temancatat.R;
import com.ppl.d4.temancatat.models.Product;
import com.ppl.d4.temancatat.models.ProductStockRecord;
import com.ppl.d4.temancatat.models.manager.RealmDataManager;
import com.ppl.d4.temancatat.presenters.interfaces.AddTransactionPresenterInterface;
import com.ppl.d4.temancatat.ui.views.AddTransactionView;
import com.ppl.d4.temancatat.utils.AppException;
import com.ppl.d4.temancatat.utils.Error;

import java.util.Date;
import java.util.List;

import javax.inject.Inject;

public class AddTransactionPresenter<V extends AddTransactionView> extends BasePresenter<V> implements AddTransactionPresenterInterface<V>, RealmDataManager.OnTransactionCallback {

    private static final String PRODUCT_PLACEHOLDER = "Pilih Produk";
    public static final String VIEW_MODE = "VIEW";
    public static final String CREATE_MODE = "CREATE";
    public static final String EDIT_MODE = "EDIT";

    private String mode;
    private ProductStockRecord transaction;

    @Inject
    public AddTransactionPresenter(final RealmDataManager realmDataManager) {
        super(realmDataManager);
    }

    @Override
    public void closeRealm() {
        super.closeRealm();
    }

    @Override
    public void onRealmSuccess() {
        getView().onFinishLoading();
        getView().showToastMessage(R.string.changes_toast_message, Toast.LENGTH_SHORT);
        getView().finish();
    }

    @Override
    public void onRealmError(Throwable e) {
        getView().showErrorMessage(e.toString());
    }

    @Override
    public void setTransactionById(String transactionId) throws AppException {
        if (transactionId == null || (transaction != null && transactionId.equals(transaction.getId())))
            return;

        transaction = getRealmDataManager().getProductStockRecordById(transactionId);
        if (transaction == null)
            throw new AppException(Error.FAILED_DATA_RETRIEVAL);
    }

    @Override
    public void changeMode(String mode) {
        if (mode == null)
            mode = CREATE_MODE;

        if (mode.equals(this.mode))
            return;

        this.mode = mode;
        if (mode.equals(VIEW_MODE))
            getView().onViewModeChanged();
        else if (mode.equals(EDIT_MODE))
            getView().onEditModeChanged();
        else if (mode.equals(CREATE_MODE))
            getView().onAddModeChanged();
    }

    @Override
    public ProductStockRecord getTransaction() {
        return transaction;
    }

    @Override
    public void deleteTransaction() throws AppException {
        if(transaction.getTransactionType() == ProductStockRecord.TransactionType.TRANSACTION_IN) {
            if (!isValidEditOrDeleteStock(transaction.getProduct().getSKU(), 0, transaction.getTransactionType())) {
                throw new AppException(Error.FAILED_DATA_DELETION);
            }
        }

        getView().onLoading();
        getRealmDataManager().syncDeleteProductStockRecordById(transaction.getId());
        getView().onFinishLoading();
    }

    @Override
    public void toggleEdit() {
        if (mode.equals(EDIT_MODE))
            changeMode(VIEW_MODE);
        else if (mode.equals(VIEW_MODE))
            changeMode(EDIT_MODE);
    }

    @Override
    public void onSaveTransaction(String productSKU, Date date, ProductStockRecord.TransactionType transactionType, Integer inputStocks, Double price) throws AppException {
        boolean hasError = false;
        if(productSKU == null) {
            getView().setInvalidProductError();
            hasError = true;
        }
        if(inputStocks == null || inputStocks < 1) {
            getView().setInvalidAmountError();
            hasError = true;
        }
        if(price == null || price < 0.0) {
            getView().setInvalidPriceError();
            hasError = true;
        }

        if(hasError) {
            throw new AppException(Error.USER_INVALID_INPUT);
        }

        Product product = getRealmDataManager().getProductBySKU(productSKU);

        ProductStockRecord record = null;
        if (mode.equals(EDIT_MODE)) {
            if (!isValidEditOrDeleteStock(productSKU, inputStocks, transactionType)) {
                getView().setEditTransactionStockError();
                throw new AppException(Error.FAILED_DATA_UPDATE);
            }

            record = getRealmDataManager().getProductStockRecordById(transaction.getId());
            record.setProduct(product);
            record.setDate(date);
            record.setTransactionType(transactionType);
            record.setStocks(inputStocks);
            record.setPrice(price);
        } else {
            if(transactionType.equals(ProductStockRecord.TransactionType.TRANSACTION_OUT)) {
                int currentStocks = getRealmDataManager().getCurrentProductStockByProductSKU(productSKU);
                if(currentStocks < inputStocks) {
                    getView().setNotEnoughStockError(currentStocks);
                    throw new AppException(Error.USER_INVALID_INPUT);
                }
            }

            record = new ProductStockRecord(product, date, transactionType, inputStocks, price);
        }

        getView().onLoading();
        getRealmDataManager().createOrUpdateProductStockRecord(record, this);
    }

    @Override
    public String getMode() {
        return mode;
    }

    @Override
    public boolean isValidEditOrDeleteStock(String productSKU, Integer inputStocks, ProductStockRecord.TransactionType inputTransactionType) {
        List<ProductStockRecord> records = getRealmDataManager().getProductStockRecordsByProductSKU(productSKU);
        int currentStock = 0;
        for(ProductStockRecord record : records) {
            if(record.getId().equals(transaction.getId())) {
                if(inputTransactionType.equals(ProductStockRecord.TransactionType.TRANSACTION_IN)) {
                    currentStock += inputStocks;
                } else {
                    currentStock -= inputStocks;
                }
            } else {
                if(record.getTransactionType() == ProductStockRecord.TransactionType.TRANSACTION_IN) {
                    currentStock += record.getStocks();
                } else {
                    currentStock -= record.getStocks();
                }
            }

            if(currentStock < 0) {
                return false;
            }
        }
        return true;
    }

    @Override
    public double getRecommendationPrice(String productSKU) {
        List<ProductStockRecord> recordsIn = getRealmDataManager().getProductStockRecordsInByProductSKU(productSKU);
        double sum = 0.0;
        int totalStocks = 0;
        if(!recordsIn.isEmpty()) {
            for(ProductStockRecord record : recordsIn) {
                sum += record.getStocks() * record.getPrice();
                totalStocks += record.getStocks();
            }
            return sum / totalStocks;
        }
        return 0.0;
    }
}
