package com.ppl.d4.temancatat.utils;

public enum Error {
    FAILED_DATA_INSERTION(1001, "Terdapat masalah dalam penambahan data."),
    FAILED_DATA_RETRIEVAL(1002, "Terdapat masalah dalam pengambilan data."),
    FAILED_DATA_UPDATE(1003, "Terdapat masalah dalam pengubahan data."),
    FAILED_DATA_DELETION(1004, "Terdapat masalah dalam penghapusan data."),
    USER_INVALID_INPUT(1101, "Terdapat kesalahan input.");

    private final int errorCode;
    private final String errorMessage;

    Error(int errorCode, String errorMessage) {
        this.errorCode = errorCode;
        this.errorMessage = errorMessage;
    }

    public int getErrorCode() {
        return this.errorCode;
    }

    public String getErrorMessage() {
        return this.errorMessage;
    }

}