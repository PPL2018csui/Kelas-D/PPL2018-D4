package com.ppl.d4.temancatat.presenters.implementations;

import android.widget.Toast;

import com.ppl.d4.temancatat.R;
import com.ppl.d4.temancatat.models.Product;
import com.ppl.d4.temancatat.models.StockUnit;
import com.ppl.d4.temancatat.models.manager.RealmDataManager;
import com.ppl.d4.temancatat.presenters.interfaces.AddProductPresenterInterface;
import com.ppl.d4.temancatat.ui.views.AddProductView;
import com.ppl.d4.temancatat.utils.AppException;
import com.ppl.d4.temancatat.utils.Error;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import javax.inject.Inject;
import org.apache.commons.lang3.math.NumberUtils;

public class AddProductPresenter<V extends AddProductView> extends BasePresenter<V> implements AddProductPresenterInterface<V>, RealmDataManager.OnTransactionCallback {

    private static final String TAG = "Add Product Presenter";

    @Inject
    public AddProductPresenter(final RealmDataManager realmDataManager) {
        super(realmDataManager);
    }

    @Override
    public void onRealmSuccess() {
        getView().onFinishLoading();
        getView().showToastMessage(R.string.barang_toast_success_string, Toast.LENGTH_SHORT);
        getView().openSearchProductActivity();
    }

    @Override
    public void onRealmError(Throwable e) {
        getView().showErrorMessage(e.toString());
    }

    @Override
    public List<String> getStockUnitsName() {
        List<StockUnit> stockUnits = getRealmDataManager().getAllStockUnits();
        List<String> stockUnitsName = new ArrayList<>();

        for (StockUnit stockUnit: stockUnits) {
            stockUnitsName.add(stockUnit.getName());
        }

        return stockUnitsName;
    }

    @Override
    public void onSaveProduct(String name, String description, String stockUnitName, String productSKU) throws AppException {

        getView().clearErrorOnAllFields();

        boolean hasError = false;

        if (!isText(name, true)) {
            getView().onErrorProductNameField(R.string.error_message_name_field_empty);
            hasError = true;
        }

        if (!isText(productSKU, true)) {
            getView().onErrorProductSKUField(R.string.error_message_SKU_field_empty);
            hasError = true;
        }

        if (!isText(description, true)) {
            getView().onErrorProductDescriptionField(R.string.error_message_desc_field_empty);
            hasError = true;
        }

        Product productBySKU = getRealmDataManager().getProductBySKU(productSKU);

        if (productBySKU != null) {
            getView().onErrorProductSKUField(R.string.error_message_SKU_already_exist);
            hasError = true;
        }

        StockUnit stockUnit = getStockUnitByName(stockUnitName);

        if (!isStockUnitPresent(stockUnit)) {
            hasError = true;
        }

        if (hasError) {
            throw new AppException(Error.USER_INVALID_INPUT);
        }

        Product product = new Product(name, description, stockUnit, productSKU);
        getView().onLoading();
        getRealmDataManager().createOrUpdateProduct(product, this);
    }

    @Override
    public StockUnit getStockUnitByName(String name) {
        return getRealmDataManager().getStockUnitByName(name);
    }

    @Override
    public String generateDefaultSKU(String productName, String productStockUnit) {
        return (productName + "-" + productStockUnit).toUpperCase(Locale.ROOT);
    }

    @Override
    public void closeRealm() {
        super.closeRealm();
    }

    @Override
    public boolean isStockUnitPresent(StockUnit stockUnit) {
        if (stockUnit == null) {
            return false;
        }
        return true;
    }

    @Override
    public boolean isText(String editTextString, boolean required) {

        if ( required && editTextString.isEmpty()) {
            return false;
        }

        return true;
    }

    @Override
    public boolean isNumber(String editTextString, boolean required) {
        String number = editTextString;

        if(!required) {
            return true;
        } else if (number.isEmpty()) {
            return false;
        }

        if (NumberUtils.isNumber(number)) {
            return true;
        }

        return false;
    }
}
