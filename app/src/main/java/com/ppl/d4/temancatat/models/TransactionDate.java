package com.ppl.d4.temancatat.models;

public class TransactionDate implements TransactionListItem {

    private String date;

    public TransactionDate(String date) {
        this.date = date;
    }

    public String getDate() {
        return date;
    }

    @Override
    public int getType() {
        return TYPE_DATE;
    }
}
