package com.ppl.d4.temancatat.ui.fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.view.View;
import android.view.WindowManager;

import com.ppl.d4.temancatat.di.components.ActivityComponent;
import com.ppl.d4.temancatat.ui.activities.BaseActivity;
import com.ppl.d4.temancatat.ui.views.BaseView;
import com.ppl.d4.temancatat.utils.ProgressBarHandler;

import butterknife.Unbinder;


public abstract class BaseFragment extends Fragment implements BaseView {

    private BaseActivity mActivity;
    private Unbinder mUnBinder;
    private ProgressBarHandler mProgressBarHandler;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mProgressBarHandler = new ProgressBarHandler(getContext());
        setHasOptionsMenu(false);
        mProgressBarHandler = new ProgressBarHandler(getContext());
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        setUp(view);
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof BaseActivity) {
            BaseActivity activity = (BaseActivity) context;
            this.mActivity = activity;
            activity.onFragmentAttached();
        }
    }

    @Override
    public void showErrorMessage(String message) {
        showSnackbar(message);
    }

    @Override
    public void showErrorMessage(int messageId) {
        showSnackbar(getResources().getString(messageId));
    }

    @Override
    public void onLoading() {
        mProgressBarHandler.show();
        getActivity().getWindow().addFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
    }

    @Override
    public void onFinishLoading() {
        mProgressBarHandler.hide();
        getActivity().getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
    }

    public ActivityComponent getActivityComponent() {
        if (mActivity != null) {
            return mActivity.getActivityComponent();
        }
        return null;
    }

    public void setUnBinder(Unbinder unBinder) {
        mUnBinder = unBinder;
    }

    protected abstract void setUp(View view);

    private void showSnackbar(String message) {
        Snackbar snackbar = Snackbar.make(
                getActivity().findViewById(android.R.id.content),
                message,
                Snackbar.LENGTH_LONG);

        snackbar.show();
    }

    @Override
    public void onDestroy() {
        if (mUnBinder != null) {
            mUnBinder.unbind();
        }
        super.onDestroy();
    }

    @Override
    public void onDetach() {
        mActivity = null;
        super.onDetach();
    }


    public interface Callback {

        void onFragmentAttached();

        void onFragmentDetached(String tag);
    }
}
