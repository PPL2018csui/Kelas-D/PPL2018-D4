package com.ppl.d4.temancatat.utils;

public class AppException extends RuntimeException {
    private int errorCode;
    private String errorMessage;

    public AppException(Error error) {
        this.errorCode = error.getErrorCode();
        this.errorMessage = error.getErrorMessage();
    }

    public AppException(Error error, String detail) {
        this.errorCode = error.getErrorCode();
        this.errorMessage = detail;
    }

    public int getErrorCode() {
        return this.errorCode;
    }

    public String getErrorMessage() {
        return this.errorMessage;
    }

    public String getFormattedMessage() {
        return this.errorMessage + " (" + this.errorCode + ").";
    }

}
