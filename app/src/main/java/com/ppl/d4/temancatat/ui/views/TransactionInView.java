package com.ppl.d4.temancatat.ui.views;

import com.ppl.d4.temancatat.models.TransactionListItem;

import java.util.List;

public interface TransactionInView extends BaseView {
    void onSetDefaultIcon();

    void onPrepareTransactionList();

    void showTransactionList(List<TransactionListItem> transactionList);

    void showNoTransactionPlaceholder();
}
