package com.ppl.d4.temancatat.ui.activities;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;

import com.ppl.d4.temancatat.BuildConfig;
import com.ppl.d4.temancatat.models.manager.RealmDataManager;
import com.ppl.d4.temancatat.seeders.DummySeeder;
import com.ppl.d4.temancatat.seeders.MandatorySeeder;
import com.ppl.d4.temancatat.seeders.Seeder;

import io.realm.Realm;

public class SplashActivity extends AppCompatActivity {

    private final int SPLASH_DISPLAY_LENGTH = 1000;

    private void seedData() {
        Realm realm = Realm.getDefaultInstance();
        RealmDataManager realmDataManager = new RealmDataManager(realm);

        Seeder seeder = new MandatorySeeder();
        seeder.seed(realmDataManager);

        if (BuildConfig.DUMMY_SEEDER_ENABLED == true) {
            seeder = new DummySeeder();
            seeder.seed(realmDataManager);
        }

        realmDataManager.closeRealm();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        seedData();

        /* New Handler to start the TransactionListActivity
         * and close this SplashActivity-Screen after some seconds.*/
        new Handler().postDelayed(new Runnable(){
            @Override
            public void run() {
                Intent mainIntent = new Intent(SplashActivity.this,TransactionListActivity.class);
                SplashActivity.this.startActivity(mainIntent);
                SplashActivity.this.finish();
            }
        }, SPLASH_DISPLAY_LENGTH);
    }
}