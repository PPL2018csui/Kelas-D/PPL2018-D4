package com.ppl.d4.temancatat.ui.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.ppl.d4.temancatat.R;
import com.ppl.d4.temancatat.di.components.ActivityComponent;
import com.ppl.d4.temancatat.models.TransactionListItem;
import com.ppl.d4.temancatat.presenters.interfaces.TransactionOutPresenterInterface;
import com.ppl.d4.temancatat.ui.adapters.TransactionListAdapter;
import com.ppl.d4.temancatat.ui.views.TransactionOutView;
import com.ppl.d4.temancatat.utils.Error;

import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;

public class TransactionOutFragment extends BaseFragment implements TransactionOutView {

    private static final String TAG = "TransactionOutFragment";

    @Inject
    TransactionOutPresenterInterface<TransactionOutView> mPresenter;TransactionListAdapter mTransactionListAdapter;

    @Inject
    LinearLayoutManager mLayoutManager;

    @BindView(R.id.transaction_list_recyclerview)
    RecyclerView transactionListRecyclerView;

    @BindView(R.id.no_transaction_linearLayout)
    LinearLayout noTransactionLinearLayout;

    @BindView(R.id.no_transaction_icon_imageView)
    ImageView noTransactionIconImageView;

    public static TransactionOutFragment newInstance() {
        Bundle args = new Bundle();
        TransactionOutFragment fragment = new TransactionOutFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.transaction_out_list_fragment, container, false);

        ActivityComponent component = getActivityComponent();
        if (component != null) {
            component.inject(this);
            setUnBinder(ButterKnife.bind(this,view));
            mPresenter.onAttach(this);
        }

        return view;
    }

    @Override
    protected void setUp(View view) {
        onSetDefaultIcon();
    }

    @Override
    public void onSetDefaultIcon() {
        noTransactionIconImageView.setImageResource(R.drawable.transaction_out);
        ActionBar actionBar = ((AppCompatActivity)getActivity()).getSupportActionBar();
        if(actionBar != null) {
            Log.d(TAG, "ActionBar is not null");
            actionBar.setIcon(R.drawable.transaction_out_white);
        }
    }

    @Override
    public void onPrepareTransactionList() {
        List<TransactionListItem> transactionList = mPresenter.getTransactionOutItems();

        if (transactionList == null) {
            showNoTransactionPlaceholder();
            showErrorMessage(Error.FAILED_DATA_RETRIEVAL.getErrorMessage());
        }
        else if (transactionList.size() == 0){
            showNoTransactionPlaceholder();
        } else {
            showTransactionList(transactionList);
        }
    }

    @Override
    public void showNoTransactionPlaceholder() {
        transactionListRecyclerView.setVisibility(View.INVISIBLE);
        noTransactionLinearLayout.setVisibility(View.VISIBLE);
    }

    public void showTransaction() {
        transactionListRecyclerView.setVisibility(View.VISIBLE);
        noTransactionLinearLayout.setVisibility(View.INVISIBLE);
    }

    @Override
    public void showTransactionList(List<TransactionListItem> transactionList) {
        mLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);

        showTransaction();
        transactionListRecyclerView.setLayoutManager(mLayoutManager);

        mTransactionListAdapter = new TransactionListAdapter(getContext(), transactionList);
        transactionListRecyclerView.setAdapter(mTransactionListAdapter);
    }

    @Override
    public void onStart() {
        super.onStart();
        onPrepareTransactionList();
    }
}
