package com.ppl.d4.temancatat.ui.adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.ppl.d4.temancatat.R;
import com.ppl.d4.temancatat.models.LegendItem;
import com.ppl.d4.temancatat.utils.PriceUtil;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class LegendAdapter extends RecyclerView.Adapter<LegendAdapter.LegendViewHolder> {

    private static final String TAG = "LegendAdapter";

    private Context context;
    private List<LegendItem> legendItems;

    public LegendAdapter(Context context, List<LegendItem> legendItems) {
        this.context = context;
        this.legendItems = legendItems;
    }

    @NonNull
    @Override
    public LegendViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(context);
        View view = inflater.inflate(R.layout.legend_item, parent, false);
        return new LegendViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull LegendViewHolder holder, int position) {
        LegendItem legendItem = legendItems.get(position);

        holder.legendImageView.setBackgroundColor(legendItem.getColor());
        holder.productNameTextView.setText(legendItem.getProductSKU());
        holder.priceAmountTextView.setText("Rp"+ PriceUtil.convertPriceFormatID(legendItem.getProductPrice()));
        holder.productAmountTextView.setText(Integer.toString(legendItem.getProductAmount()));
    }

    @Override
    public int getItemCount() {
        return legendItems.size();
    }

    class LegendViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.legend_imageview)
        ImageView legendImageView;

        @BindView(R.id.product_name_textview)
        TextView productNameTextView;

        @BindView(R.id.price_amount_textview)
        TextView priceAmountTextView;

        @BindView(R.id.product_amount_textview)
        TextView productAmountTextView;

        @BindView(R.id.legend_item_layout)
        LinearLayout legendItemLayout;

        public LegendViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
