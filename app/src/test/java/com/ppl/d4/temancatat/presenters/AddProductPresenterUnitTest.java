package com.ppl.d4.temancatat.presenters;

import com.ppl.d4.temancatat.R;
import com.ppl.d4.temancatat.models.Product;
import com.ppl.d4.temancatat.models.StockUnit;
import com.ppl.d4.temancatat.models.manager.RealmDataManager;
import com.ppl.d4.temancatat.presenters.implementations.AddProductPresenter;
import com.ppl.d4.temancatat.ui.views.AddProductView;
import com.ppl.d4.temancatat.utils.AppException;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.ArrayList;
import java.util.List;

import static junit.framework.Assert.assertTrue;
import static junit.framework.Assert.assertFalse;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class AddProductPresenterUnitTest {

    @Mock
    RealmDataManager mRealmDataManager;

    @Mock
    AddProductView mView;

    private AddProductPresenter presenter;
    private List<StockUnit> expectedStockUnits;

    private int expectedStockUnitSize;
    private int invalidStockUnitSize;

    private String expectedStockUnitName;
    private String invalidStockUnitName;

    private String expectedProductName;
    private String expectedProductDesc;
    private String expectedProductSKU;

    private String invalidProductName;
    private String invalidProductDesc;

    private Product expectedProduct;

    private String expectedText;
    private String expectedNumberInt;
    private String expectedNumberFloat;
    private String invalidNumber;
    private String emptyString;

    private String invalidSKUGenerator;
    private String expectedSKUGenerator;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);

        presenter = new AddProductPresenter(mRealmDataManager);
        presenter.onAttach(mView);

        expectedStockUnitSize = 5;
        invalidStockUnitSize = 10;

        expectedStockUnitName = "Anab Ganteng";
        invalidStockUnitName = "Anab Jahat";

        expectedStockUnits = new ArrayList<>();

        for (int i = 0; i < expectedStockUnitSize; i++) {
            StockUnit stockUnit = new StockUnit(expectedStockUnitName);
            expectedStockUnits.add(stockUnit);
        }

        when(mRealmDataManager.getAllStockUnits()).thenReturn(expectedStockUnits);

        when(mRealmDataManager.getStockUnitByName(expectedStockUnitName))
                .thenReturn(expectedStockUnits.get(0));

        invalidProductName = "";
        invalidProductDesc = "";

        expectedProductName = "Anab Ganteng";
        expectedProductDesc = "Hahahaha";
        expectedProductSKU = "Foto sapa tuh";

        expectedProduct = new Product(
                expectedProductName,
                expectedProductDesc,
                expectedStockUnits.get(0),
                expectedProductSKU);

        expectedText = "susu 500ml";
        expectedNumberInt = "123";
        expectedNumberFloat = "123.321";
        invalidNumber = "A22B9S";
        emptyString = "";

        expectedSKUGenerator = (expectedProductName+"-"+expectedStockUnitName).toUpperCase();
        invalidSKUGenerator = (expectedProductName+"-"+expectedStockUnitName);
    }

    @Test
    public void testStockUnitSizeValid() {
        List<String> stockUnits = presenter.getStockUnitsName();
        assertTrue(stockUnits.size() == expectedStockUnitSize);
    }

    @Test
    public void testStockUnitSizeInvalid() {
        List<String> stockUnits = presenter.getStockUnitsName();
        assertFalse(stockUnits.size() == invalidStockUnitSize);
    }

    @Test
    public void testStockUnitNameValid() {
        List<String> stockUnits = presenter.getStockUnitsName();
        for (String stockUnit: stockUnits) {
            assertTrue(stockUnit.equals(expectedStockUnitName));
        }
    }

    @Test
    public void testStockUnitNameInvalid() {
        List<String> stockUnits = presenter.getStockUnitsName();
        for (String stockUnit: stockUnits) {
            assertFalse(stockUnit.equals(invalidStockUnitName));
        }
    }

    @Test
    public void testGetStockUnitByNameValid() {
        StockUnit stockUnit = presenter.getStockUnitByName(expectedStockUnitName);
        assertTrue(stockUnit.getName().equals(expectedStockUnitName));
    }

    @Test
    public void testGetStockUnitByNameInvalid() {
        StockUnit stockUnit = presenter.getStockUnitByName(expectedStockUnitName);
        assertFalse(stockUnit.getName().equals(invalidStockUnitName));
    }

    @Test
    public void testOnSaveProductSuccess() {
        when(mRealmDataManager.getProductBySKU(expectedProductSKU)).thenReturn(null);

        presenter.onSaveProduct(
                expectedProductName,
                expectedProductDesc,
                expectedStockUnitName,
                expectedProductSKU);

        verify(mView, times(1)).onLoading();
        verify(mRealmDataManager, times(1)).createOrUpdateProduct(any(), any());
    }

    @Test(expected = AppException.class)
    public void testOnSaveProductDuplicateSKU() {
        when(mRealmDataManager.getProductBySKU(expectedProductSKU)).thenReturn(expectedProduct);

        presenter.onSaveProduct(
                expectedProductName,
                expectedProductDesc,
                expectedStockUnitName,
                expectedProductSKU);

        verify(mView, times(1)).onErrorProductSKUField(R.string.error_message_SKU_already_exist);
    }

    @Test(expected = AppException.class)
    public void testOnSaveProductFailedAtNameField() {
        presenter.onSaveProduct(
                invalidProductName,
                expectedProductDesc,
                expectedStockUnitName,
                expectedProductSKU);

        verify(mView, times(1))
                .onErrorProductNameField(R.string.error_message_name_field_empty);
    }

    @Test(expected = AppException.class)
    public void testOnSaveProductFailedAtSKUField() {
        presenter.onSaveProduct(
                expectedProductName,
                expectedProductDesc,
                expectedStockUnitName,
                invalidProductDesc);

        verify(mView, times(1))
                .onErrorProductSKUField(R.string.error_message_desc_field_empty);
    }

    @Test(expected = AppException.class)
    public void testOnSaveProductFailedAtDescField() {
        presenter.onSaveProduct(
                expectedProductName,
                invalidProductDesc,
                expectedStockUnitName,
                expectedProductSKU);

        verify(mView, times(1))
                .onErrorProductDescriptionField(R.string.error_message_desc_field_empty);
    }

    @Test(expected = AppException.class)
    public void testOnSaveProductFailedAtStockUnitField() {
        presenter.onSaveProduct(
                expectedProductName,
                expectedProductDesc,
                null,
                expectedProductSKU);
    }

    @Test
    public void testGenerateDefaultSKUValid() {
        String sKU = presenter.generateDefaultSKU(expectedProductName, expectedStockUnitName);
        assertTrue(sKU.equals(expectedSKUGenerator));
    }

    @Test
    public void testGenerateDefaultSKUInvalid() {
        String sKU = presenter.generateDefaultSKU(expectedProductName, expectedStockUnitName);
        assertFalse(sKU.equals(invalidSKUGenerator));
    }

    @Test
    public void testOnRealmSuccess() {
        presenter.onRealmSuccess();

        verify(mView, times(1)).onFinishLoading();
        verify(mView, times(1)).openSearchProductActivity();
    }

    @Test
    public void testOnRealmError() {
        presenter.onRealmError(new Throwable());
    }

    @Test
    public void testOnRealmClose() {
        presenter.closeRealm();
        verify(mRealmDataManager, times(1)).closeRealm();
    }

    @Test
    public void testOnAttach() {
        presenter.onAttach(mView);
        assert(presenter.getView()).equals(mView);
    }

    @Test
    public void testOnDetach() {
        presenter.onDetach();
    }

    @Test
    public void testGetView() {
        presenter.onAttach(mView);
        assert(presenter.getView()).equals(mView);
    }

    @Test
    public void testIsTextValid() {
        assertTrue (presenter.isText(expectedText,true));
    }

    @Test
    public void testIsTextEmpty() {
        assertFalse (presenter.isText(emptyString,true));
    }

    @Test
    public void testIsNumberValid() {
        assertTrue (presenter.isNumber(expectedNumberInt,true));
        assertTrue (presenter.isNumber(expectedNumberFloat,true));
    }

    @Test
    public void testIsNumberInvalid() {
        assertFalse (presenter.isNumber(invalidNumber,true));
    }

    @Test
    public void testIsNumberEmpty() {
        assertFalse (presenter.isNumber(emptyString,true));
    }


    @Test
    public void testIsTextNotRequired() {
        assertTrue (presenter.isText(emptyString,false));
    }

    @Test
    public void testIsNumberNotRequired() {
        assertTrue (presenter.isNumber(emptyString,false));
    }
}
