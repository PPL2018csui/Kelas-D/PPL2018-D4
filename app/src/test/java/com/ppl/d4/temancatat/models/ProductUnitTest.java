package com.ppl.d4.temancatat.models;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;

import static junit.framework.Assert.assertFalse;
import static junit.framework.Assert.assertTrue;
import static org.mockito.Mockito.when;

public class ProductUnitTest {

    public static final int PRODUCT_NUMBER = 100000;

    private String expectedName;
    private String invalidName;
    private String expectedSKUDefault;
    private String invalidSKUDefault;
    private String expectedSKUCustom;
    private String invalidSKUCustom;
    private String expectedDescription;
    private String invalidDescription;
    private String expectedStockUnitName;
    private String invalidStockUnitName;

    private List<Product> productList;

    private Product product;
    private Product customProduct;

    @Mock
    private StockUnit mStockUnit;

    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);

        expectedName = "Anab Ganteng";
        invalidName = "Anab Jahat";

        expectedDescription = "Anab Ganteng";
        invalidDescription = "Anab Jahat";

        expectedName = "Anab Ganteng";
        invalidName = "Anab Jahat";

        expectedStockUnitName = "Anab Ganteng";
        invalidStockUnitName = "Anab Jahat";

        expectedSKUDefault = expectedName + ' ' + expectedStockUnitName;
        invalidSKUDefault = "Anab Jahat";

        expectedSKUCustom = "Anab Ganteng";
        invalidSKUCustom = "Anab Jahat";

        when(mStockUnit.getName()).thenReturn(expectedStockUnitName);
        customProduct = new Product();
        product = new Product(expectedName, expectedDescription, mStockUnit, expectedSKUDefault);

        productList = new ArrayList<Product>();

        for (int i = 0; i < PRODUCT_NUMBER; i++) {
            Product tempProduct = new Product(expectedName, expectedDescription, mStockUnit, expectedSKUDefault);
            productList.add(tempProduct);
        }
    }

    @Test
    public void testNameValid() {
        assertTrue(product.getName().equals(expectedName));
    }

    @Test
    public void testNameInvalid() {
        assertFalse(product.getName().equals(invalidName));
    }

    @Test
    public void testSKUDefaultValid() {
        assertTrue(product.getSKU().equals(expectedSKUDefault));
    }

    @Test
    public void testSKUDefaultInvalid() {
        assertFalse(product.getSKU().equals(invalidSKUDefault));
    }

    @Test
    public void testSKUCustomValid() {
        customProduct.setSKU(expectedSKUCustom);
        assertTrue(customProduct.getSKU().equals(expectedSKUCustom));
    }

    @Test
    public void testSKUCustomInvalid() {
        customProduct.setSKU(expectedSKUCustom);
        assertFalse(customProduct.getSKU().equals(invalidSKUCustom));
    }

    @Test
    public void testDescriptionValid() {
        assertTrue(product.getDescription().equals(expectedDescription));
    }

    @Test
    public void testDescriptionInvalid() {
        assertFalse(product.getDescription().equals(invalidDescription));
    }

    @Test
    public void testStockUnitValid() {
        assertTrue(product.getStockUnit().getName().equals(expectedStockUnitName));
    }

    @Test
    public void testStockUnitInvalid() {
        assertFalse(product.getStockUnit().getName().equals(invalidStockUnitName));
    }

    @Test
    public void testSetNameValid() {
        customProduct.setName(expectedName);
        assertTrue(customProduct.getName().equals(expectedName));
    }

    @Test
    public void testSetNameInvalid() {
        customProduct.setName(expectedName);
        assertFalse(customProduct.getName().equals(invalidName));
    }

    @Test
    public void testSetDescriptionValid() {
        customProduct.setDescription(expectedDescription);
        assertTrue(customProduct.getDescription().equals(expectedDescription));
    }

    @Test
    public void testSetDescriptionInvalid() {
        customProduct.setDescription(expectedDescription);
        assertFalse(customProduct.getDescription().equals(invalidName));
    }

    @Test
    public void testSetStockUnitValid() {
        customProduct.setStockUnit(mStockUnit);
        assertTrue(customProduct.getStockUnit().getName().equals(expectedStockUnitName));
    }

    @Test
    public void testSetStockUnitInvalid() {
        customProduct.setStockUnit(mStockUnit);
        assertFalse(customProduct.getStockUnit().getName().equals(invalidStockUnitName));
    }

    @Test
    public void testToStringValid() {
        assertTrue(product.toString().equals(expectedName));
    }

    @Test
    public void testToStringInvalid() {
        assertFalse(product.toString().equals(invalidName));
    }
}

