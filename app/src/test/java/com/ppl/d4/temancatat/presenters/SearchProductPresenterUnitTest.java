package com.ppl.d4.temancatat.presenters;

import com.ppl.d4.temancatat.models.Product;
import com.ppl.d4.temancatat.models.manager.RealmDataManager;
import com.ppl.d4.temancatat.presenters.implementations.SearchProductPresenter;
import com.ppl.d4.temancatat.ui.views.SearchProductView;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.ArrayList;
import java.util.List;

import static junit.framework.Assert.assertFalse;
import static junit.framework.Assert.assertTrue;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class SearchProductPresenterUnitTest {

    public static final int PRODUCT_NUMBER = 10;

    @Mock
    RealmDataManager mRealmDataManager;

    @Mock
    SearchProductView mView;

    @Mock
    Product mProduct;

    private SearchProductPresenter mPresenter;
    private List<Product> productList;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);

        mPresenter = new SearchProductPresenter(mRealmDataManager);
        mPresenter.onAttach(mView);

        productList = new ArrayList<Product>();

        for(int i = 0; i < PRODUCT_NUMBER; i++) {
            productList.add(mProduct);
        }

        when(mRealmDataManager.getAllProducts()).thenReturn(productList);
        when(mRealmDataManager.getProductsFilterBySKU(anyString())).thenReturn(productList);
    }

    @Test
    public void testGetAllProductsValid() {
        List<Product> resProductList = mPresenter.getAllProducts();
        assertTrue(resProductList.size() == PRODUCT_NUMBER);

        verify(mView, times(1)).onLoading();
        verify(mView, times(1)).onFinishLoading();
    }

    @Test
    public void testGetAllProductsInvalid() {
        List<Product> resProductList = mPresenter.getAllProducts();
        assertFalse(resProductList == null);
    }

    @Test
    public void testFilterProductsBySKUValid() {
        List<Product> resProductList = mPresenter.filterProductsBySKU("");
        assertTrue(resProductList.size() == PRODUCT_NUMBER);

        verify(mView, times(1)).onLoading();
        verify(mView, times(1)).onFinishLoading();
    }

    @Test
    public void testFilterProductsBySKUInvalid() {
        List<Product> resProductList = mPresenter.filterProductsBySKU("");
        assertFalse(resProductList == null);
    }

    @Test
    public void testOnRealmSuccess() {
        mPresenter.onRealmSuccess();
    }

    @Test
    public void testOnRealmError() {
        mPresenter.onRealmError(new Throwable());
    }

    @Test
    public void testOnRealmClose() {
        mPresenter.closeRealm();
        verify(mRealmDataManager, times(1)).closeRealm();
    }

    @Test
    public void testOnAttach() {
        mPresenter.onAttach(mView);
        assert(mPresenter.getView()).equals(mView);
    }

    @Test
    public void testOnDetach() {
        mPresenter.onDetach();
    }

    @Test
    public void testGetView() {
        mPresenter.onAttach(mView);
        assert(mPresenter.getView()).equals(mView);
    }

}
