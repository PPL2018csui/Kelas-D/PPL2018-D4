package com.ppl.d4.temancatat.presenters;

import com.ppl.d4.temancatat.R;
import com.ppl.d4.temancatat.models.Product;
import com.ppl.d4.temancatat.models.ProductStockRecord;
import com.ppl.d4.temancatat.models.StockUnit;
import com.ppl.d4.temancatat.models.manager.RealmDataManager;
import com.ppl.d4.temancatat.presenters.implementations.AddProductPresenter;
import com.ppl.d4.temancatat.presenters.implementations.EditProductPresenter;
import com.ppl.d4.temancatat.ui.views.AddProductView;
import com.ppl.d4.temancatat.ui.views.EditProductView;
import com.ppl.d4.temancatat.utils.AppException;
import com.ppl.d4.temancatat.utils.DateUtil;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import static junit.framework.Assert.assertFalse;
import static junit.framework.Assert.assertTrue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class EditProductPresenterUnitTest {

    @Mock
    RealmDataManager mRealmDataManager;

    @Mock
    EditProductView mView;

    @Mock
    private Date mDate;

    private EditProductPresenter presenter;
    private List<StockUnit> expectedStockUnits;

    private int expectedStockUnitSize;
    private int invalidStockUnitSize;

    private String expectedStockUnitName;
    private String invalidStockUnitName;

    private String expectedProductName;
    private String expectedProductDesc;
    private String expectedProductSKU;

    private String invalidProductName;
    private String invalidProductDesc;

    private Product expectedProduct;

    private String expectedText;
    private String expectedNumberInt;
    private String expectedNumberFloat;
    private String invalidNumber;
    private String emptyString;

    private String invalidSKUGenerator;
    private String expectedSKUGenerator;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);

        presenter = new EditProductPresenter(mRealmDataManager);
        presenter.onAttach(mView);

        expectedStockUnitSize = 5;
        invalidStockUnitSize = 10;

        expectedStockUnitName = "Anab Ganteng";
        invalidStockUnitName = "Anab Jahat";

        expectedStockUnits = new ArrayList<>();

        for (int i = 0; i < expectedStockUnitSize; i++) {
            StockUnit stockUnit = new StockUnit(expectedStockUnitName);
            expectedStockUnits.add(stockUnit);
        }

        when(mRealmDataManager.getAllStockUnits()).thenReturn(expectedStockUnits);

        when(mRealmDataManager.getStockUnitByName(expectedStockUnitName))
                .thenReturn(expectedStockUnits.get(0));

        invalidProductName = "";
        invalidProductDesc = "";

        expectedProductName = "Anab Ganteng";
        expectedProductDesc = "Hahahaha";
        expectedProductSKU = "Foto sapa tuh";

        expectedProduct = new Product(
                expectedProductName,
                expectedProductDesc,
                expectedStockUnits.get(0),
                expectedProductSKU);

        expectedText = "susu 500ml";
        expectedNumberInt = "123";
        expectedNumberFloat = "123.321";
        invalidNumber = "A22B9S";
        emptyString = "";

        expectedSKUGenerator = (expectedProductName+"-"+expectedStockUnitName).toUpperCase();
        invalidSKUGenerator = (expectedProductName+"-"+expectedStockUnitName);
    }

    @Test
    public void testStockUnitSizeValid() {
        List<String> stockUnits = presenter.getStockUnitsName();
        assertTrue(stockUnits.size() == expectedStockUnitSize);
    }

    @Test
    public void testStockUnitSizeInvalid() {
        List<String> stockUnits = presenter.getStockUnitsName();
        assertFalse(stockUnits.size() == invalidStockUnitSize);
    }

    @Test
    public void testStockUnitNameValid() {
        List<String> stockUnits = presenter.getStockUnitsName();
        for (String stockUnit: stockUnits) {
            assertTrue(stockUnit.equals(expectedStockUnitName));
        }
    }

    @Test
    public void testStockUnitNameInvalid() {
        List<String> stockUnits = presenter.getStockUnitsName();
        for (String stockUnit: stockUnits) {
            assertFalse(stockUnit.equals(invalidStockUnitName));
        }
    }

    @Test
    public void testGetStockUnitByNameValid() {
        StockUnit stockUnit = presenter.getStockUnitByName(expectedStockUnitName);
        assertTrue(stockUnit.getName().equals(expectedStockUnitName));
    }

    @Test
    public void testGetStockUnitByNameInvalid() {
        StockUnit stockUnit = presenter.getStockUnitByName(expectedStockUnitName);
        assertFalse(stockUnit.getName().equals(invalidStockUnitName));
    }
    
    @Test(expected = AppException.class)
    public void testOnSaveProductFailedAtNameField() {
        presenter.onSaveProduct(
                invalidProductName,
                expectedProductDesc,
                expectedStockUnitName,
                expectedProductSKU);

        verify(mView, times(1))
                .onErrorProductNameField(R.string.error_message_name_field_empty);
    }

    @Test(expected = AppException.class)
    public void testOnSaveProductFailedAtSKUField() {
        presenter.onSaveProduct(
                expectedProductName,
                expectedProductDesc,
                expectedStockUnitName,
                invalidProductDesc);

        verify(mView, times(1))
                .onErrorProductSKUField(R.string.error_message_desc_field_empty);
    }

    @Test(expected = AppException.class)
    public void testOnSaveProductFailedAtDescField() {
        presenter.onSaveProduct(
                expectedProductName,
                invalidProductDesc,
                expectedStockUnitName,
                expectedProductSKU);

        verify(mView, times(1))
                .onErrorProductDescriptionField(R.string.error_message_desc_field_empty);
    }

    @Test(expected = AppException.class)
    public void testOnSaveProductFailedAtStockUnitField() {
        presenter.onSaveProduct(
                expectedProductName,
                expectedProductDesc,
                null,
                expectedProductSKU);
    }

    @Test
    public void testGenerateDefaultSKUValid() {
        String sKU = presenter.generateDefaultSKU(expectedProductName, expectedStockUnitName);
        assertTrue(sKU.equals(expectedSKUGenerator));
    }

    @Test
    public void testGenerateDefaultSKUInvalid() {
        String sKU = presenter.generateDefaultSKU(expectedProductName, expectedStockUnitName);
        assertFalse(sKU.equals(invalidSKUGenerator));
    }

    @Test
    public void testOnRealmSuccess() {
        presenter.onRealmSuccess();
        verify(mView, times(1)).onFinishLoading();
        verify(mView, times(1)).finish();
    }

    @Test
    public void testOnRealmError() {
        presenter.onRealmError(new Throwable());
    }

    @Test
    public void testOnRealmClose() {
        presenter.closeRealm();
        verify(mRealmDataManager, times(1)).closeRealm();
    }

    @Test
    public void testOnAttach() {
        presenter.onAttach(mView);
        assert(presenter.getView()).equals(mView);
    }

    @Test
    public void testOnDetach() {
        presenter.onDetach();
    }

    @Test
    public void testGetView() {
        presenter.onAttach(mView);
        assert(presenter.getView()).equals(mView);
    }

    @Test
    public void testIsTextValid() {
        assertTrue (presenter.isText(expectedText,true));
    }

    @Test
    public void testIsTextEmpty() {
        assertFalse (presenter.isText(emptyString,true));
    }

    @Test
    public void testIsNumberValid() {
        assertTrue (presenter.isNumber(expectedNumberInt,true));
        assertTrue (presenter.isNumber(expectedNumberFloat,true));
    }

    @Test
    public void testIsNumberInvalid() {
        assertFalse (presenter.isNumber(invalidNumber,true));
    }

    @Test
    public void testIsNumberEmpty() {
        assertFalse (presenter.isNumber(emptyString,true));
    }


    @Test
    public void testIsTextNotRequired() {
        assertTrue (presenter.isText(emptyString,false));
    }

    @Test
    public void testIsNumberNotRequired() {
        assertTrue (presenter.isNumber(emptyString,false));
    }

    @Test
    public void testChangeModeViewValid() throws AppException {
        presenter.changeMode(presenter.VIEW_MODE);
        verify(mView, times(1)).onViewModeChanged();
    }

    @Test
    public void testChangeModeEditValid() throws AppException {
        presenter.changeMode(presenter.EDIT_MODE);
        verify(mView, times(1)).onEditModeChanged();
    }

    @Test
    public void testChangeModeInvalid() throws AppException {
        presenter.changeMode("A");
        verify(mView, times(0)).onEditModeChanged();
        verify(mView, times(0)).onViewModeChanged();
    }

    @Test
    public void testToggleViewMode() throws AppException {
        presenter.changeMode(presenter.VIEW_MODE);
        presenter.toggleEdit();
        assert(presenter.getMode().equals(presenter.EDIT_MODE));
    }

    @Test
    public void testToggleEditMode() throws AppException {
        presenter.changeMode(presenter.EDIT_MODE);
        presenter.toggleEdit();
        assert(presenter.getMode().equals(presenter.VIEW_MODE));
    }

    @Test
    public void testToggleInvalidMode() throws AppException {
        presenter.changeMode("A");
        presenter.toggleEdit();
        assert(presenter.getMode().equals("A"));
    }

    @Test
    public void testChangeModeInvalidArgument() throws AppException {
        verify(mView, times(0)).onViewModeChanged();
        verify(mView, times(0)).onEditModeChanged();
    }

    @Test
    public void testChangeModeNullArgument() throws AppException {
        presenter.changeMode(null);
        assert(presenter.getMode().equals(presenter.VIEW_MODE));
    }

    @Test
    public void testChangeModeSameTwiceArgument() throws AppException {
        presenter.changeMode(presenter.VIEW_MODE);
        presenter.changeMode(presenter.VIEW_MODE);
        verify(mView, times(1)).onViewModeChanged();
    }

    @Test
    public void testOnSaveProductEditSuccess() throws AppException {
        when(mRealmDataManager.getProductBySKU(expectedProductSKU)).thenReturn(expectedProduct);
        presenter.setProductBySKU(expectedProduct.getSKU());
        presenter.changeMode(presenter.EDIT_MODE);
        presenter.onSaveProduct(expectedProductName,
                expectedProductDesc,
                expectedStockUnitName,
                expectedProductSKU);
        verify(mRealmDataManager, times(1)).createOrUpdateProduct(expectedProduct, presenter);
    }

    @Test(expected = AppException.class)
    public void testOnSaveProductEditFail() throws AppException {
        presenter.setProductBySKU(expectedProduct.getSKU());
        presenter.changeMode(presenter.EDIT_MODE);
        presenter.onSaveProduct(invalidProductName, invalidProductDesc, null, expectedProductSKU);
    }

    @Test
    public void testSetProductBySKUNull() throws AppException {
        presenter.setProductBySKU(null);
        verify(mRealmDataManager, times(0)).getProductBySKU(any());
    }

    @Test(expected = AppException.class)
    public void testSetProductRealmError() throws AppException {
        when(mRealmDataManager.getProductBySKU(any())).thenReturn(null);
        presenter.setProductBySKU("Random");
    }

    @Test
    public void testSetProductBySKUDuplicate() throws AppException {
        Product p = mock(Product.class);
        when(p.getSKU()).thenReturn("SameId");
        when(mRealmDataManager.getProductBySKU(any())).thenReturn(p);
        presenter.setProductBySKU(p.getSKU());
        presenter.setProductBySKU(p.getSKU());
        verify(mRealmDataManager, times(1)).getProductBySKU(any());
    }

    @Test
    public void testSetTransactionByIdSuccess() throws AppException {
        Product p = mock(Product.class);
        when(p.getSKU()).thenReturn("SameId");
        when(mRealmDataManager.getProductBySKU(any())).thenReturn(p);
        presenter.setProductBySKU("newId");
        presenter.setProductBySKU("newId");
        verify(mRealmDataManager, times(2)).getProductBySKU(any());
    }

    @Test
    public void testGetTransactionValid() throws AppException {
        Product p = mock(Product.class);
        when(p.getSKU()).thenReturn("SameId");
        when(mRealmDataManager.getProductBySKU(any())).thenReturn(p);
        presenter.setProductBySKU(p.getSKU());
        assert(presenter.getProduct() == p);
    }

    @Test
    public void testGetStockValueValid() {
        Product p = mock(Product.class);
        ProductStockRecord record = mock(ProductStockRecord.class);
        record.setProduct(p);
        int expectedStock = Integer.parseInt(expectedNumberInt);
        when(mRealmDataManager.getCurrentProductStockByProductSKU(p.getSKU())).thenReturn(expectedStock);
        assertTrue(presenter.getStockValue(p.getSKU()) == expectedStock);
    }

    @Test
    public void testGetStockValueInvalid() {
        Product p = mock(Product.class);
        ProductStockRecord record = mock(ProductStockRecord.class);
        record.setProduct(p);
        int expectedStock = Integer.parseInt(expectedNumberInt);
        when(mRealmDataManager.getCurrentProductStockByProductSKU(p.getSKU())).thenReturn(expectedStock+1);
        assertTrue(presenter.getStockValue(p.getSKU()) != expectedStock);
    }

    @Test
    public void testGetStockTransactionInValid() {
        Product p = mock(Product.class);
        ProductStockRecord record = mock(ProductStockRecord.class);
        record.setProduct(p);

        List<ProductStockRecord> productStockRecordList = new ArrayList<ProductStockRecord>();
        productStockRecordList.add(record);

        int expectedTransactionIn = Integer.parseInt(expectedNumberInt);
        when(record.getStocks()).thenReturn(expectedTransactionIn);
        when(mRealmDataManager.getProductStockRecordsInByProductSKU(p.getSKU())).thenReturn(productStockRecordList);
        assertTrue(presenter.getStockTransactionIn(p.getSKU()) == expectedTransactionIn);
    }

    @Test
    public void testStockCountTransactionOutValid() {
        Product p = mock(Product.class);
        ProductStockRecord record = mock(ProductStockRecord.class);
        record.setProduct(p);

        List<ProductStockRecord> productStockRecordList = new ArrayList<ProductStockRecord>();
        productStockRecordList.add(record);

        int expectedTransactionOut = Integer.parseInt(expectedNumberInt);
        when(record.getStocks()).thenReturn(expectedTransactionOut);
        when(mRealmDataManager.getProductStockRecordsOutByProductSKU(p.getSKU())).thenReturn(productStockRecordList);
        assertTrue(presenter.getStockTransactionOut(p.getSKU()) == expectedTransactionOut);
    }

    @Test
    public void testGetMonthlyStock() {
        Product p = mock(Product.class);

        ProductStockRecord record1 = mock(ProductStockRecord.class);
        ProductStockRecord record2 = mock(ProductStockRecord.class);
        record1.setProduct(p);
        record2.setProduct(p);

        when(record1.getDate()).thenReturn(mDate);
        when(record1.getTransactionType()).thenReturn(ProductStockRecord.TransactionType.TRANSACTION_IN);

        when(record2.getDate()).thenReturn(mDate);
        when(record2.getTransactionType()).thenReturn(ProductStockRecord.TransactionType.TRANSACTION_OUT);

        List<ProductStockRecord> productStockRecordList = new ArrayList<ProductStockRecord>();
        productStockRecordList.add(record1);
        productStockRecordList.add(record2);

        when(mRealmDataManager.getProductStockRecordsByProductSKU(p.getSKU())).thenReturn(productStockRecordList);

        Map<String,Integer> map = new LinkedHashMap<>();


        Date dateCursor = productStockRecordList.get(0).getDate();
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(dateCursor);
        String month = calendar.getDisplayName(Calendar.MONTH, Calendar.SHORT, Locale.getDefault());
        int year = calendar.get(Calendar.YEAR) % 2000;
        String monthYearCursor = month + " " + year;

        map.put(monthYearCursor,productStockRecordList.get(0).getStocks());

        dateCursor = productStockRecordList.get(1).getDate();
        calendar = Calendar.getInstance();
        calendar.setTime(dateCursor);
        month = calendar.getDisplayName(Calendar.MONTH, Calendar.SHORT, Locale.getDefault());
        year = calendar.get(Calendar.YEAR) % 2000;
        monthYearCursor = month + " " + year;
        map.put(monthYearCursor,productStockRecordList.get(1).getStocks());

        assertTrue(presenter.getMonthlyStock(p.getSKU()).equals(map));
    }
}
