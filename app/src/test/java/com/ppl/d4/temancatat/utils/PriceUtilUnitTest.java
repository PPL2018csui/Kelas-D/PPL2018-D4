package com.ppl.d4.temancatat.utils;

import org.junit.Test;
import org.junit.Before;
import static junit.framework.Assert.assertTrue;

public class PriceUtilUnitTest {
    // TODO: Standardized Error Message
    private static String INVALID_PRICE_ERROR = "Error";
    private static String CURRENCY = "Rp";
    private static String US_FORMAT = "US";
    private static String ID_FORMAT = "ID";
    private static String INVALID_TEST_FIELD = "Hahahehe";

    private Double price;
    private Double priceDecimal;
    private Double priceNegative;

    private String expectedPriceUS;
    private String expectedPriceID;
    private String expectedPriceDecimalUS;
    private String expectedPriceDecimalID;

    private String expectedPriceRpUS;
    private String expectedPriceRpDecimalUS;
    private String expectedPriceNegativeRpUS;
    private String expectedPriceRpID;
    private String expectedPriceRpDecimalID;
    private String expectedPriceNegativeRpID;

    @Before
    public void setup() {
        price = 250123400d;
        priceDecimal = 123000.456;
        priceNegative = -123500d;

        expectedPriceUS = "250,123,400.00";
        expectedPriceRpUS = "Rp250,123,400.00";
        expectedPriceID = "250.123.400,00";
        expectedPriceRpID = "Rp250.123.400,00";
        expectedPriceDecimalUS = "123,000.46";
        expectedPriceRpDecimalUS = "Rp123,000.46";
        expectedPriceDecimalID = "123.000,46";
        expectedPriceRpDecimalID = "Rp123.000,46";
        expectedPriceNegativeRpID = "-Rp123.500,00";
        expectedPriceNegativeRpUS = "-Rp123,500.00";
    }

    @Test
    public void testValidPriceFormatUS() {
        assertTrue(
                PriceUtil.convertPriceFormatUS(price).equals(expectedPriceUS)
        );
    }

    @Test
    public void testValidPriceDecimalFormatUS() {
        assertTrue(
                PriceUtil.convertPriceFormatUS(priceDecimal).equals(expectedPriceDecimalUS)
        );
    }

    @Test
    public void testInvalidPriceFormatUS() {
        assertTrue(
                PriceUtil.convertPriceFormatUS(priceNegative).equals(INVALID_PRICE_ERROR)
        );
    }

    @Test
    public void testValidPriceFormatID() {
        assertTrue(
                PriceUtil.convertPriceFormatID(price).equals(expectedPriceID)
        );
    }

    @Test
    public void testValidPriceDecimalFormatID() {
        assertTrue(
                PriceUtil.convertPriceFormatID(priceDecimal).equals(expectedPriceDecimalID)
        );
    }

    @Test
    public void testInvalidPriceFormatID() {
        assertTrue(
                PriceUtil.convertPriceFormatID(priceNegative).equals(INVALID_PRICE_ERROR)
        );
    }

    @Test
    public void testRupiahID() {
        assertTrue(
                PriceUtil.convertPriceFormatWithCurrency(
                        price,
                        CURRENCY,
                        ID_FORMAT).equals(expectedPriceRpID)
        );
    }

    @Test
    public void testRupiahUS() {
        assertTrue(
                PriceUtil.convertPriceFormatWithCurrency(
                        price,
                        CURRENCY,
                        US_FORMAT).equals(expectedPriceRpUS)
        );
    }

    @Test
    public void testRupiahInvalid() {
        assertTrue(
                PriceUtil.convertPriceFormatWithCurrency(
                        price,
                        CURRENCY,
                        INVALID_TEST_FIELD).equals(INVALID_PRICE_ERROR)
        );
    }

    @Test
    public void testRupiahDecimalID() {
        assertTrue(
                PriceUtil.convertPriceFormatWithCurrency(
                        priceDecimal,
                        CURRENCY,
                        ID_FORMAT).equals(expectedPriceRpDecimalID)
        );
    }

    @Test
    public void testRupiahDecimalUS() {
        assertTrue(
                PriceUtil.convertPriceFormatWithCurrency(
                        priceDecimal,
                        CURRENCY,
                        US_FORMAT).equals(expectedPriceRpDecimalUS)
        );
    }

    @Test
    public void testRupiahDecimalInvalid() {
        assertTrue(
                PriceUtil.convertPriceFormatWithCurrency(
                        priceDecimal,
                        CURRENCY,
                        INVALID_TEST_FIELD).equals(INVALID_PRICE_ERROR)
        );
    }

    @Test
    public void testRupiahNegativeID() {
        assertTrue(
                PriceUtil.convertPriceFormatWithCurrency(
                        priceNegative,
                        CURRENCY,
                        ID_FORMAT).equals(expectedPriceNegativeRpID)
        );
    }

    @Test
    public void testRupiahNegativeUS() {
        assertTrue(
                PriceUtil.convertPriceFormatWithCurrency(
                        priceNegative,
                        CURRENCY,
                        US_FORMAT).equals(expectedPriceNegativeRpUS)
        );
    }

    @Test
    public void testRupiahNegativeInvalid() {
        assertTrue(
                PriceUtil.convertPriceFormatWithCurrency(
                        priceNegative,
                        CURRENCY,
                        INVALID_TEST_FIELD).equals(INVALID_PRICE_ERROR)
        );
    }

    @Test
    public void testPriceUtil() {
        PriceUtil priceUtil = new PriceUtil();
        assertTrue(priceUtil.convertPriceFormatUS(price).equals(
                PriceUtil.convertPriceFormatUS(price)
        ));
    }
}