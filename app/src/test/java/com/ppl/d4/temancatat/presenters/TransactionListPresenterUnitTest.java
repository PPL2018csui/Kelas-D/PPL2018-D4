package com.ppl.d4.temancatat.presenters;

import com.ppl.d4.temancatat.models.Product;
import com.ppl.d4.temancatat.models.ProductStockRecord;
import com.ppl.d4.temancatat.models.StockUnit;
import com.ppl.d4.temancatat.models.TransactionListItem;
import com.ppl.d4.temancatat.models.manager.RealmDataManager;
import com.ppl.d4.temancatat.presenters.implementations.TransactionListPresenter;
import com.ppl.d4.temancatat.ui.views.TransactionListView;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static junit.framework.Assert.assertFalse;
import static junit.framework.Assert.assertTrue;
import static org.junit.Assert.assertEquals;
import static org.mockito.ArgumentMatchers.isA;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class TransactionListPresenterUnitTest {
    @Mock
    RealmDataManager mRealmDataManager;

    @Mock
    TransactionListView mView;

    private TransactionListPresenter presenter;

    @Before
    public void setup() throws Exception{
        MockitoAnnotations.initMocks(this);

        presenter = new TransactionListPresenter(mRealmDataManager);
        presenter.onAttach(mView);
    }

    @Test
    public void testOnRealmSuccess() {
        presenter.onRealmSuccess();
    }

    @Test
    public void testOnRealmError() {
        presenter.onRealmError(new Throwable());
    }

    @Test
    public void testOnRealmClose() {
        presenter.closeRealm();
        verify(mRealmDataManager, times(1)).closeRealm();
    }

    @Test
    public void testOnAttach() {
        presenter.onAttach(mView);
        assert(presenter.getView()).equals(mView);
    }

    @Test
    public void testOnDetach() {
        presenter.onDetach();
    }

    @Test
    public void testGetView() {
        presenter.onAttach(mView);
        assert(presenter.getView()).equals(mView);
    }
}
