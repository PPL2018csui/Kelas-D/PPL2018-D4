package com.ppl.d4.temancatat.seeders;

import com.ppl.d4.temancatat.models.StockUnit;
import com.ppl.d4.temancatat.models.manager.RealmDataManager;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;

import static junit.framework.Assert.assertFalse;
import static junit.framework.Assert.assertTrue;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

public class DummyStockUnitSeederUnitTest {

    // Instance lower bound for passing randomness test
    public static final int STOCKUNIT_NUMBER = 10;

    private int expectedNameLength;
    private int invalidNameLength;

    private List<StockUnit> stockUnitList;

    private DummyStockUnitSeeder seeder;

    @Mock
    RealmDataManager mRealmDataManager;

    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);

        expectedNameLength = 15;
        invalidNameLength = 10;

        seeder = new DummyStockUnitSeeder();

        stockUnitList = new ArrayList<StockUnit>();
        for (int i = 0; i < STOCKUNIT_NUMBER; i++) {
            StockUnit temp = seeder.generateDummyStockUnit(expectedNameLength);
            stockUnitList.add(temp);
        }
    }

    @Test
    public void testRandomNameValid() {
        String randomName = seeder.generateRandomName(expectedNameLength);
        assertTrue(randomName.length() == expectedNameLength);
    }

    @Test
    public void testRandomNameInvalid() {
        String randomName = seeder.generateRandomName(expectedNameLength);
        assertFalse(randomName.length() == invalidNameLength);
    }

    @Test
    public void testDummyStockUnitValid() {
        HashSet<String> seenNames = new HashSet<String>();

        for(StockUnit currentItem: stockUnitList) {
            assertFalse(seenNames.contains(currentItem.getName()));
            seenNames.add(currentItem.getName());
        }
    }

    @Test
    public void testHandleSeed() {
        seeder.handleSeed(mRealmDataManager);
        verify(mRealmDataManager, times(DummyStockUnitSeeder.SEED_COUNT)).createStockUnit(any());
    }
}