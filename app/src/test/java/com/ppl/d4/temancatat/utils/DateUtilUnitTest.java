package com.ppl.d4.temancatat.utils;


import android.util.Log;

import org.junit.Test;
import org.junit.Before;
import java.util.Calendar;
import java.util.Date;

import static junit.framework.Assert.assertTrue;
import static junit.framework.Assert.assertFalse;

public class DateUtilUnitTest {

    private Calendar calendar;
    private Calendar calendarInvalidHandled;

    private String expectedDMYBahasa;

    private String expectedDMYInvalidHandled;

    @Before
    public void setup() {
        calendar = Calendar.getInstance();
        calendar.set(Calendar.YEAR, 2018);
        calendar.set(Calendar.MONTH, Calendar.APRIL);
        calendar.set(Calendar.DAY_OF_MONTH, 14);

        expectedDMYBahasa = "Sabtu, 14 April 2018";

        calendarInvalidHandled = Calendar.getInstance();
        calendarInvalidHandled.set(Calendar.YEAR, 2018);
        calendarInvalidHandled.set(Calendar.MONTH, Calendar.FEBRUARY);
        calendarInvalidHandled.set(Calendar.DAY_OF_MONTH, 30);

        expectedDMYInvalidHandled = "Jumat, 02 Maret 2018";
    }

    @Test
    public void testObject() {
        assertTrue(new DateUtil() != null);
    }

    @Test
    public void testFormatToDMY() {
        assertTrue(DateUtil.formatToDMY(calendar.getTime()).equals(expectedDMYBahasa));
    }

    @Test
    public void testFormatToDMYInvalidHandled() {
        assertTrue(DateUtil.formatToDMY(calendarInvalidHandled.getTime()).equals(expectedDMYInvalidHandled));
    }
}
