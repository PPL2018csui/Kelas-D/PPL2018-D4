package com.ppl.d4.temancatat.models;

import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;

import static junit.framework.Assert.assertFalse;
import static junit.framework.Assert.assertTrue;

public class StockUnitTest {

    public static final int STOCK_UNIT_NUMBER = 100000;

    private String expectedUnit;
    private String invalidUnit;
    private StockUnit stockUnit;
    private StockUnit customStockUnit;

    private List<StockUnit> stockUnitList;

    @Before
    public void setUp() {
        expectedUnit = "Anab Ganteng";

        invalidUnit = "Kuci Kuci Hotahe";

        stockUnit = new StockUnit(expectedUnit);
        customStockUnit = new StockUnit();
        customStockUnit.setName(expectedUnit);

        stockUnitList = new ArrayList<StockUnit>();
        for (int i = 0; i < STOCK_UNIT_NUMBER; i++) {
            StockUnit stockUnitTemp = new StockUnit(expectedUnit);
            stockUnitList.add(stockUnitTemp);
        }
    }

    @Test
    public void testStockUnitIdNotDuplicate() {
        HashSet<String> hashId = new HashSet<String>();

        for (StockUnit stockUnitItem: stockUnitList) {
            assertFalse(hashId.contains(stockUnitItem.getId()));

            hashId.add(stockUnitItem.getId());
        }
    }

    @Test
    public void testUnitValid() {
        assertTrue(stockUnit.getName().equals(expectedUnit));
    }

    @Test
    public void testUnitInvalid() {
        assertFalse(stockUnit.getName().equals(invalidUnit));
    }


    @Test
    public void testSetUnitValid() {
        assertTrue(customStockUnit.getName().equals(expectedUnit));
    }

    @Test
    public void testSetUnitInvalid() {
        assertFalse(customStockUnit.getName().equals(invalidUnit));
    }
}
