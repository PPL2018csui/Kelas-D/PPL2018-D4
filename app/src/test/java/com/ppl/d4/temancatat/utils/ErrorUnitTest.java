package com.ppl.d4.temancatat.utils;

import org.junit.Before;
import org.junit.Test;

import static junit.framework.Assert.assertTrue;
import static junit.framework.TestCase.assertFalse;

public class ErrorUnitTest {
    private int expectedFailedDataInsertionErrorCode;
    private int expectedFailedDataRetrievalErrorCode;
    private int expectedFailedDataUpdateErrorCode;
    private int expectedFailedDataDeletionErrorCode;
    private int expectedUserInvalidInputErrorCode;
    private String expectedFailedDataInsertionErrorMessage;
    private String expectedFailedDataRetrievalErrorMessage;
    private String expectedFailedDataUpdateErrorMessage;
    private String expectedFailedDataDeletionErrorMessage;
    private String expectedUserInvalidInputErrorMessage;
    private int invalidErrorCode;
    private String invalidErrorMessage;

    @Before
    public void setUp() {
        expectedFailedDataInsertionErrorCode = 1001;
        expectedFailedDataRetrievalErrorCode = 1002;
        expectedFailedDataUpdateErrorCode = 1003;
        expectedFailedDataDeletionErrorCode = 1004;
        expectedUserInvalidInputErrorCode = 1101;

        expectedFailedDataInsertionErrorMessage = "Terdapat masalah dalam penambahan data.";
        expectedFailedDataRetrievalErrorMessage = "Terdapat masalah dalam pengambilan data.";
        expectedFailedDataUpdateErrorMessage = "Terdapat masalah dalam pengubahan data.";
        expectedFailedDataDeletionErrorMessage = "Terdapat masalah dalam penghapusan data.";
        expectedUserInvalidInputErrorMessage = "Terdapat kesalahan input.";

        invalidErrorCode = -1;
        invalidErrorMessage = "Ga ada error.";
    }

    @Test
    public void getFailedDataInsertionErrorCodeValid(){
        assertTrue(Error.FAILED_DATA_INSERTION.getErrorCode() == expectedFailedDataInsertionErrorCode);
    }

    @Test
    public void getFailedDataInsertionErrorCodeInvalid(){
        assertFalse(Error.FAILED_DATA_INSERTION.getErrorCode() == invalidErrorCode);
    }

    @Test
    public void getFailedDataInsertionErrorMessageValid(){
        assertTrue((Error.FAILED_DATA_INSERTION.getErrorMessage()).equals(expectedFailedDataInsertionErrorMessage));
    }

    @Test
    public void getFailedDataInsertionErrorMessageInvalid(){
        assertFalse((Error.FAILED_DATA_INSERTION.getErrorMessage()).equals(invalidErrorMessage));
    }

    @Test
    public void getFailedDataRetrievalErrorCodeValid(){
        assertTrue(Error.FAILED_DATA_RETRIEVAL.getErrorCode() == expectedFailedDataRetrievalErrorCode);
    }

    @Test
    public void getFailedDataRetrievalErrorCodeInvalid(){
        assertFalse(Error.FAILED_DATA_RETRIEVAL.getErrorCode() == invalidErrorCode);
    }

    @Test
    public void getFailedDataRetrievalErrorMessageValid(){
        assertTrue((Error.FAILED_DATA_RETRIEVAL.getErrorMessage()).equals(expectedFailedDataRetrievalErrorMessage));
    }

    @Test
    public void getFailedDataRetrievalErrorMessageInvalid(){
        assertFalse((Error.FAILED_DATA_RETRIEVAL.getErrorMessage()).equals(invalidErrorMessage));
    }

    @Test
    public void getFailedDataUpdateErrorCodeValid(){
        assertTrue(Error.FAILED_DATA_UPDATE.getErrorCode() == expectedFailedDataUpdateErrorCode);
    }

    @Test
    public void getFailedDataUpdateErrorCodeInvalid(){
        assertFalse(Error.FAILED_DATA_UPDATE.getErrorCode() == invalidErrorCode);
    }

    @Test
    public void getFailedDataUpdateErrorMessageValid(){
        assertTrue((Error.FAILED_DATA_UPDATE.getErrorMessage()).equals(expectedFailedDataUpdateErrorMessage));
    }

    @Test
    public void getFailedDataUpdateErrorMessageInvalid(){
        assertFalse((Error.FAILED_DATA_UPDATE.getErrorMessage()).equals(invalidErrorMessage));
    }

    @Test
    public void getFailedDataDeletionErrorCodeValid(){
        assertTrue(Error.FAILED_DATA_DELETION.getErrorCode() == expectedFailedDataDeletionErrorCode);
    }

    @Test
    public void getFailedDataDeletionErrorCodeInvalid(){
        assertFalse(Error.FAILED_DATA_DELETION.getErrorCode() == invalidErrorCode);
    }

    @Test
    public void getFailedDataDeletionErrorMessageValid(){
        assertTrue((Error.FAILED_DATA_DELETION.getErrorMessage()).equals(expectedFailedDataDeletionErrorMessage));
    }

    @Test
    public void getFailedDataDeletionErrorMessageInvalid(){
        assertFalse((Error.FAILED_DATA_DELETION.getErrorMessage()).equals(invalidErrorMessage));
    }


    @Test
    public void getUserInvalidInputErrorCodeValid(){
        assertTrue(Error.USER_INVALID_INPUT.getErrorCode() == expectedUserInvalidInputErrorCode);
    }

    @Test
    public void getUserInvalidInputErrorCodeInvalid(){
        assertFalse(Error.USER_INVALID_INPUT.getErrorCode() == invalidErrorCode);
    }

    @Test
    public void getUserInvalidInputErrorMessageValid(){
        assertTrue((Error.USER_INVALID_INPUT.getErrorMessage()).equals(expectedUserInvalidInputErrorMessage));
    }

    @Test
    public void getUserInvalidInputErrorMessageInvalid(){
        assertFalse((Error.USER_INVALID_INPUT.getErrorMessage()).equals(invalidErrorMessage));
    }
}
