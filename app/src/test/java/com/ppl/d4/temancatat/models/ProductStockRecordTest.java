package com.ppl.d4.temancatat.models;

import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.UUID;

import static com.ppl.d4.temancatat.models.TransactionListItem.TYPE_TRANSACTION;
import static junit.framework.Assert.assertFalse;
import static junit.framework.Assert.assertTrue;

public class ProductStockRecordTest {
    public static final int PRODUCT_STOCK_RECORD_NUMBER = 100000;
    private Product validProduct;
    private Product validProduct2;
    private Product invalidProduct;
    private Date validDate;
    private Date validDate2;
    private Date invalidDate;
    private ProductStockRecord.TransactionType validType;
    private ProductStockRecord.TransactionType validType2;
    private ProductStockRecord.TransactionType invalidType;
    private int validStock;
    private int validStock2;
    private int invalidStock;
    private double validPrice;
    private double validPrice2;
    private double invalidPrice;
    private List<ProductStockRecord> productStockRecordList;
    private ProductStockRecord productStockRecord;

    @Before
    public void setUp() {
        validProduct = new Product();
        validProduct2 = new Product();
        invalidProduct = null;
        validDate = new Date();
        validDate2 = new Date(0);
        invalidDate = null;
        validType = ProductStockRecord.TransactionType.TRANSACTION_IN;
        validType2 = ProductStockRecord.TransactionType.TRANSACTION_OUT;
        invalidType = null;
        validStock = 10;
        validStock2 = 20;
        invalidStock = 0;
        validPrice = 10.0;
        validPrice2 = 100.0;
        invalidPrice = -1.0;
        productStockRecord = new ProductStockRecord(validProduct, validDate, validType, validStock, validPrice);

        productStockRecordList = new ArrayList<ProductStockRecord>();
        for (int i = 0; i < PRODUCT_STOCK_RECORD_NUMBER; i++) {
            ProductStockRecord tempProductStockRecord = new ProductStockRecord();
            productStockRecordList.add(tempProductStockRecord);
        }
    }

    @Test
    public void testValidProductConstructor() {
        assertTrue(productStockRecord.getProduct().equals(validProduct));
    }

    @Test(expected = IllegalArgumentException.class)
    public void testInvalidProductConstructor() {
        new ProductStockRecord(invalidProduct, validDate, validType, validStock, validPrice);
    }

    @Test
    public void testValidDateConstructor() {
        assertTrue(productStockRecord.getDate().equals(validDate));
    }

    @Test(expected = IllegalArgumentException.class)
    public void testInvalidDateConstructor() {
        new ProductStockRecord(validProduct, invalidDate, validType, validStock, validPrice);
    }

    @Test
    public void testValidTypeConstructor() {
        assertTrue(productStockRecord.getTransactionType().equals(validType));
    }

    @Test(expected = IllegalArgumentException.class)
    public void testInvalidTypeConstructor() {
        new ProductStockRecord(validProduct, validDate, invalidType, validStock, validPrice);
    }

    @Test
    public void testValidStockConstructor() {
        assertTrue(productStockRecord.getStocks() == validStock);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testInvalidStockConstructor() {
        new ProductStockRecord(validProduct, validDate, validType, invalidStock, validPrice);
    }

    @Test
    public void testValidPriceConstructor() {
        assertTrue(productStockRecord.getPrice() == validPrice);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testInvalidPriceConstructor() {
        new ProductStockRecord(validProduct, validDate, validType, validStock, invalidPrice);
    }

    @Test
    public void testUUIDConstructor() {
        UUID.fromString(productStockRecord.getId());
    }

    @Test
    public void testProductStockRecordIdNotDuplicate() {
        HashSet<String> hashId = new HashSet<String>();

        for(ProductStockRecord productStockRecordItem: productStockRecordList) {
            assertFalse(hashId.contains(productStockRecordItem.getId()));

            hashId.add(productStockRecordItem.getId());
        }

    }

    @Test
    public void testProductStockRecordViewType() {
        assertTrue(productStockRecord.getType() == TYPE_TRANSACTION);
    }

    @Test
    public void testValidProductSetter() {
        productStockRecord.setProduct(validProduct2);
        assertFalse(productStockRecord.getProduct().equals(validProduct));
        assertTrue(productStockRecord.getProduct().equals(validProduct2));
    }

    @Test(expected = IllegalArgumentException.class)
    public void testInvalidProductSetter() {
        productStockRecord.setProduct(invalidProduct);
    }

    @Test
    public void testValidDateSetter() {
        productStockRecord.setDate(validDate2);
        assertFalse(productStockRecord.getDate().equals(validDate));
        assertTrue(productStockRecord.getDate().equals(validDate2));
    }

    @Test(expected = IllegalArgumentException.class)
    public void testInvalidDateSetter() {
        productStockRecord.setDate(invalidDate);
    }

    @Test
    public void testValidTransactionTypeSetter() {
        productStockRecord.setTransactionType(validType2);
        assertFalse(productStockRecord.getTransactionType().equals(validType));
        assertTrue(productStockRecord.getTransactionType().equals(validType2));
    }

    @Test(expected = IllegalArgumentException.class)
    public void testInvalidTypeSetter() {
        productStockRecord.setTransactionType(invalidType);
    }

    @Test
    public void testValidStockSetter() {
        productStockRecord.setStocks(validStock2);
        assertFalse(productStockRecord.getStocks() == validStock);
        assertTrue(productStockRecord.getStocks() == validStock2);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testInvalidStockSetter() {
        productStockRecord.setStocks(invalidStock);
    }

    @Test
    public void testValidPriceSetter() {
        productStockRecord.setPrice(validPrice2);
        assertFalse(productStockRecord.getPrice() == validPrice);
        assertTrue(productStockRecord.getPrice() == validPrice2);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testInvalidPriceSetter() {
        productStockRecord.setPrice(invalidPrice);
    }

}
