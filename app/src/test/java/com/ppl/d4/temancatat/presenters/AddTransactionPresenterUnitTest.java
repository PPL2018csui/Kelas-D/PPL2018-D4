package com.ppl.d4.temancatat.presenters;
import com.ppl.d4.temancatat.models.Product;
import com.ppl.d4.temancatat.models.ProductStockRecord;
import com.ppl.d4.temancatat.models.StockUnit;
import com.ppl.d4.temancatat.models.manager.RealmDataManager;
import com.ppl.d4.temancatat.presenters.implementations.AddTransactionPresenter;
import com.ppl.d4.temancatat.ui.views.AddTransactionView;
import com.ppl.d4.temancatat.utils.AppException;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.Date;
import java.util.List;
import java.util.ArrayList;

import static junit.framework.Assert.assertFalse;
import static junit.framework.Assert.assertTrue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class AddTransactionPresenterUnitTest {
    private final double EPS = 1e-10;

    @Mock
    private RealmDataManager mRealmDataManager;

    @Mock
    private AddTransactionView addTransactionView;

    @Mock
    private Date mDate;

    private AddTransactionPresenter addTransactionPresenter;

    private Integer validButLargeStock;
    private Integer validStock;
    private Integer lessThanOneStock;
    private Integer nullStock;
    private Double validPrice;
    private Double lessThanZeroPrice;
    private Double nullPrice;

    private Product validProduct;
    private String validProductSKU;
    private String nullProductId;

    private Throwable throwable;

    private List<ProductStockRecord> records;

    private Integer expectedCurrentStock;
    private Integer editTransactionInputStocks;
    private ProductStockRecord record;

    private double validRecommendationPrice;
    private double invalidRecommendationPrice;

    @Before
    public void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);

        addTransactionPresenter = new AddTransactionPresenter(mRealmDataManager);
        addTransactionPresenter.onAttach(addTransactionView);

        validButLargeStock = 100;
        validStock = 1;
        lessThanOneStock = 0;
        nullStock = null;
        validPrice = 1.0;
        lessThanZeroPrice = -1.0;
        nullPrice = null;
        validRecommendationPrice = validPrice;
        invalidRecommendationPrice = validPrice + 1.0;

        throwable = new Throwable();

        StockUnit stockUnit = new StockUnit("GB");

        validProduct = new Product("teh tabung", "teh dalam kemasan kotak", stockUnit, "this is a url");
        validProductSKU = validProduct.getSKU();
        nullProductId = null;

        expectedCurrentStock = 5;

        when(mRealmDataManager.getCurrentProductStockByProductSKU(validProductSKU)).thenReturn(expectedCurrentStock);
        when(mRealmDataManager.getProductBySKU(validProductSKU)).thenReturn(validProduct);

        record = mock(ProductStockRecord.class);
        when(record.getId()).thenReturn("SomeId");
        when(record.getStocks()).thenReturn(1);
        when(record.getPrice()).thenReturn(validPrice);

        records =  new ArrayList<ProductStockRecord>();
        records.add(new ProductStockRecord(validProduct, mDate, ProductStockRecord.TransactionType.TRANSACTION_IN, 1, validPrice));
        records.add(record);
        records.add(new ProductStockRecord(validProduct, mDate, ProductStockRecord.TransactionType.TRANSACTION_IN, 4, validPrice));
        records.add(new ProductStockRecord(validProduct, mDate, ProductStockRecord.TransactionType.TRANSACTION_OUT, 3, validPrice));

        when(mRealmDataManager.getProductStockRecordById(any())).thenReturn(record);
        when(mRealmDataManager.getProductStockRecordsByProductSKU(any())).thenReturn(records);
    }

    @Test
    public void testOnRealmSuccess() {
        addTransactionPresenter.onRealmSuccess();
        verify(addTransactionView, times(1)).onFinishLoading();
        verify(addTransactionView, times(1)).finish();
    }

    @Test
    public void testOnRealmError() {
        addTransactionPresenter.onRealmError(throwable);
        verify(addTransactionView, times(1)).showErrorMessage(throwable.toString());
    } //assert bahwa showErrorMessage dipanggil sekali

    @Test
    public void testOnRealmClose() {
        addTransactionPresenter.closeRealm();
        verify(mRealmDataManager, times(1)).closeRealm();
    }

    @Test
    public void testOnAttach() {
        addTransactionPresenter.onAttach(addTransactionView);
        assert(addTransactionPresenter.getView().equals(addTransactionView));
    }

    @Test
    public void testOnDetach() {
        addTransactionPresenter.onDetach();
    }

    @Test
    public void testGetView() {
        addTransactionPresenter.onAttach(addTransactionView);
        assert(addTransactionPresenter.getView().equals(addTransactionView));
    }

    @Test(expected = AppException.class)
    public void testOnSaveTransactionNullArguments() throws AppException {
        addTransactionPresenter.onSaveTransaction(
                nullProductId,
                mDate,
                ProductStockRecord.TransactionType.TRANSACTION_IN,
                nullStock,
                nullPrice);
        verify(addTransactionView, times(1)).setInvalidProductError();
        verify(addTransactionView, times(1)).setInvalidAmountError();
        verify(addTransactionView, times(1)).setInvalidPriceError();
    }

    @Test(expected = AppException.class)
    public void testOnSaveTransactionLessThanExpectedArguments() throws AppException {
        addTransactionPresenter.onSaveTransaction(
                validProductSKU,
                mDate,
                ProductStockRecord.TransactionType.TRANSACTION_IN,
                lessThanOneStock,
                lessThanZeroPrice);
        verify(addTransactionView, times(1)).setInvalidAmountError();
        verify(addTransactionView, times(1)).setInvalidPriceError();
    }

    @Test(expected = AppException.class)
    public void testOnSaveTransactionTooManyStocksOut() throws AppException {
        addTransactionPresenter.changeMode(addTransactionPresenter.CREATE_MODE);
        addTransactionPresenter.onSaveTransaction(
                validProductSKU,
                mDate,
                ProductStockRecord.TransactionType.TRANSACTION_OUT,
                validButLargeStock,
                validPrice);
        verify(addTransactionView, times(1)).setNotEnoughStockError(expectedCurrentStock);
    }

    @Test
    public void testOnSaveTransactionOutSuccess() throws AppException {
        addTransactionPresenter.changeMode(addTransactionPresenter.CREATE_MODE);
        addTransactionPresenter.onSaveTransaction(
                validProductSKU,
                mDate,
                ProductStockRecord.TransactionType.TRANSACTION_OUT,
                validStock,
                validPrice);

        verify(addTransactionView, times(1)).onLoading();
    }

    @Test
    public void testOnSaveTransactionInSuccess() throws AppException {
        addTransactionPresenter.changeMode(addTransactionPresenter.CREATE_MODE);
        addTransactionPresenter.onSaveTransaction(
                validProductSKU,
                mDate,
                ProductStockRecord.TransactionType.TRANSACTION_IN,
                validStock,
                validPrice);

        verify(addTransactionView, times(1)).onLoading();
    }

    @Test
    public void testOnSaveTransactionEditSuccess() throws AppException {
        int stocksLeft = 5;
        int recordStocks = 1;

        ProductStockRecord record = mock(ProductStockRecord.class);
        when(record.getId()).thenReturn("SomeId");
        when(record.getStocks()).thenReturn(recordStocks);
        when(record.getProduct()).thenReturn(validProduct);
        when(record.getTransactionType()).thenReturn(ProductStockRecord.TransactionType.TRANSACTION_IN);
        when(mRealmDataManager.getProductStockRecordById(any())).thenReturn(record);
        when(mRealmDataManager.getCurrentProductStockByProductSKU(any())).thenReturn(stocksLeft);

        addTransactionPresenter.setTransactionById(record.getId());

        addTransactionPresenter.changeMode(addTransactionPresenter.EDIT_MODE);
        addTransactionPresenter.onSaveTransaction(
                validProductSKU,
                mDate,
                ProductStockRecord.TransactionType.TRANSACTION_IN,
                validStock,
                validPrice);

        verify(mRealmDataManager, times(1)).createOrUpdateProductStockRecord(record, addTransactionPresenter);
    }

    @Test (expected = AppException.class)
    public void testOnSaveTransactionEditNotEnoughStock() throws AppException {
        int stocksLeft = 0;
        int recordStocks = 5;
        int nextStocks = 7;

        ProductStockRecord record = mock(ProductStockRecord.class);
        when(record.getId()).thenReturn("SomeId");
        when(record.getStocks()).thenReturn(recordStocks);
        when(record.getProduct()).thenReturn(validProduct);
        when(record.getTransactionType()).thenReturn(ProductStockRecord.TransactionType.TRANSACTION_OUT);
        when(mRealmDataManager.getProductStockRecordById(any())).thenReturn(record);
        when(mRealmDataManager.getCurrentProductStockByProductSKU(any())).thenReturn(stocksLeft);

        addTransactionPresenter.setTransactionById(record.getId());

        addTransactionPresenter.changeMode(addTransactionPresenter.EDIT_MODE);
        addTransactionPresenter.onSaveTransaction(
                validProductSKU,
                mDate,
                ProductStockRecord.TransactionType.TRANSACTION_OUT,
                nextStocks,
                validPrice);

        verify(mRealmDataManager, times(1)).createOrUpdateProductStockRecord(record, addTransactionPresenter);
    }

    @Test
    public void testSetTransactionByIdNull() throws AppException {
        addTransactionPresenter.setTransactionById(null);
        verify(mRealmDataManager, times(0)).getProductStockRecordById(any());
    }

    @Test(expected = AppException.class)
    public void testSetTransactionRealmError() throws AppException {
        when(mRealmDataManager.getProductStockRecordById(any())).thenReturn(null);
        addTransactionPresenter.setTransactionById("Random");
    }

    @Test
    public void testSetTransactionByIdDuplicate() throws AppException {
        ProductStockRecord record = mock(ProductStockRecord.class);
        when(record.getId()).thenReturn("SameId");
        when(mRealmDataManager.getProductStockRecordById(any())).thenReturn(record);
        addTransactionPresenter.setTransactionById(record.getId());
        addTransactionPresenter.setTransactionById(record.getId());
        verify(mRealmDataManager, times(1)).getProductStockRecordById(any());
    }

    @Test
    public void testSetTransactionByIdSuccess() throws AppException {
        ProductStockRecord record = mock(ProductStockRecord.class);
        when(record.getId()).thenReturn("SameId");
        when(mRealmDataManager.getProductStockRecordById(any())).thenReturn(record);
        addTransactionPresenter.setTransactionById("newId");
        addTransactionPresenter.setTransactionById("newId");
        verify(mRealmDataManager, times(2)).getProductStockRecordById(any());
    }

    @Test
    public void testGetTransactionValid() throws AppException {
        ProductStockRecord record = mock(ProductStockRecord.class);
        when(record.getId()).thenReturn("SameId");
        when(mRealmDataManager.getProductStockRecordById(any())).thenReturn(record);
        addTransactionPresenter.setTransactionById(record.getId());
        assert(addTransactionPresenter.getTransaction() == record);
    }

    @Test
    public void deleteInTransactionValid() throws AppException  {
        int stocksLeft = 5;
        int recordStocks = 1;

        ProductStockRecord record = mock(ProductStockRecord.class);
        when(record.getId()).thenReturn("SomeId");
        when(record.getStocks()).thenReturn(recordStocks);
        when(record.getProduct()).thenReturn(validProduct);
        when(record.getTransactionType()).thenReturn(ProductStockRecord.TransactionType.TRANSACTION_IN);
        when(mRealmDataManager.getProductStockRecordById(any())).thenReturn(record);
        when(mRealmDataManager.getCurrentProductStockByProductSKU(any())).thenReturn(stocksLeft);

        addTransactionPresenter.setTransactionById(record.getId());
        addTransactionPresenter.deleteTransaction();
        verify(mRealmDataManager, times(1)).syncDeleteProductStockRecordById(record.getId());
    }

    @Test
    public void deleteOutTransactionValid() throws AppException  {
        ProductStockRecord record = mock(ProductStockRecord.class);
        when(record.getId()).thenReturn("SomeId");
        when(record.getProduct()).thenReturn(validProduct);
        when(record.getTransactionType()).thenReturn(ProductStockRecord.TransactionType.TRANSACTION_OUT);
        when(mRealmDataManager.getProductStockRecordById(any())).thenReturn(record);

        addTransactionPresenter.setTransactionById(record.getId());
        addTransactionPresenter.deleteTransaction();
        verify(mRealmDataManager, times(1)).syncDeleteProductStockRecordById(record.getId());
    }

    @Test(expected = NullPointerException.class)
    public void deleteTransactionNullInvalid() throws AppException  {
        addTransactionPresenter.deleteTransaction();
    }

    @Test (expected = AppException.class)
    public void deleteInTransactionNotEnoughStock() throws AppException  {
        when(record.getProduct()).thenReturn(validProduct);
        when(record.getTransactionType()).thenReturn(ProductStockRecord.TransactionType.TRANSACTION_IN);
        when(mRealmDataManager.getProductStockRecordById(any())).thenReturn(record);

        records.add(new ProductStockRecord(validProduct, mDate, ProductStockRecord.TransactionType.TRANSACTION_OUT, 3, validPrice));

        addTransactionPresenter.setTransactionById(record.getId());
        addTransactionPresenter.deleteTransaction();
    }

    @Test
    public void testChangeModeInvalidArgument() throws AppException {
        addTransactionPresenter.changeMode("Invalid Mode");
        verify(addTransactionView, times(0)).onAddModeChanged();
        verify(addTransactionView, times(0)).onViewModeChanged();
        verify(addTransactionView, times(0)).onEditModeChanged();
    }

    @Test
    public void testChangeModeNullArgument() throws AppException {
        addTransactionPresenter.changeMode(null);
        assert(addTransactionPresenter.getMode().equals(addTransactionPresenter.CREATE_MODE));
    }

    @Test
    public void testChangeModeSameTwiceArgument() throws AppException {
        addTransactionPresenter.changeMode(addTransactionPresenter.CREATE_MODE);
        addTransactionPresenter.changeMode(addTransactionPresenter.CREATE_MODE);
        verify(addTransactionView, times(1)).onAddModeChanged();
    }

    @Test
    public void testChangeModeCreateValid() throws AppException {
        addTransactionPresenter.changeMode(addTransactionPresenter.CREATE_MODE);
        verify(addTransactionView, times(1)).onAddModeChanged();
    }

    @Test
    public void testChangeModeViewValid() throws AppException {
        addTransactionPresenter.changeMode(addTransactionPresenter.VIEW_MODE);
        verify(addTransactionView, times(1)).onViewModeChanged();
    }

    @Test
    public void testChangeModeEditValid() throws AppException {
        addTransactionPresenter.changeMode(addTransactionPresenter.EDIT_MODE);
        verify(addTransactionView, times(1)).onEditModeChanged();
    }

    @Test
    public void testToggleViewMode() throws AppException {
        addTransactionPresenter.changeMode(addTransactionPresenter.VIEW_MODE);
        addTransactionPresenter.toggleEdit();
        assert(addTransactionPresenter.getMode().equals(addTransactionPresenter.EDIT_MODE));
    }

    @Test
    public void testToggleEditMode() throws AppException {
        addTransactionPresenter.changeMode(addTransactionPresenter.EDIT_MODE);
        addTransactionPresenter.toggleEdit();
        assert(addTransactionPresenter.getMode().equals(addTransactionPresenter.VIEW_MODE));
    }

    @Test
    public void testToggleCreateMode() throws AppException {
        addTransactionPresenter.changeMode(addTransactionPresenter.CREATE_MODE);
        addTransactionPresenter.toggleEdit();
        assert(addTransactionPresenter.getMode().equals(addTransactionPresenter.CREATE_MODE));
    }

    @Test
    public void testOnSaveTransactionEditSuccessChangeStock() throws AppException {
        editTransactionInputStocks = 1;

        addTransactionPresenter.setTransactionById(record.getId());

        addTransactionPresenter.changeMode(addTransactionPresenter.EDIT_MODE);
        addTransactionPresenter.onSaveTransaction(
                validProductSKU,
                mDate,
                ProductStockRecord.TransactionType.TRANSACTION_OUT,
                editTransactionInputStocks,
                validPrice);

        verify(mRealmDataManager, times(1)).createOrUpdateProductStockRecord(record, addTransactionPresenter);
    }

    @Test
    public void testOnSaveTransactionEditSuccessChangeTransactionType() throws AppException {
        editTransactionInputStocks = 1;
        when(record.getTransactionType()).thenReturn(ProductStockRecord.TransactionType.TRANSACTION_OUT);

        addTransactionPresenter.setTransactionById(record.getId());

        addTransactionPresenter.changeMode(addTransactionPresenter.EDIT_MODE);
        addTransactionPresenter.onSaveTransaction(
                validProductSKU,
                mDate,
                ProductStockRecord.TransactionType.TRANSACTION_IN,
                editTransactionInputStocks,
                validPrice);

        verify(mRealmDataManager, times(1)).createOrUpdateProductStockRecord(record, addTransactionPresenter);
    }

    @Test (expected = AppException.class)
    public void testOnSaveTransactionEditFailedChangeStock() throws AppException {
        editTransactionInputStocks = 2;
        when(record.getTransactionType()).thenReturn(ProductStockRecord.TransactionType.TRANSACTION_OUT);

        addTransactionPresenter.setTransactionById(record.getId());
        addTransactionPresenter.changeMode(addTransactionPresenter.EDIT_MODE);
        addTransactionPresenter.onSaveTransaction(
                validProductSKU,
                mDate,
                ProductStockRecord.TransactionType.TRANSACTION_OUT,
                editTransactionInputStocks,
                validPrice);

        verify(mRealmDataManager, times(1)).createOrUpdateProductStockRecord(record, addTransactionPresenter);
    }

    @Test (expected = AppException.class)
    public void testOnSaveTransactionEditFailedChangeTransactionType() throws AppException {
        editTransactionInputStocks = 2;
        when(record.getTransactionType()).thenReturn(ProductStockRecord.TransactionType.TRANSACTION_IN);

        addTransactionPresenter.setTransactionById(record.getId());
        addTransactionPresenter.changeMode(addTransactionPresenter.EDIT_MODE);
        addTransactionPresenter.onSaveTransaction(
                validProductSKU,
                mDate,
                ProductStockRecord.TransactionType.TRANSACTION_OUT,
                editTransactionInputStocks,
                validPrice);

        verify(mRealmDataManager, times(1)).createOrUpdateProductStockRecord(record, addTransactionPresenter);
    }

    @Test
    public void testGetPriceRecommendationEmptyValid()  {
        when(mRealmDataManager.getProductStockRecordsInByProductSKU((any()))).thenReturn(new ArrayList<ProductStockRecord>());
        assertTrue(Math.abs(addTransactionPresenter.getRecommendationPrice(validProductSKU)) <= EPS);
    }

    @Test
    public void testGetPriceRecommendationEmptyInvalid()  {
        when(mRealmDataManager.getProductStockRecordsInByProductSKU((any()))).thenReturn(new ArrayList<ProductStockRecord>());
        assertFalse(Math.abs(addTransactionPresenter.getRecommendationPrice(validProductSKU) - 1) <= EPS);
    }

    @Test
    public void testGetPriceRecommendationNotEmptyValid()  {
        when(mRealmDataManager.getProductStockRecordsInByProductSKU((any()))).thenReturn(records);
        assertTrue(Math.abs(addTransactionPresenter.getRecommendationPrice(validProductSKU) - validRecommendationPrice) <= EPS);
    }

    @Test
    public void testGetPriceRecommendationNotEmptyInvalid()  {
        when(mRealmDataManager.getProductStockRecordsInByProductSKU((any()))).thenReturn(records);
        assertFalse(Math.abs(addTransactionPresenter.getRecommendationPrice(validProductSKU) - invalidRecommendationPrice) <= EPS);
    }

}
