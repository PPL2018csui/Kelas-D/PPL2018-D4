package com.ppl.d4.temancatat.presenters;


import android.content.Context;

import com.github.mikephil.charting.charts.PieChart;
import com.ppl.d4.temancatat.models.Product;
import com.ppl.d4.temancatat.models.ProductStockRecord;
import com.ppl.d4.temancatat.models.StockUnit;
import com.ppl.d4.temancatat.models.manager.RealmDataManager;
import com.ppl.d4.temancatat.presenters.implementations.OverallStatisticsPresenter;
import com.ppl.d4.temancatat.ui.views.OverallStatisticsView;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static junit.framework.Assert.assertFalse;
import static org.hamcrest.CoreMatchers.instanceOf;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class OverallStatisticsPresenterUnitTest {
    public static final int TRANSACTION_NUMBER = 10;

    @Mock
    RealmDataManager mRealmDataManager;

    @Mock
    OverallStatisticsView mView;

    @Mock
    Context mMockContext;

    PieChart pieChart;

    private OverallStatisticsPresenter mPresenter;
    private List<ProductStockRecord> transactionList;
    private List<ProductStockRecord> transactionInList;
    private List<ProductStockRecord> transactionOutList;
    private List<Product> productList;

    @Before
    public void setup() throws Exception{
        MockitoAnnotations.initMocks(this);

        mPresenter = new OverallStatisticsPresenter(mRealmDataManager);
        mPresenter.onAttach(mView);

        pieChart = new PieChart(mMockContext);

        transactionList = new ArrayList<>();
        transactionInList = new ArrayList<>();
        transactionOutList = new ArrayList<>();
        productList = new ArrayList<>();

        for(int i = 0; i < TRANSACTION_NUMBER/2; i++) {
            productList.add(new Product("Dummy Product" + i, "Dummy Product Description" + i, new StockUnit("Dummy StockUnit" + i), "DummySKU" + i));
            ProductStockRecord productStockRecord = new ProductStockRecord(productList.get(i), new Date(), ProductStockRecord.TransactionType.TRANSACTION_IN, 10, 10 );
            transactionList.add(productStockRecord);
            transactionInList.add(productStockRecord);
        }
        for(int i = TRANSACTION_NUMBER/2; i < TRANSACTION_NUMBER; i++) {
            ProductStockRecord productStockRecord = new ProductStockRecord(productList.get(i-TRANSACTION_NUMBER/2), new Date(), ProductStockRecord.TransactionType.TRANSACTION_OUT, 10, 10 );
            transactionList.add(productStockRecord);
            transactionOutList.add(productStockRecord);
        }

        when(mRealmDataManager.getAllProductStockRecords()).thenReturn(transactionList);
        when(mRealmDataManager.getTransactionInProductStockRecords()).thenReturn(transactionInList);
        when(mRealmDataManager.getTransactionOutProductStockRecords()).thenReturn(transactionOutList);
        when(mRealmDataManager.getAllProducts()).thenReturn(productList);
    }

    @After
    public void tearDown() {
        transactionList = null;
    }

    @Test
    public void testOnRealmSuccess() {
        mPresenter.onRealmSuccess();
    }

    @Test
    public void testOnRealmError() {
        mPresenter.onRealmError(new Throwable());
    }

    @Test
    public void testOnRealmClose() {
        mPresenter.closeRealm();
        verify(mRealmDataManager, times(1)).closeRealm();
    }

    @Test
    public void testOnAttach() {
        mPresenter.onAttach(mView);
        assert(mPresenter.getView()).equals(mView);
    }

    @Test
    public void testOnDetach() {
        mPresenter.onDetach();
    }

    @Test
    public void testGetView() {
        mPresenter.onAttach(mView);
        assert(mPresenter.getView()).equals(mView);
    }

    @Test
    public void testGetTotalPurchasesAmount() {
        assertThat(mPresenter.getTotalPurchasesAmount(), instanceOf(Double.class));
        verify(mView, times(1)).onLoading();
        verify(mView, times(1)).onFinishLoading();
    }

    @Test
    public void testGetTotalSalesAmount() {
        assertThat(mPresenter.getTotalSalesAmount(), instanceOf(Double.class));
        verify(mView, times(1)).onLoading();
        verify(mView, times(1)).onFinishLoading();
    }

    @Test
    public void testGetNetRevenueAmount() {
        assertThat(mPresenter.getNetRevenueAmount(), instanceOf(Double.class));
        verify(mView, times(1)).onLoading();
        verify(mView, times(1)).onFinishLoading();
    }

    @Test
    public void testOnPreparePieChartData() {
        mPresenter.onPreparePieChartData();
        verify(mView, times(1)).onLoading();
        verify(mView, times(1)).onFinishLoading();

    }

    @Test
    public void testNoTransactions() {
        transactionList = new ArrayList<>();
        when(mRealmDataManager.getAllProductStockRecords()).thenReturn(transactionList);
        assertThat(mPresenter.checkIsExistTransaction(), instanceOf(boolean.class));
        assertFalse(mPresenter.checkIsExistTransaction());
    }

    @Test
    public void testCheckIsExistTransaction() {
        assertThat(mPresenter.checkIsExistTransaction(), instanceOf(boolean.class));
        assertTrue(mPresenter.checkIsExistTransaction());
    }

    @Test
    public void testCheckIsExistTransactionIn() {
        assertThat(mPresenter.checkIsExistTransactionIn(), instanceOf(boolean.class));
        assertTrue(mPresenter.checkIsExistTransactionIn());
    }

    @Test
    public void testNoTransactionsIn() {
        transactionInList = new ArrayList<>();
        when(mRealmDataManager.getTransactionInProductStockRecords()).thenReturn(transactionInList);
        assertThat(mPresenter.checkIsExistTransactionIn(), instanceOf(boolean.class));
        assertFalse(mPresenter.checkIsExistTransactionIn());
    }

    @Test
    public void testCheckIsExistTransactionOut() {
        assertThat(mPresenter.checkIsExistTransactionOut(), instanceOf(boolean.class));
        assertTrue(mPresenter.checkIsExistTransactionOut());
    }

    @Test
    public void testNoTransactionsOut() {
        transactionOutList = new ArrayList<>();
        when(mRealmDataManager.getTransactionOutProductStockRecords()).thenReturn(transactionOutList);
        assertThat(mPresenter.checkIsExistTransactionOut(), instanceOf(boolean.class));
        assertFalse(mPresenter.checkIsExistTransactionOut());
    }


    @Test
    public void testShowSalesPieChart() {
        mPresenter.showPieChart(pieChart, 0);
        verify(mView, times(1)).onLoading();
        verify(mView, times(1)).onFinishLoading();
    }

    @Test
    public void testShowPurchasesPieChart() {
        mPresenter.showPieChart(pieChart, 1);
        verify(mView, times(1)).onLoading();
        verify(mView, times(1)).onFinishLoading();
    }

    @Test
    public void testGetSalesLegendItems() {
        mPresenter.onPreparePieChartData();
        assertThat(mPresenter.getLegendItems(0), instanceOf(List.class));
    }

    @Test
    public void testGetPurchasesLegendItems() {
        mPresenter.onPreparePieChartData();
        assertThat(mPresenter.getLegendItems(1), instanceOf(List.class));
    }
}
