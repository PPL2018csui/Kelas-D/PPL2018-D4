package com.ppl.d4.temancatat.seeders;

import com.ppl.d4.temancatat.models.SeederTransaction;
import com.ppl.d4.temancatat.models.manager.RealmDataManager;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.ArrayList;
import java.util.List;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.spy;

import static junit.framework.Assert.assertFalse;
import static junit.framework.Assert.assertTrue;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class SeederUnitTest {

    @Mock
    RealmDataManager mRealmDataManager;

    @Mock
    RealmDataManager mRealmDataManagerSkipped;

    private String expectedId;
    private String invalidId;
    private String expectedDesc;
    private String invalidDesc;

    private Seeder seeder;

    private List<Seeder> dependencies;

    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);

        expectedId = "Test Seeder";
        invalidId = "Real Seeder";

        expectedDesc = "Seeder buat test";
        invalidDesc = "Seeder buat beneran";

        Seeder dependent1 = mock(Seeder.class);
        Seeder dependent2 = mock(Seeder.class);

        dependencies = new ArrayList<>();
        dependencies.add(dependent1);
        dependencies.add(dependent2);

        Seeder testSeeder = new Seeder(expectedId, expectedDesc, dependencies);
        seeder = spy(testSeeder);

        SeederTransaction transaction = mock(SeederTransaction.class);
        when(mRealmDataManager.getSeederTransactionById(anyString()))
                .thenReturn(null);
        when(mRealmDataManagerSkipped.getSeederTransactionById(anyString()))
                .thenReturn(transaction);
    }

    @Test
    public void testValidId() {
        assertTrue(seeder.getId().equals(expectedId));
    }

    @Test
    public void testInvalidId() {
        assertFalse(seeder.getId().equals(invalidId));
    }

    @Test
    public void testValidDesc() {
        assertTrue(seeder.getDescription().equals(expectedDesc));
    }

    @Test
    public void testInvalidDesc() {
        assertFalse(seeder.getDescription().equals(invalidDesc));
    }

    @Test
    public void testGetDependencies() {
        assertTrue(seeder.getDependencies() == dependencies);
    }

    @Test
    public void testSeedDependencies() {
        seeder.seedDependencies(mRealmDataManager);
        for (Seeder dependency: dependencies) {
            verify(dependency, times(1)).seed(mRealmDataManager);
        }
    }

    @Test
    public void testCommitTransaction() {
        seeder.commitTransaction(mRealmDataManager);
        verify(mRealmDataManager, times(1)).createSeederTransaction(any());
    }

    @Test
    public void testIsAlreadyExecutedSuccess() {
        assertTrue(seeder.isAlreadyExecuted(mRealmDataManagerSkipped));
    }

    @Test
    public void testIsAlreadyExecutedFail() {
        assertFalse(seeder.isAlreadyExecuted(mRealmDataManager));
    }

    @Test
    public void testSeedSuccess() {
        seeder.seed(mRealmDataManager);
        verify(seeder, times(1)).isAlreadyExecuted(mRealmDataManager);
        verify(seeder, times(1)).seedDependencies(mRealmDataManager);
        verify(seeder, times(1)).handleSeed(mRealmDataManager);
        verify(seeder, times(1)).commitTransaction(mRealmDataManager);
    }

    @Test
    public void testSeedSkipped() {
        seeder.seed(mRealmDataManagerSkipped);
        verify(seeder, times(1)).isAlreadyExecuted(mRealmDataManagerSkipped);
        verify(seeder, times(0)).seedDependencies(mRealmDataManagerSkipped);
        verify(seeder, times(0)).handleSeed(mRealmDataManagerSkipped);
        verify(seeder, times(0)).commitTransaction(mRealmDataManagerSkipped);
    }
}
