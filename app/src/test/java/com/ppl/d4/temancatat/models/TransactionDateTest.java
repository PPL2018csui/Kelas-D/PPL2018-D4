package com.ppl.d4.temancatat.models;

import org.junit.Before;
import org.junit.Test;

import static com.ppl.d4.temancatat.models.TransactionListItem.TYPE_DATE;
import static junit.framework.Assert.assertFalse;
import static junit.framework.Assert.assertTrue;

public class TransactionDateTest {
    private TransactionDate transactionDate;
    private String expectedDate;
    private String invalidDate;

    @Before
    public void setUp() {
        expectedDate = "dummy date";
        transactionDate = new TransactionDate(expectedDate);
    }

    @Test
    public void testTransactionDateViewType() {
        assertTrue(transactionDate.getType() == TYPE_DATE);
    }

    @Test
    public void testDateValid() {
        assertTrue(transactionDate.getDate().equals(expectedDate));
    }

    @Test
    public void testDateInvalid() {
        assertFalse(transactionDate.getDate().equals(invalidDate));
    }

}



