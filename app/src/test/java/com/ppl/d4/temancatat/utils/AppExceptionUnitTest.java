package com.ppl.d4.temancatat.utils;

import org.junit.Before;
import org.junit.Test;
import static junit.framework.Assert.assertFalse;
import static junit.framework.Assert.assertTrue;

public class AppExceptionUnitTest {
    private Error error;
    private int expectedErrorCode;
    private int invalidErrorCode;
    private String expectedErrorMessage;
    private String expectedErrorMessageDetail;
    private String invalidErrorMessage;
    private String expectedFormattedMessage;
    private String invalidFormattedMessage;
    private AppException exception;
    private AppException exceptionDetailed;

    @Before
    public void setUp() {
        error = Error.FAILED_DATA_INSERTION;
        expectedErrorCode = error.getErrorCode();
        invalidErrorCode = -1;

        expectedErrorMessage = error.getErrorMessage();
        expectedErrorMessageDetail = "Terdapat masalah dalam penambahan data pada bagian A.";
        expectedFormattedMessage = expectedErrorMessage + " (" + expectedErrorCode + ").";
        invalidErrorMessage = "Ga ada error.";
        invalidFormattedMessage = "Yey ga ada error.";

        exception = new AppException(error);
        exceptionDetailed = new AppException(error, expectedErrorMessageDetail);
    }

    @Test
    public void getErrorCodeValid(){
        assertTrue(exception.getErrorCode() == expectedErrorCode);
    }

    @Test
    public void getErrorCodeInvalid(){
        assertFalse(exception.getErrorCode() == invalidErrorCode);
    }

    @Test
    public void getErrorMessageValid(){
        assertTrue((exception.getErrorMessage()).equals(expectedErrorMessage));
    }

    @Test
    public void getErrorMessageDetailedValid(){
        assertTrue((exceptionDetailed.getErrorMessage()).equals(expectedErrorMessageDetail));
    }

    @Test
    public void getErrorMessageInvalid(){
        assertFalse((exception.getErrorMessage()).equals(invalidErrorMessage));
    }

    @Test
    public void getFormattedMessageValid() {
        assertTrue(exception.getFormattedMessage().equals(expectedFormattedMessage));
    }

    @Test
    public void getFormattedMessageInvalid() {
        assertFalse(exception.getFormattedMessage().equals(invalidFormattedMessage));
    }

}
