package com.ppl.d4.temancatat.presenters;

import com.ppl.d4.temancatat.models.Product;
import com.ppl.d4.temancatat.models.ProductStockRecord;
import com.ppl.d4.temancatat.models.StockUnit;
import com.ppl.d4.temancatat.models.TransactionDate;
import com.ppl.d4.temancatat.models.TransactionListItem;
import com.ppl.d4.temancatat.models.manager.RealmDataManager;
import com.ppl.d4.temancatat.presenters.implementations.AllTransactionPresenter;
import com.ppl.d4.temancatat.ui.views.AllTransactionView;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static junit.framework.Assert.assertEquals;
import static junit.framework.Assert.assertFalse;
import static junit.framework.Assert.assertNotSame;
import static junit.framework.Assert.assertTrue;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class AllTransactionPresenterUnitTest {
    @Mock
    RealmDataManager mRealmDataManager;

    @Mock
    AllTransactionView mView;

    private AllTransactionPresenter presenter;

    private Product expectedProduct;
    private String expectedProductName;
    private String expectedProductDesc;
    private String expectedSKU;
    private StockUnit expectedStockUnit;

    private List<ProductStockRecord> expectedProductStockRecords;
    private int expectedProductStockRecordSize;
    private int invalidProductStockRecordSize;
    private int expectedTransactionInSize;
    private int invalidTransactionInSize;
    private int expectedTransactionOutSize;
    private int invalidTransactionOutSize;
    private int expectedStock;
    private double expectedPrice;

    private Date date1;
    private Date date2;
    private String expectedDate1;
    private String expectedDate2;
    private int expectedDateHeaderSize;
    private int invalidDateHeaderSize;


    private ProductStockRecord.TransactionType expectedTypeIn = ProductStockRecord.TransactionType.TRANSACTION_IN;
    private ProductStockRecord.TransactionType expectedTypeOut = ProductStockRecord.TransactionType.TRANSACTION_OUT;


    @Before
    public void setup() throws Exception{
        MockitoAnnotations.initMocks(this);

        presenter = new AllTransactionPresenter(mRealmDataManager);
        presenter.onAttach(mView);

        //Prepare Product
        expectedStockUnit = new StockUnit("Stock Unit");
        expectedSKU = "SKU";
        expectedProductName = "Product Name";
        expectedProductDesc = "Product Description";
        expectedProduct = new Product(
                expectedProductName,
                expectedProductDesc,
                expectedStockUnit,
                expectedSKU
        );

        expectedStock = 5;
        expectedPrice = 50000.0;

        //Prepare Date
        expectedDate1 = "SENIN, 01 JANUARI 2018";
        expectedDate2 = "JUMAT, 02 FEBRUARI 2018";
        String tmp = "01-01-2018";
        DateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
        date1 = dateFormat.parse(tmp);
        tmp = "02-02-2018";
        date2 = dateFormat.parse(tmp);
        expectedDateHeaderSize = 2;
        invalidDateHeaderSize = 3;

        //Prepare ProductStockRecord
        expectedProductStockRecords = new ArrayList<>();
        expectedProductStockRecordSize = 10;
        expectedTransactionInSize = 5;
        invalidTransactionInSize = 4;
        expectedTransactionOutSize = 5;
        invalidTransactionOutSize = 4;
        invalidProductStockRecordSize = 11;

        //Prepare list of ProductStockRecord
        for(int i = 0; i < expectedProductStockRecordSize/2; i++) {
            ProductStockRecord temp = new ProductStockRecord(
                    expectedProduct,
                    date1,
                    expectedTypeIn,
                    expectedStock,
                    expectedPrice
            );
            expectedProductStockRecords.add(temp);
        }

        for(int i = expectedProductStockRecordSize/2; i < expectedProductStockRecordSize; i++) {
            ProductStockRecord temp = new ProductStockRecord(
                    expectedProduct,
                    date2,
                    expectedTypeOut,
                    expectedStock,
                    expectedPrice
            );
            expectedProductStockRecords.add(temp);
        }

        when(mRealmDataManager.getAllProductStockRecords()).thenReturn(expectedProductStockRecords);
    }

    @Test
    public void testOnRealmSuccess() {
        presenter.onRealmSuccess();
    }

    @Test
    public void testOnRealmError() {
        presenter.onRealmError(new Throwable());
    }

    @Test
    public void testOnRealmClose() {
        presenter.closeRealm();
        verify(mRealmDataManager, times(1)).closeRealm();
    }

    @Test
    public void testOnAttach() {
        presenter.onAttach(mView);
        assert(presenter.getView()).equals(mView);
    }

    @Test
    public void testOnDetach() {
        presenter.onDetach();
    }

    @Test
    public void testGetView() {
        presenter.onAttach(mView);
        assert(presenter.getView()).equals(mView);
    }

    @Test
    public void testGetAllTransactionsValid() {
        List<TransactionListItem> transactionListItems = presenter.getAllTransactionItems();
        int transactionCounter = 0;
        int transactionInCounter = 0;
        int transactionOutCounter = 0;
        for (TransactionListItem transactionListItem: transactionListItems) {
            if (transactionListItem.getType() == TransactionListItem.TYPE_TRANSACTION) {
                transactionCounter++;
                if (((ProductStockRecord)transactionListItem).getTransactionType().equals(ProductStockRecord.TransactionType.TRANSACTION_IN)) {
                    transactionInCounter++;
                } else {
                    transactionOutCounter++;
                }
            }
        }

        assertTrue(transactionCounter == expectedProductStockRecordSize);
        assertTrue(transactionInCounter == expectedTransactionInSize);
        assertTrue(transactionOutCounter == expectedTransactionOutSize);
    }

    @Test
    public void testGetAllTransactionsInvalid() {
        List<TransactionListItem> transactionListItems = presenter.getAllTransactionItems();
        int transactionCounter = 0;
        int transactionInCounter = 0;
        int transactionOutCounter = 0;
        for (TransactionListItem transactionListItem: transactionListItems) {
            if (transactionListItem.getType() == TransactionListItem.TYPE_TRANSACTION) {
                transactionCounter++;
                if (((ProductStockRecord)transactionListItem).getTransactionType().equals(ProductStockRecord.TransactionType.TRANSACTION_IN)) {
                    transactionInCounter++;
                } else {
                    transactionOutCounter++;
                }
            }
        }
        assertFalse(transactionCounter == invalidProductStockRecordSize);
        assertFalse(transactionInCounter == invalidTransactionInSize);
        assertFalse(transactionOutCounter == invalidTransactionOutSize);
    }

    @Test
    public void testGetAllDateHeaderValid() {
        List<TransactionListItem> transactionListItems = presenter.getAllTransactionItems();
        int dateCounter = 0;
        String date1 = "";
        String date2 = "";
        for (TransactionListItem transactionListItem: transactionListItems) {
            if (transactionListItem.getType() == TransactionListItem.TYPE_DATE) {
                dateCounter++;
                String tmp = ((TransactionDate)transactionListItem).getDate();
                if (tmp.equalsIgnoreCase("Senin, 01 Januari 2018")) {
                    date1 = tmp;
                } else if (tmp.equalsIgnoreCase("Jumat, 02 Februari 2018")){
                    date2 = tmp;
                }
            }
        }
        assertTrue(dateCounter == expectedDateHeaderSize);
        assertEquals(expectedDate1, date1);
        assertEquals(expectedDate2, date2);
    }

    @Test
    public void testGetAllDateHeaderInvalid() {
        List<TransactionListItem> transactionListItems = presenter.getAllTransactionItems();
        int dateCounter = 0;
        String date1 = "";
        String date2 = "";
        for (TransactionListItem transactionListItem: transactionListItems) {
            if (transactionListItem.getType() == TransactionListItem.TYPE_DATE) {
                dateCounter++;
                String tmp = ((TransactionDate)transactionListItem).getDate();
                if (tmp.equalsIgnoreCase("Senin, 1 Januari 2018")) {
                    date1 = tmp;
                } else if (tmp.equalsIgnoreCase("Jumat, 2 Februari 2018")){
                    date2 = tmp;
                }
            }
        }
        assertFalse(dateCounter == invalidDateHeaderSize);
        assertNotSame(date1, expectedDate1);
        assertNotSame(date2,expectedDate2);
    }


}
