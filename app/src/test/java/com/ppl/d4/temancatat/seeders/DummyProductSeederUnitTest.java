package com.ppl.d4.temancatat.seeders;

import com.ppl.d4.temancatat.models.Product;
import com.ppl.d4.temancatat.models.StockUnit;
import com.ppl.d4.temancatat.models.manager.RealmDataManager;


import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;

import static junit.framework.Assert.assertFalse;
import static junit.framework.Assert.assertTrue;

public class DummyProductSeederUnitTest {

    // Instance lower bound for passing randomness test
    public static final int PRODUCT_NUMBER = 10;

    private int expectedNameLength;
    private int invalidNameLength;

    private int expectedDescLength;
    private int invalidDescLength;

    private int expectedPhotoURLLength;
    private int invalidPhotoURLLength;

    private int expectedSKULength;
    private int invalidSKULength;

    private List<StockUnit> expectedStockUnitList;

    private List<Product> productList;

    private DummyProductSeeder seeder;

    @Mock
    RealmDataManager mRealmDataManager;

    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);

        expectedNameLength = 15;
        invalidNameLength = 10;

        expectedDescLength = 30;
        invalidDescLength = 20;

        expectedPhotoURLLength = 9;
        invalidPhotoURLLength = 10;

        expectedSKULength = 8;
        invalidSKULength = 4;

        String expectedStockUnitId1 = "IDPERTAMA";
        String expectedStockUnitId2 = "IDKEDUA";

        StockUnit mStockUnit1 = mock(StockUnit.class);
        when(mStockUnit1.getId()).thenReturn(expectedStockUnitId1);

        StockUnit mStockUnit2 = mock(StockUnit.class);
        when(mStockUnit2.getId()).thenReturn(expectedStockUnitId2);

        expectedStockUnitList = new ArrayList<StockUnit>();
        expectedStockUnitList.add(mStockUnit1);
        expectedStockUnitList.add(mStockUnit2);

        when(mRealmDataManager.getAllStockUnits())
                .thenReturn(expectedStockUnitList);

        seeder = new DummyProductSeeder();

        productList = new ArrayList<Product>();
        for (int i = 0; i < PRODUCT_NUMBER; i++) {
            Product temp = seeder.generateDummyProduct(
                    expectedNameLength,
                    expectedDescLength,
                    expectedStockUnitList,
                    expectedPhotoURLLength,
                    expectedSKULength
            );

            productList.add(temp);
        }
    }

    @Test
    public void testRandomNameValid() {
        String randomName = seeder.generateRandomName(expectedNameLength);
        assertTrue(randomName.length() == expectedNameLength);
    }

    @Test
    public void testRandomNameInvalid() {
        String randomName = seeder.generateRandomName(expectedNameLength);
        assertFalse(randomName.length() == invalidNameLength);
    }

    @Test
    public void testRandomDescValid() {
        String randomDesc = seeder.generateRandomDescription(expectedDescLength);
        assertTrue(randomDesc.length() == expectedDescLength);
    }

    @Test
    public void testRandomDescInvalid() {
        String randomDesc = seeder.generateRandomDescription(expectedDescLength);
        assertFalse(randomDesc.length() == invalidDescLength);
    }

    @Test
    public void testRandomPhotoURLValid() {
        String randomPhotoURL = seeder.generateRandomPhotoURL(expectedPhotoURLLength);
        assertTrue(randomPhotoURL.length() == expectedPhotoURLLength);
    }

    @Test
    public void testRandomPhotoURLInvalid() {
        String randomPhotoURL = seeder.generateRandomPhotoURL(expectedPhotoURLLength);
        assertFalse(randomPhotoURL.length() == invalidPhotoURLLength);
    }

    @Test
    public void testRandomSKUValid() {
        String randomSKU = seeder.generateRandomSKU(expectedSKULength);
        assertTrue(randomSKU.length() == expectedSKULength);
    }

    @Test
    public void testRandomSKUInvalid() {
        String randomSKU = seeder.generateRandomSKU(expectedSKULength);
        assertFalse(randomSKU.length() == invalidSKULength);
    }

    @Test
    public void testDummyProductValid() {
        HashSet<String> seenNames = new HashSet<String>();
        HashSet<String> seenDescriptions = new HashSet<String>();
        HashSet<String> seenPhotoURLs = new HashSet<String>();
        HashSet<String> seenSKUs = new HashSet<String>();

        for(Product currentItem: productList) {
            assertFalse(seenNames.contains(currentItem.getName()));
            seenNames.add(currentItem.getName());

            assertFalse(seenDescriptions.contains(currentItem.getDescription()));
            seenDescriptions.add(currentItem.getDescription());

            assertFalse(seenSKUs.contains(currentItem.getSKU()));
            seenSKUs.add(currentItem.getSKU());

            boolean hasMatchingStockUnit = false;
            for(StockUnit stockUnit: expectedStockUnitList) {
                if (stockUnit.getId().equals(currentItem.getStockUnit().getId())) {
                    hasMatchingStockUnit = true;
                    break;
                }
            }

            assertTrue(hasMatchingStockUnit);
        }
    }

    @Test
    public void testHandleSeed() {
        seeder.handleSeed(mRealmDataManager);
        verify(mRealmDataManager, times(DummyProductSeeder.SEED_COUNT)).syncCreateOrUpdateProduct(any());
    }
}