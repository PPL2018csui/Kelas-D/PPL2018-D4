package com.ppl.d4.temancatat.models;

import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;

import static junit.framework.Assert.assertFalse;
import static junit.framework.Assert.assertTrue;

public class SeederTransactionTest {

    private static final int SEEDER_TRANSACTIONS_NUMBER = 100000;

    private String expectedId;
    private String invalidId;
    private String expectedDescription;
    private String customDescription;
    private String invalidDescription;

    private List<SeederTransaction> defaultSeeders;
    private SeederTransaction customSeeder;

    @Before
    public void setUp() {
        expectedId = "Custom Seeder";
        invalidId = "Bad Seeder";

        expectedDescription = "Seeder yang custom";
        customDescription = "Seeder edit-an";
        invalidDescription = "Seeder rusak";

        customSeeder = new SeederTransaction(expectedId, expectedDescription);

        defaultSeeders = new ArrayList<>();
        for (int i = 0; i < SEEDER_TRANSACTIONS_NUMBER; i++) {
            SeederTransaction temp = new SeederTransaction();
            defaultSeeders.add(temp);
        }
    }

    @Test
    public void testDefaultIdNotDuplicate() {
        HashSet<String> hashId = new HashSet<String>();

        for (SeederTransaction seederTransaction: defaultSeeders) {
            assertFalse(hashId.contains(seederTransaction.getId()));
            hashId.add(seederTransaction.getId());
        }
    }

    @Test
    public void testIdValid() {
        assertTrue(customSeeder.getId().equals(expectedId));
    }

    @Test
    public void testIdInvalid() {
        assertFalse(customSeeder.getId().equals(invalidId));
    }

    @Test
    public void testDescValid() {
        assertTrue(customSeeder.getDescription().equals(expectedDescription));
    }

    @Test
    public void testDescInvalid() {
        assertFalse(customSeeder.getDescription().equals(invalidDescription));
    }

    @Test
    public void testSetDescriptionValid() {
        customSeeder.setDescription(customDescription);
        assertTrue(customSeeder.getDescription().equals(customDescription));
    }
}
