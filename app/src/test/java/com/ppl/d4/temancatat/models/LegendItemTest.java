package com.ppl.d4.temancatat.models;

import org.junit.Before;
import org.junit.Test;

import static junit.framework.Assert.assertFalse;
import static junit.framework.Assert.assertTrue;


public class LegendItemTest {
    public int expectedColor;
    public int invalidColor;

    public String expectedProductSKU;
    public String invalidProductSKU;

    public double expectedPrice;
    public double invalidPrice;

    public int expectedAmount;
    public int invalidAmount;

    public LegendItem legendItem;
    public LegendItem legendItem2;

    public int expectedPriceDifference;
    public int invalidPriceDifference;

    @Before
    public void setUp() {
        expectedColor = 0xffffff;
        invalidColor = 0x000000;

        expectedProductSKU = "Pepeel Gege";
        invalidProductSKU = "Pepeel Tidak Gege";

        expectedPrice = 250000d;
        invalidPrice = -1d;

        expectedAmount = 727;
        invalidAmount = -1;

        legendItem = new LegendItem(
                expectedColor,
                expectedProductSKU,
                expectedPrice,
                expectedAmount
        );

        expectedPriceDifference = 123;
        invalidPriceDifference = -123;

        legendItem2 = new LegendItem(
                expectedColor,
                expectedProductSKU,
                expectedPrice + expectedPriceDifference,
                expectedAmount
        );
    }

    @Test
    public void testGetColorValid() {
        assertTrue(legendItem.getColor() == expectedColor);
    }

    @Test
    public void testGetColorInvalid() {
        assertTrue(legendItem.getColor() != invalidColor);
    }

    @Test
    public void testGetProductSKUValid() {
        assertTrue(legendItem.getProductSKU().equals(expectedProductSKU));
    }

    @Test
    public void testGetProductSKUInvalid() {
        assertTrue(!legendItem.getProductSKU().equals(invalidProductSKU));
    }

    @Test
    public void testGetProductAmountValid() {
        assertTrue(legendItem.getProductAmount() == expectedAmount);
    }

    @Test
    public void testGetProductAmountInvalid() {
        assertTrue(!(legendItem.getProductAmount() == invalidAmount));
    }

    @Test
    public void testCompareToValid() {
        assertTrue(legendItem.compareTo(legendItem2) == expectedPriceDifference);
    }

    @Test
    public void testCompareToInvalid() {
        assertTrue(!(legendItem.compareTo(legendItem2) == invalidPriceDifference));
    }
}
