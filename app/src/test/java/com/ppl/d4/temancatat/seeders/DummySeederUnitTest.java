package com.ppl.d4.temancatat.seeders;

import org.junit.Test;

import static junit.framework.Assert.assertTrue;

public class DummySeederUnitTest {
    @Test
    public void testSeedSuccess() {
        Seeder seeder = new DummySeeder();
        assertTrue(seeder.getDependencies() == DummySeeder.DEPENDENCIES);
    }
}
