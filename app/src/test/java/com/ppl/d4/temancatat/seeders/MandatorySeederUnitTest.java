package com.ppl.d4.temancatat.seeders;

import org.junit.Test;

import static junit.framework.Assert.assertTrue;

public class MandatorySeederUnitTest {
    @Test
    public void testSeedSuccess() {
        Seeder seeder = new MandatorySeeder();
        assertTrue(seeder.getDependencies() == MandatorySeeder.DEPENDENCIES);
    }
}
