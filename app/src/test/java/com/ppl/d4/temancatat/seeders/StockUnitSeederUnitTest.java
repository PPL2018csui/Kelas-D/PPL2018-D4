package com.ppl.d4.temancatat.seeders;

import com.ppl.d4.temancatat.models.StockUnit;
import com.ppl.d4.temancatat.models.manager.RealmDataManager;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class StockUnitSeederUnitTest {

    public StockUnitSeeder seeder;

    @Mock
    RealmDataManager mRealmDataManager;

    @Mock
    RealmDataManager mRealmDataManagerSkipped;

    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);

        seeder = new StockUnitSeeder();
        StockUnit sampleStockUnit = mock(StockUnit.class);

        when(mRealmDataManager.getStockUnitByName(anyString()))
                .thenReturn(null);
        when(mRealmDataManagerSkipped.getStockUnitByName(anyString()))
                .thenReturn(sampleStockUnit);
    }

    @Test
    public void testSeedSuccess() {
        int numberOfSeedStockUnits = StockUnitSeeder.STOCK_NAMES.size();

        seeder.handleSeed(mRealmDataManager);
        verify(mRealmDataManager, times(numberOfSeedStockUnits)).createStockUnit(any());
    }

    @Test
    public void testSeedSkipped() {
        seeder.handleSeed(mRealmDataManagerSkipped);
        verify(mRealmDataManager, times(0)).createStockUnit(any());
    }
}