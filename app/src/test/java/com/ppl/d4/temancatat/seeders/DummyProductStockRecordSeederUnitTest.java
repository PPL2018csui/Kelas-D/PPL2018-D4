package com.ppl.d4.temancatat.seeders;

import com.ppl.d4.temancatat.models.Product;
import com.ppl.d4.temancatat.models.ProductStockRecord;
import com.ppl.d4.temancatat.models.manager.RealmDataManager;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.mock;

import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.junit.Before;
import org.junit.Test;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Date;
import java.util.List;

import static junit.framework.Assert.assertFalse;
import static junit.framework.Assert.assertTrue;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class DummyProductStockRecordSeederUnitTest {

    // Instance lower bound for passing randomness test
    private static final int RECORD_NUMBER = 10;

    private static final List<ProductStockRecord.TransactionType> TRANSACTION_TYPES =
            Collections.unmodifiableList(
                    Arrays.asList(ProductStockRecord.TransactionType.values())
            );

    private static final String DATE_LOWER_BOUND = "2000-01-01 00:00:00";
    private static final String DATE_UPPER_BOUND = "2049-12-31 23:59:59";

    private List<ProductStockRecord> stockRecords;

    private long dateLowerBound;
    private long dateUpperBound;

    private Product product;

    private DummyProductStockRecordSeeder seeder;

    @Mock
    RealmDataManager mRealmDataManager;

    private List<Product> productList;

    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);

        product = mock(Product.class);

        dateLowerBound = Timestamp.valueOf(DATE_LOWER_BOUND).getTime();
        dateUpperBound = Timestamp.valueOf(DATE_UPPER_BOUND).getTime();

        seeder = new DummyProductStockRecordSeeder();

        stockRecords = new ArrayList<ProductStockRecord>();
        for (int i=0; i < RECORD_NUMBER; i++) {
            ProductStockRecord temp = seeder.generateDummyProductStockRecord(
                    product,
                    TRANSACTION_TYPES,
                    dateLowerBound,
                    dateUpperBound
            );

            stockRecords.add(temp);
        }

        Product mProduct1 = mock(Product.class);
        Product mProduct2 = mock(Product.class);

        productList = new ArrayList<Product>();
        productList.add(mProduct1);
        productList.add(mProduct2);

        when(mRealmDataManager.getAllProducts())
                .thenReturn(productList);
    }

    @Test
    public void testGeneratePriceValid() {
        double price = seeder.generatePrice();
        assertTrue(price >= 0);
    }

    @Test
    public void testGeneratePriceInvalid() {
        double price = seeder.generatePrice();
        assertFalse(price < 0);
    }

    @Test
    public void testGenerateStocksValid() {
        int stocks = seeder.generateStocks();
        assertTrue(stocks >= 0);
    }

    @Test
    public void testGenerateStocksInvalid() {
        int stocks = seeder.generateStocks();
        assertFalse(stocks < 0);
    }

    @Test
    public void testGenerateTypeValid() {
        ProductStockRecord.TransactionType type =
                seeder.generateType(TRANSACTION_TYPES);
        assertTrue(TRANSACTION_TYPES.contains(type));
    }

    @Test
    public void testGenerateRandomDateValid() {
        Date gen_date = seeder.generateRandomDate(dateLowerBound, dateUpperBound);

        assertTrue(
                gen_date.getTime() >= dateLowerBound &&
                        gen_date.getTime() <= dateUpperBound
        );
    }

    @Test
    public void generateDummyProductStockRecordValid() {
        for(int i = 0; i < RECORD_NUMBER; i++) {
            ProductStockRecord temp = stockRecords.get(i);

            assertTrue(temp.getProduct() == product);
        }
    }

    @Test
    public void testHandleSeed() {
        seeder.handleSeed(mRealmDataManager);
        int numberOfInvocations = DummyProductStockRecordSeeder.SEED_COUNT * productList.size();
        verify(mRealmDataManager, times(numberOfInvocations)).syncCreateOrUpdateProductStockRecord(any());
    }
}
