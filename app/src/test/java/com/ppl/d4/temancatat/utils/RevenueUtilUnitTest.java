package com.ppl.d4.temancatat.utils;

import com.ppl.d4.temancatat.models.Product;
import com.ppl.d4.temancatat.models.ProductStockRecord;
import com.ppl.d4.temancatat.models.manager.RealmDataManager;
import com.ppl.d4.temancatat.seeders.DummyProductStockRecordSeeder;

import org.junit.Before;
import org.junit.Test;
import org.mockito.MockitoAnnotations;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Date;
import java.util.List;

import static junit.framework.Assert.assertTrue;
import static org.mockito.Mockito.mock;

public class RevenueUtilUnitTest {

    // Instance lower bound for passing randomness test
    private static final int RECORD_NUMBER = 10;

    private static final List<ProductStockRecord.TransactionType> TRANSACTION_TYPES =
            Collections.unmodifiableList(
                    Arrays.asList(ProductStockRecord.TransactionType.values())
            );

    private static final String date = "2018-04-01 00:00:00";

    private List<ProductStockRecord> stockRecords;

    private RevenueUtil util = new RevenueUtil();

    private double expectedPrice1 = 0;
    private double notExpectedPrice1 = 0;
    private double expectedPrice2 = 0;
    private double notExpectedPrice2 = 0;
    private double expectedPrice3 = 0;
    private double notExpectedPrice3 = 0;

    private int expectedStock1 = 0;
    private int expectedStock2 = 0;

    private ProductStockRecord productStockRecord;
    private Product product;

    private DummyProductStockRecordSeeder seeder;

    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);

        productStockRecord = mock(ProductStockRecord.class);
        product = mock(Product.class);

        seeder = new DummyProductStockRecordSeeder();

        long dateConv = Timestamp.valueOf(date).getTime();

        stockRecords = new ArrayList<>();

        for (int i = 1; i <= RECORD_NUMBER/2; i++) {
            expectedPrice1 += i * 500d * i;
            expectedStock1 += i;

            ProductStockRecord temp = new ProductStockRecord(
                    product,
                    new Date(dateConv),
                    ProductStockRecord.TransactionType.TRANSACTION_IN,
                    i,
                    i * 500d);

            stockRecords.add(temp);
        }

        for (int i = 1; i <= RECORD_NUMBER/2; i++) {
            expectedPrice2 += i * 1000d * i;
            expectedStock2 += i;

            ProductStockRecord temp = new ProductStockRecord(
                    product,
                    new Date(dateConv),
                    ProductStockRecord.TransactionType.TRANSACTION_OUT,
                    i,
                    i * 1000d);

            stockRecords.add(temp);
        }

        expectedPrice3 = expectedPrice2 - expectedPrice1;
        notExpectedPrice1 = expectedPrice1 * -1;
        notExpectedPrice2 = expectedPrice2 * -1;
        notExpectedPrice3 = expectedPrice3 * -1;
    }

    @Test
    public void testCountPurchasesValid() {
        double purchases = util.countPurchases(stockRecords);
        assertTrue(purchases == expectedPrice1);
    }

    @Test
    public void testCountPurchasesInvalid() {
        double purchases = util.countPurchases(stockRecords);
        assertTrue(purchases != notExpectedPrice1);
    }

    @Test
    public void testCountSalesValid() {
        double sales = util.countSales(stockRecords);
        assertTrue(sales == expectedPrice2);
    }

    @Test
    public void testCountSalesInvalid() {
        double sales = util.countSales(stockRecords);
        assertTrue(sales != notExpectedPrice1);
    }

    @Test
    public void testCountRevenueValid() {
        double revenue = util.countTotalRevenue(stockRecords);
        assertTrue(revenue == expectedPrice3);
    }

    @Test
    public void testCountRevenueInvalid() {
        double revenue = util.countTotalRevenue(stockRecords);
        assertTrue(revenue != notExpectedPrice3);
    }

    @Test
    public void testCountRevenueValid2() {
        double revenue = util.countTotalRevenue(expectedPrice1, expectedPrice2);
        assertTrue(revenue == expectedPrice3);
    }

    @Test
    public void testCountRevenueInvalid2() {
        double revenue = util.countTotalRevenue(expectedPrice1, expectedPrice2);
        assertTrue(revenue != notExpectedPrice3);
    }

    @Test
    public void testCountTotalStockPurchaseValid() {
        int stock = util.countTotalStockPurchase(stockRecords);
        assertTrue(stock == expectedStock1);
    }

    @Test
    public void testCountTotalStockSalesValid() {
        int stock = util.countTotalStockSales(stockRecords);
        assertTrue(stock == expectedStock2);
    }
}
