package com.ppl.d4.temancatat.presenters;

import com.ppl.d4.temancatat.models.Product;
import com.ppl.d4.temancatat.models.manager.RealmDataManager;
import com.ppl.d4.temancatat.presenters.implementations.ProductListPresenter;
import com.ppl.d4.temancatat.presenters.implementations.SearchProductPresenter;
import com.ppl.d4.temancatat.ui.views.ProductListView;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.ArrayList;
import java.util.List;

import static junit.framework.Assert.assertFalse;
import static junit.framework.Assert.assertTrue;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class ProductListPresenterUnitTest {

    public static final int PRODUCT_NUMBER = 10;

    @Mock
    RealmDataManager mRealmDataManager;

    @Mock
    ProductListView mView;

    @Mock
    Product mProduct;

    private ProductListPresenter presenter;
    private List<Product> productList;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);

        presenter = new ProductListPresenter(mRealmDataManager);
        presenter.onAttach(mView);

        productList = new ArrayList<Product>();

        for(int i = 0; i < PRODUCT_NUMBER; i++) {
            productList.add(mProduct);
        }

        when(mRealmDataManager.getAllProducts()).thenReturn(productList);
        when(mRealmDataManager.getProductsFilterBySKU(anyString())).thenReturn(productList);
    }

    @Test
    public void testGetAllProductsValid() {
        List<Product> resProductList = presenter.getAllProducts();
        assertTrue(resProductList.size() == PRODUCT_NUMBER);

        verify(mView, times(1)).onLoading();
        verify(mView, times(1)).onFinishLoading();
    }

    @Test
    public void testGetAllProductsInvalid() {
        List<Product> resProductList = presenter.getAllProducts();
        assertFalse(resProductList == null);
    }

    @Test
    public void testFilterProductsBySKUValid() {
        List<Product> resProductList = presenter.filterProductsBySKU("");
        assertTrue(resProductList.size() == PRODUCT_NUMBER);

        verify(mView, times(1)).onLoading();
        verify(mView, times(1)).onFinishLoading();
    }

    @Test
    public void testFilterProductsBySKUInvalid() {
        List<Product> resProductList = presenter.filterProductsBySKU("");
        assertFalse(resProductList == null);
    }

    @Test
    public void testOnRealmSuccess() {
        presenter.onRealmSuccess();
    }

    @Test
    public void testOnRealmError() {
        presenter.onRealmError(new Throwable());
    }

    @Test
    public void testOnRealmClose() {
        presenter.closeRealm();
        verify(mRealmDataManager, times(1)).closeRealm();
    }
}
