package com.ppl.d4.temancatat;

import android.support.test.InstrumentationRegistry;
import android.support.test.rule.ActivityTestRule;
import android.support.test.runner.AndroidJUnit4;

import com.ppl.d4.temancatat.ui.activities.AddTransactionActivity;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;


import static org.hamcrest.Matchers.not;
import static org.hamcrest.Matchers.allOf;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.instanceOf;
import static org.hamcrest.Matchers.containsString;
import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.Espresso.onData;
import static android.support.test.espresso.assertion.ViewAssertions.matches;
import static android.support.test.espresso.matcher.ViewMatchers.isDisplayed;
import static android.support.test.espresso.matcher.ViewMatchers.withId;
import static android.support.test.espresso.matcher.ViewMatchers.withText;
import static android.support.test.espresso.matcher.ViewMatchers.withSpinnerText;
import static android.support.test.espresso.action.ViewActions.click;
import static android.support.test.espresso.action.ViewActions.typeText;
import static android.support.test.espresso.action.ViewActions.closeSoftKeyboard;


@RunWith(AndroidJUnit4.class)
public class AddTransactionActivityInstrumentedTest {
	
    private String numberString;
    private String dateString;
    private String characterString;
	
    @Rule
    public ActivityTestRule<AddTransactionActivity> activityTestRule =
            new ActivityTestRule<AddTransactionActivity>(AddTransactionActivity.class);
	
    @Before
    public void setup() {
        numberString = "105";
        dateString = "10/50-123123";
        characterString = "lalala123";
    }

    @Test
    public void changeDateEditTextValid() {
        onView(withId(R.id.date_edittext))
                .perform(typeText(dateString), closeSoftKeyboard());

        onView(withId(R.id.date_edittext))
                .check(matches(withText(dateString)));

        onView(withId(R.id.date_edittext))
                .check(matches(isDisplayed()));
    }
	
	@Test
    public void changeDateEditTextInvalid() {
        onView(withId(R.id.date_edittext))
                .perform(typeText(characterString), closeSoftKeyboard());

        onView(withId(R.id.date_edittext))
                .check(matches(not(withText(characterString))));

        onView(withId(R.id.date_edittext))
                .check(matches(isDisplayed()));
    }
	
    public void changeAmountEditTextValid() {
        onView(withId(R.id.amount_edittext))
                .perform(typeText(numberString), closeSoftKeyboard());

        onView(withId(R.id.amount_edittext))
                .check(matches(withText(numberString)));

        onView(withId(R.id.amount_edittext))
                .check(matches(isDisplayed()));
    }
	
    @Test
    public void changeAmountEditTextInvalid() {
        onView(withId(R.id.amount_edittext))
                .perform(typeText(dateString), closeSoftKeyboard());

        onView(withId(R.id.amount_edittext))
                .check(matches(not(withText(dateString))));

        onView(withId(R.id.amount_edittext))
                .check(matches(isDisplayed()));
    }
	
    public void changeUnitPriceEditTextValid() {
        onView(withId(R.id.unit_price_edittext))
                .perform(typeText(numberString), closeSoftKeyboard());

        onView(withId(R.id.unit_price_edittext))
                .check(matches(withText(numberString)));

        onView(withId(R.id.unit_price_edittext))
                .check(matches(isDisplayed()));
    }
	
    @Test
    public void changeUnitPriceEditTextInvalid() {
        onView(withId(R.id.unit_price_edittext))
                .perform(typeText(dateString), closeSoftKeyboard());

        onView(withId(R.id.unit_price_edittext))
                .check(matches(not(withText(dateString))));

        onView(withId(R.id.unit_price_edittext))
                .check(matches(isDisplayed()));
    }
	
    @Test
    public void checkTransactionTypeSpinnerFirstEntry() {
        String[] selections = activityTestRule.getActivity().getResources()
                .getStringArray(R.array.transaction_type_array);
        String selectionText = selections[0];

        onView(withId(R.id.transaction_type_dropdown_spinner)).perform(click());
        onData(allOf(is(instanceOf(String.class)), is(selectionText))).perform(click());
        onView(withId(R.id.transaction_type_dropdown_spinner)).check(matches(withSpinnerText(containsString(selectionText))));
    }
	
    @Test
    public void checkTransactionTypeSpinnerSecondEntry() {
        String[] selections = activityTestRule.getActivity().getResources()
                .getStringArray(R.array.transaction_type_array);
        String selectionText = selections[1];

        onView(withId(R.id.transaction_type_dropdown_spinner)).perform(click());
        onData(allOf(is(instanceOf(String.class)), is(selectionText))).perform(click());
        onView(withId(R.id.transaction_type_dropdown_spinner)).check(matches(withSpinnerText(containsString(selectionText))));
    }

}
