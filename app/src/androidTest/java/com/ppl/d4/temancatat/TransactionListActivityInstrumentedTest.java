package com.ppl.d4.temancatat;

import android.support.test.rule.ActivityTestRule;
import android.support.test.runner.AndroidJUnit4;

import com.ppl.d4.temancatat.ui.activities.TransactionListActivity;

import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.assertion.ViewAssertions.matches;
import static android.support.test.espresso.matcher.ViewMatchers.isDisplayed;
import static android.support.test.espresso.matcher.ViewMatchers.withId;
import static org.junit.Assert.*;

@RunWith(AndroidJUnit4.class)
public class TransactionListActivityInstrumentedTest {
    @Rule
    public ActivityTestRule<TransactionListActivity> activityTestRule =
            new ActivityTestRule<TransactionListActivity>(TransactionListActivity.class);

    @Test
    public void checkToolBarIsDisplayedCorrectly() {
        assertEquals(activityTestRule
                        .getActivity()
                        .getSupportActionBar()
                        .getTitle()
                        .toString(),
                activityTestRule
                        .getActivity()
                        .getResources()
                        .getString(R.string.transaction_list_toolbar_text));
    }

    @Test
    public void checkFloatingButtonIsDisplayed() {
        onView(withId(R.id.add_new_fabutton))
                .check(matches(isDisplayed()));
    }

    @Test
    public void checkRecyclerViewIsDisplayed() {
        onView(withId(R.id.transaction_list_recyclerview))
                .check(matches(isDisplayed()));
    }
}