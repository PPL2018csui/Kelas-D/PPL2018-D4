package com.ppl.d4.temancatat;


import android.support.test.rule.ActivityTestRule;
import android.support.test.runner.AndroidJUnit4;

import com.ppl.d4.temancatat.ui.activities.AddProductActivity;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import static android.support.test.espresso.Espresso.onData;
import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.action.ViewActions.click;
import static android.support.test.espresso.action.ViewActions.closeSoftKeyboard;
import static android.support.test.espresso.action.ViewActions.typeText;
import static android.support.test.espresso.assertion.ViewAssertions.matches;
import static android.support.test.espresso.matcher.ViewMatchers.isDisplayed;
import static android.support.test.espresso.matcher.ViewMatchers.withId;
import static android.support.test.espresso.matcher.ViewMatchers.withText;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.not;
import static org.hamcrest.core.AllOf.allOf;


@RunWith(AndroidJUnit4.class)
public class AddProductActivityInstrumentedTest {

    private String characterString;
    private String numberString;

    @Rule
    public ActivityTestRule<AddProductActivity> activityTestRule =
            new ActivityTestRule<AddProductActivity>(AddProductActivity.class);

    @Before
    public void setup() {
        characterString = "Bania";
        numberString = "123";
    }

    @Test
    public void spinnerisDisplayed() {
        String[] myArray = activityTestRule.getActivity().getResources()
                .getStringArray(R.array.units_array);

        onView(allOf(withId(R.id.product_unit_spinner), withText(myArray[1]), isDisplayed()));
    }

    //@Test
    public void validateSpinner() {
        String[] myArray = activityTestRule.getActivity().getResources()
                .getStringArray(R.array.units_array);

        int size = myArray.length;

        for (int i=0; i<size; i++) {
            onView(withId(R.id.product_unit_spinner)).perform(click());

            onData(is(myArray[i])).perform(click());

            onView(allOf(withId(R.id.product_unit_spinner),
                    withText(myArray[i]), isDisplayed()));
        }
    }

    //@Test
    public void changeNameEditTextValid() {
        onView(withId(R.id.add_product_name_editText))
                .perform(typeText(characterString), closeSoftKeyboard());

        onView(withId(R.id.add_product_name_editText))
                .check(matches(withText(characterString)));

        onView(withId(R.id.add_product_name_editText))
                .check(matches(isDisplayed()));
    }

    //@Test
    public void changeDescEditTextValid() {
        onView(withId(R.id.add_product_desc_editText))
                .perform(typeText(characterString), closeSoftKeyboard());

        onView(withId(R.id.add_product_desc_editText))
                .check(matches(withText(characterString)));

        onView(withId(R.id.add_product_desc_editText))
                .check(matches(isDisplayed()));
    }

    //@Test
    public void changeValueEditTextValid() {
        onView(withId(R.id.add_product_value_editText))
                .perform(typeText(String.valueOf(numberString)), closeSoftKeyboard());

        onView(withId(R.id.add_product_value_editText))
                .check(matches(withText(String.valueOf(numberString))));

        onView(withId(R.id.add_product_value_editText))
                .check(matches(isDisplayed()));
    }

    //@Test
    public void changeValueEditTextInvalid() {
        onView(withId(R.id.add_product_value_editText))
                .perform(typeText(characterString), closeSoftKeyboard());

        onView(withId(R.id.add_product_value_editText))
                .check(matches(not(withText(characterString))));

        onView(withId(R.id.add_product_value_editText))
                .check(matches(isDisplayed()));
    }

    //@Test
    public void changeQuantityEditTextValid() {
        onView(withId(R.id.add_product_quantity_editText))
                .perform(typeText(String.valueOf(numberString)), closeSoftKeyboard());

        onView(withId(R.id.add_product_quantity_editText))
                .check(matches(withText(String.valueOf(numberString))));

        onView(withId(R.id.add_product_quantity_editText))
                .check(matches(isDisplayed()));
    }

    //@Test
    public void changeQuantityEditTextInvalid() {
        onView(withId(R.id.add_product_quantity_editText))
                .perform(typeText(characterString), closeSoftKeyboard());

        onView(withId(R.id.add_product_quantity_editText))
                .check(matches(not(withText(characterString))));

        onView(withId(R.id.add_product_quantity_editText))
                .check(matches(isDisplayed()));
    }
}
